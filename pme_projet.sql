-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  sam. 20 nov. 2021 à 18:21
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `pme_projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `identifiant` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `profil` varchar(255) NOT NULL,
  `statut` int(11) NOT NULL,
  `telephone` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`id`, `identifiant`, `password`, `email`, `profil`, `statut`, `telephone`) VALUES
(1, 'franck', '21232f297a57a5a743894a0e4a801fc3', 'franckymbeums@gmail.com', 'IMG-20210610-WA0026.jpg26-10-2021a18-271.jpg', 1, '695043420');

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `date_modif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom`, `date_modif`, `date_creation`) VALUES
(1, 'SARL', '2021-11-09 15:39:26', '2021-10-25 14:14:38'),
(2, 'SA', '2021-11-12 04:17:28', '2021-10-26 13:35:47'),
(3, 'ETS1', '2021-11-20 14:47:43', '2021-11-20 14:47:43');

-- --------------------------------------------------------

--
-- Structure de la table `departement`
--

CREATE TABLE `departement` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `chef_departement` varchar(255) NOT NULL,
  `id_entreprise` int(11) NOT NULL,
  `statut` int(11) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `departement`
--

INSERT INTO `departement` (`id`, `nom`, `reference`, `chef_departement`, `id_entreprise`, `statut`, `date_creation`) VALUES
(3, 'Ressource Humaine', 'RH-iNCH ENTREPRISE', 'post vaccant', 1, 1, '2021-11-14 02:41:16'),
(4, 'Formation', 'form', 'post vaccant', 6, 1, '2021-11-19 05:29:54'),
(5, 'Laboratoire', 'SARL-Labo', 'post vaccant', 1, 1, '2021-11-19 18:24:00'),
(6, 'Communication', 'sarl-com-find it group', 'post vaccant', 6, 1, '2021-11-20 00:14:06'),
(7, 'Logistique', 'Logis-studio graphique', 'post vaccant', 2, 1, '2021-11-20 09:04:22'),
(8, 'Production', 'Prod-Studio graph', 'post vaccant', 2, 1, '2021-11-20 09:05:24'),
(9, 'FORMATION', 'inch-Forma', 'post vaccant', 7, 1, '2021-11-20 15:07:29');

-- --------------------------------------------------------

--
-- Structure de la table `diplome`
--

CREATE TABLE `diplome` (
  `id` int(11) NOT NULL,
  `id_employer` int(11) NOT NULL,
  `diplome` varchar(255) NOT NULL,
  `annee` date NOT NULL,
  `etablissement` varchar(255) NOT NULL,
  `pdf_diplome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `employer`
--

CREATE TABLE `employer` (
  `id` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `id_entreprise` int(11) NOT NULL,
  `id_departement` int(11) NOT NULL,
  `identifiant` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `statut` int(11) NOT NULL,
  `id_responsabilite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `employer`
--

INSERT INTO `employer` (`id`, `id_users`, `id_entreprise`, `id_departement`, `identifiant`, `password`, `statut`, `id_responsabilite`) VALUES
(2, 13, 6, 4, 'francoistema', '7bfc7530d049a9ace6e6ab9c2ddd7d95', 2, 3),
(3, 14, 1, 3, 'borelngounou', '21232f297a57a5a743894a0e4a801fc3', 1, 2),
(4, 15, 6, 6, 'paulinngantchou', '21232f297a57a5a743894a0e4a801fc3', 1, 6),
(5, 16, 2, 7, 'madeleinedogmo', '21232f297a57a5a743894a0e4a801fc3', 1, 8),
(6, 17, 2, 8, 'djuissihelene', '21232f297a57a5a743894a0e4a801fc3', 1, 9),
(8, 20, 7, 9, 'apppol', 'd599ed22dfb8eb88ee0d21c94871a692', 1, 10);

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE `entreprise` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `localisation` varchar(255) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_proprietaire` int(11) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `numero_contrib` varchar(255) NOT NULL,
  `statut` int(11) NOT NULL,
  `id_secteur` int(11) NOT NULL,
  `profil` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `entreprise`
--

INSERT INTO `entreprise` (`id`, `nom`, `reference`, `localisation`, `date_creation`, `id_proprietaire`, `id_categorie`, `numero_contrib`, `statut`, `id_secteur`, `profil`) VALUES
(1, 'GLOBAL ENTREPRISE', 'SARL-EDUCATION-G.', 'akwa bali', '2021-11-30 14:30:43', 1, 1, '1232542', 1, 2, 'téléchargement.jpg19-11-2021a17-171.jpg'),
(2, 'STUDIO GRAPHIC', 'Elect125-T', 'Paris France', '2021-11-03 08:40:18', 2, 2, '52589', 1, 1, 'colorful-modele-photographie_1057-1790.jpg19-11-2021a17-121.jpg'),
(3, 'Elect CAMER', 'SA-ELECTRICITE', 'akwa bali', '2021-11-23 15:19:28', 5, 2, '', 1, 1, 'ELECTRIC.jpg19-11-2021a17-141.jpg'),
(4, 'LOGICLOS', 'SA-ALUCAM', 'BASE INDUSTRIELLE BASSA', '2021-11-24 23:30:00', 4, 2, '120542203', 1, 6, 'images.png19-11-2021a17-161.png'),
(6, 'FIND IT GROUP', 'SARL-FIND IT GROUP', 'Akwa Dubai', '2021-11-18 14:07:10', 8, 1, 'N8123ffdd33', 1, 6, '3814.jpg_wh860.jpg18-11-2021a14-071.jpg'),
(7, 'Digital Zangalewa', 'sa-Digital Zangalewa', 'akwa bali', '2021-11-20 14:56:39', 9, 2, 'ND 123 -  124', 1, 2, 'images.png20-11-2021a15-02.png');

-- --------------------------------------------------------

--
-- Structure de la table `externe_entreprise`
--

CREATE TABLE `externe_entreprise` (
  `id` int(11) NOT NULL,
  `id_entreprise` int(11) NOT NULL,
  `id_departement` int(11) NOT NULL,
  `nom` int(11) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `numero_contrib` varchar(255) NOT NULL,
  `localisation` varchar(255) NOT NULL,
  `statut` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `externe_user`
--

CREATE TABLE `externe_user` (
  `id` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `id_entreprise` int(11) NOT NULL,
  `id_departement` int(11) NOT NULL,
  `entreprise` varchar(255) NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `statut` int(11) NOT NULL,
  `id_externe_entreprise` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `formation`
--

CREATE TABLE `formation` (
  `id` int(11) NOT NULL,
  `id_employer` int(11) NOT NULL,
  `etablissement` varchar(255) NOT NULL,
  `filiere` varchar(255) NOT NULL,
  `annee` varchar(255) NOT NULL,
  `pdf_certification` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `proprietaire`
--

CREATE TABLE `proprietaire` (
  `id` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `identifiant` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `statut` int(11) NOT NULL,
  `profession` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `proprietaire`
--

INSERT INTO `proprietaire` (`id`, `id_users`, `identifiant`, `password`, `statut`, `profession`) VALUES
(1, 1, 'APPOLOS', '21232f297a57a5a743894a0e4a801fc3', 1, 'Musicien'),
(2, 2, 'FRANCO', '21232f297a57a5a743894a0e4a801fc3', 1, 'Entrepreneur'),
(3, 3, 'kevino', '21232f297a57a5a743894a0e4a801fc3', 1, 'Develppeur pro'),
(4, 4, 'zidan', '21232f297a57a5a743894a0e4a801fc3', 1, 'Designeur'),
(5, 5, 'quintilia', '21232f297a57a5a743894a0e4a801fc3', 1, 'avocat'),
(6, 6, 'pascalobito', '21232f297a57a5a743894a0e4a801fc3', 1, 'Community manager'),
(7, 7, 'pascalobito', '21232f297a57a5a743894a0e4a801fc3', 2, 'Community manager'),
(8, 9, 'stephtako', '21232f297a57a5a743894a0e4a801fc3', 1, 'Développeur'),
(9, 18, 'dontsacyprien', '21232f297a57a5a743894a0e4a801fc3', 1, 'Devellopeur ');

-- --------------------------------------------------------

--
-- Structure de la table `responsabilite`
--

CREATE TABLE `responsabilite` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `id_entreprise` int(11) NOT NULL,
  `statut` int(11) NOT NULL,
  `id_departement` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `responsabilite`
--

INSERT INTO `responsabilite` (`id`, `nom`, `reference`, `id_entreprise`, `statut`, `id_departement`, `description`, `date_creation`) VALUES
(2, 'Directeur ', 'DG-RH-iNCH ENTREPRISE', 1, 3, 3, 'jhhkjkjl', '2021-11-14 02:42:08'),
(3, 'Formateur', 'form-formateur', 6, 2, 4, 'hduhRGSDFJQOGJDF%IQJGDFIJQPOKSDFPIGJQOFJQOGKSDFOJQPJFDIJOKVOCJVIJDFIJGVDFIJQVFJKJ', '2021-11-19 05:32:12'),
(4, 'Secretaire', 'RH-Secretaire', 1, 1, 3, 'Veillez a mettre a l\'aillaise le Directeur des ressources humaines', '2021-11-19 18:06:23'),
(5, 'Design', 'sarl-design-fid it group', 6, 2, 6, 'shdsmiqugmisfdugqiodfmjdf', '2021-11-20 00:17:51'),
(6, 'Community m.', 'sarl-community m- find it ', 6, 0, 6, 'fsdqghsuigisfojqgoi', '2021-11-20 06:13:27'),
(7, 'Acheteur', 'logis-Acheteur-studio graphiq', 2, 1, 7, 'qsjgijmsfklgjfidjl', '2021-11-20 09:07:37'),
(8, 'Magasinier', 'Logis-Magasinier-studio graphiqu', 2, 1, 7, 'dfhsuithtuiris', '2021-11-20 09:08:48'),
(9, 'Infographe', 'Prod-Infograph-studio graphiq', 2, 1, 8, 'ufierugoieru', '2021-11-20 09:16:40'),
(10, 'Formateur', 'inch-forma', 7, 1, 9, 'tiens en mains le suivis', '2021-11-20 15:09:10');

-- --------------------------------------------------------

--
-- Structure de la table `secteur`
--

CREATE TABLE `secteur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `secteur`
--

INSERT INTO `secteur` (`id`, `nom`, `date_creation`, `date_modif`) VALUES
(1, 'Finances', '2021-10-25 00:00:00', '2021-11-16 15:20:41'),
(2, 'Education1', '2021-10-26 13:37:24', '2021-11-12 11:55:21'),
(5, 'AGRICULTURE1', '2021-11-04 17:15:56', '2021-11-12 11:55:42'),
(6, 'INDUSTRIE', '2021-11-08 11:44:44', '2021-11-12 11:54:46'),
(7, 'AGROALIMENTAIRE', '2021-11-20 14:48:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `date_naissance` date NOT NULL,
  `sexe` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `profil` varchar(255) NOT NULL,
  `numero_cni` varchar(255) NOT NULL,
  `nationalite` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `regime_matrimo` varchar(255) NOT NULL,
  `etat_sante` varchar(255) NOT NULL,
  `nbre_enfant` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `nom`, `prenom`, `date_naissance`, `sexe`, `telephone`, `profil`, `numero_cni`, `nationalite`, `age`, `regime_matrimo`, `etat_sante`, `nbre_enfant`, `email`) VALUES
(1, 'Ngono', 'Appolinaire', '0000-00-00', '', '657489876', 'user4.jpg19-11-2021a17-31.jpg', '154 675 987', '', 0, '', '', '', 'appoloss96@gmail.com'),
(2, 'MBEUMI', 'FRANCK', '2021-10-25', 'Homme', '698 03 24 15', 'IMG-20210610-WA0026.jpg26-10-2021a18-271.jpg', '123 456 789', 'Camerounaise', 80, 'Marié', 'Apte', 'Aucune reponse', 'franckymbeums@gmail.com'),
(3, 'DEFFO', 'KEVIN', '2021-10-26', 'Homme', '698 25 41 74 ', 'IMG-20210610-WA0026.jpg26-10-2021a18-271.jpg', '147 258 369 ', 'Camerounaise', 26, 'Marié', 'Apte', 'Aucune reponse', 'kevino@gmail.com'),
(4, 'Nsangou', 'Zidan', '2021-10-26', 'Homme', '695 12 35 48', 'Gant.jpg06-11-2021a16-321.jpg', '123 546 987 ', 'Camerounaise', 25, 'Marié', 'Apte', 'Aucune reponse', 'zidan@gmail.com'),
(5, 'Nsenga', 'Junie', '2021-10-27', 'Femme', '654 73 68 90', 'page_de__couverture-02.jpg27-10-2021a21-171.jpg', '123 456 789', 'Camerounaise', 25, 'Celibataire', 'Apte', 'Aucune reponse', 'junie@gmail.com'),
(6, 'EBANDA', 'Pascal', '1993-06-04', 'Homme', '398 43 56 47', 'P2.JPG.jpeg06-11-2021a06-491.jpeg', '123 456 789', 'Camerounaise', 50, 'Marié', 'Apte', 'Aucune reponse', 'pascalobito@gmail.com'),
(7, 'EBANDA', 'Pascal', '1993-06-04', 'Homme', '398 43 56 47', 'P2.JPG.jpeg06-11-2021a06-541.jpeg', '123 456 789', 'Camerounaise', 50, 'Marié', 'Apte', 'Aucune reponse', 'pascaLto@gmail.com'),
(9, 'TAKODJOU', 'Stephane', '2019-02-07', 'Homme', '690 75 59 79', 'j_zYz-RI_400x400.jpg18-11-2021a14-001.jpg', '132 456 789 2', 'Camerounaise', 23, 'Celibataire', 'Apte', 'Aucune reponse', 'stephanetakodjou@gmail.com'),
(13, 'TEMA', 'Francois', '1995-11-10', 'Homme', '654 67 87 69', 'vehicule1.jpg19-11-2021a10-038.jpg', '132 456 789 ', 'Camerounaise', 25, 'Celibataire', 'Apte', 'Aucun', 'vvymmo@gmail.com'),
(14, 'NGOUNOU', 'BOREL', '1995-11-19', 'Homme', '657 48 93 20', 'user1.jpg19-11-2021a17-361.jpg', '123 456 789', 'Camerounaise', 26, 'Celibataire', 'Apte', '2', 'achillemanuela@gmail.com'),
(15, 'NGANTCHOU', 'Paulin', '1990-11-01', 'Homme', '698 76 45 36', 'user2.jpg20-11-2021a07-098.jpg', '119 234 564', 'Camerounaise', 31, 'Celibataire', 'Apte', '3', 'paulin@gmail.com'),
(16, 'DOGMO', 'Madeleine', '1998-11-03', 'Femme', '690 78 65 34', 'employers.jpg20-11-2021a09-522.jpg', '117 879 650', 'Camerounaise', 23, 'Marié', 'Apte', '1', 'bergereloutioumbe@gmail.com'),
(17, 'DJUISSI MBINDA', 'Helene', '1997-11-01', 'Femme', '693 25 63 17', 'employer.jpg20-11-2021a10-052.jpg', '117 234 567', 'Camerounaise', 24, 'Celibataire', 'Apte', '1', 'djuissi@gmail.com'),
(18, 'DONTSA', 'Cyprien1', '1995-11-02', 'Homme', '675 43 23 26', 'user5.jpg20-11-2021a14-531.jpg', '113 345 654', 'Camerounais', 27, 'Celibataire', 'Apte', 'Aucun', 'cypriendontsa@gmail.com'),
(19, 'NTOBONG ', 'Appolinaire', '2003-07-03', 'Homme', '235 565 767', 'j_zYz-RI_400x400.jpg20-11-2021a15-069.jpg', '132 456', 'Congolaise', 14, 'Celibataire', 'Apte', '2', 'appolos66@gmail.com'),
(20, 'MBEUMI', 'APPOL', '2000-02-20', 'Homme', '263276744', 'j_zYz-RI_400x400.jpg20-11-2021a15-129.jpg', '132 456 789 66', 'Congolaise', 21, 'Divorcé', 'Apte', 'Aucun', 'superemployer@forum.com');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_entreprise_departement` (`id_entreprise`);

--
-- Index pour la table `diplome`
--
ALTER TABLE `diplome`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_employer_diplome` (`id_employer`);

--
-- Index pour la table `employer`
--
ALTER TABLE `employer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_users_employer` (`id_users`),
  ADD KEY `fk_id_entreprise_employer` (`id_entreprise`),
  ADD KEY `fk_id_departement_employer` (`id_departement`),
  ADD KEY `fk_id_responsabilite_employer` (`id_responsabilite`);

--
-- Index pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_categorie_entreprise` (`id_categorie`),
  ADD KEY `fk_id_proprietaire_entreprise` (`id_proprietaire`),
  ADD KEY `fk_id_secteur_entreprise` (`id_secteur`);

--
-- Index pour la table `externe_entreprise`
--
ALTER TABLE `externe_entreprise`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_entreprise_externe_entreprise` (`id_entreprise`),
  ADD KEY `fk_id_departement_externe_entreprise` (`id_departement`);

--
-- Index pour la table `externe_user`
--
ALTER TABLE `externe_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_users_externe_user` (`id_users`),
  ADD KEY `fk_id_entreprise_externe_user` (`id_entreprise`),
  ADD KEY `fk_id_departement_externe_user` (`id_departement`),
  ADD KEY `fk_id_externe_entreprise_externe_user` (`id_externe_entreprise`);

--
-- Index pour la table `formation`
--
ALTER TABLE `formation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_employer_formation` (`id_employer`);

--
-- Index pour la table `proprietaire`
--
ALTER TABLE `proprietaire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_users_proprietaire` (`id_users`);

--
-- Index pour la table `responsabilite`
--
ALTER TABLE `responsabilite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_entreprise_responsabilite` (`id_entreprise`),
  ADD KEY `fk_id_departement_responsabilite` (`id_departement`);

--
-- Index pour la table `secteur`
--
ALTER TABLE `secteur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `departement`
--
ALTER TABLE `departement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `diplome`
--
ALTER TABLE `diplome`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `employer`
--
ALTER TABLE `employer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `entreprise`
--
ALTER TABLE `entreprise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `externe_entreprise`
--
ALTER TABLE `externe_entreprise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `externe_user`
--
ALTER TABLE `externe_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `formation`
--
ALTER TABLE `formation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `proprietaire`
--
ALTER TABLE `proprietaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `responsabilite`
--
ALTER TABLE `responsabilite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `secteur`
--
ALTER TABLE `secteur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `departement`
--
ALTER TABLE `departement`
  ADD CONSTRAINT `fk_id_entreprise_departement` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id`);

--
-- Contraintes pour la table `diplome`
--
ALTER TABLE `diplome`
  ADD CONSTRAINT `fk_id_employer_diplome` FOREIGN KEY (`id_employer`) REFERENCES `employer` (`id`);

--
-- Contraintes pour la table `employer`
--
ALTER TABLE `employer`
  ADD CONSTRAINT `fk_id_departement_employer` FOREIGN KEY (`id_departement`) REFERENCES `departement` (`id`),
  ADD CONSTRAINT `fk_id_entreprise_employer` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id`),
  ADD CONSTRAINT `fk_id_responsabilite_employer` FOREIGN KEY (`id_responsabilite`) REFERENCES `responsabilite` (`id`),
  ADD CONSTRAINT `fk_id_users_employer` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD CONSTRAINT `fk_id_categorie_entreprise` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `fk_id_proprietaire_entreprise` FOREIGN KEY (`id_proprietaire`) REFERENCES `proprietaire` (`id`),
  ADD CONSTRAINT `fk_id_secteur_entreprise` FOREIGN KEY (`id_secteur`) REFERENCES `secteur` (`id`);

--
-- Contraintes pour la table `externe_entreprise`
--
ALTER TABLE `externe_entreprise`
  ADD CONSTRAINT `fk_id_departement_externe_entreprise` FOREIGN KEY (`id_departement`) REFERENCES `departement` (`id`),
  ADD CONSTRAINT `fk_id_entreprise_externe_entreprise` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id`);

--
-- Contraintes pour la table `externe_user`
--
ALTER TABLE `externe_user`
  ADD CONSTRAINT `fk_id_departement_externe_user` FOREIGN KEY (`id_departement`) REFERENCES `departement` (`id`),
  ADD CONSTRAINT `fk_id_entreprise_externe_user` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id`),
  ADD CONSTRAINT `fk_id_externe_entreprise_externe_user` FOREIGN KEY (`id_externe_entreprise`) REFERENCES `externe_entreprise` (`id`),
  ADD CONSTRAINT `fk_id_users_externe_user` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `formation`
--
ALTER TABLE `formation`
  ADD CONSTRAINT `fk_id_employer_formation` FOREIGN KEY (`id_employer`) REFERENCES `employer` (`id`);

--
-- Contraintes pour la table `proprietaire`
--
ALTER TABLE `proprietaire`
  ADD CONSTRAINT `fk_id_users_proprietaire` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `responsabilite`
--
ALTER TABLE `responsabilite`
  ADD CONSTRAINT `fk_id_departement_responsabilite` FOREIGN KEY (`id_departement`) REFERENCES `departement` (`id`),
  ADD CONSTRAINT `fk_id_entreprise_responsabilite` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
