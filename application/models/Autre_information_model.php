<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Autre_information_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer une autre information

			private $id;
			private $id_employer;
			private $entreprise_stage;
			private $annee_stage;
			private $poste_stage;
            private $entreprise_emploi;
            private $annee_emploi;
            private $poste_emploi;
            private $pdf_stage;
            private $pdf_emploi;
            private $duree_stage;
			private $duree_emploi;

			protected $table= 'autre_information';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addAutre_information(){

			    $this->db->set('id', $this->id)
			    	->set('id_employer', $this->id_employer)
			    	->set('entreprise_stage', $this->entreprise_stage)
			    	->set('annee_stage', $this->annee_stage)
			    	->set('poste_stage', $this->poste_stage)
			    	->set('entreprise_emploi', $this->entreprise_emploi)
					->set('annee_emploi', $this->annee_emploi)
                    ->set('poste_emploi', $this->poste_emploi)
                    ->set('pdf_stage', $this->pdf_stage)
                    ->set('pdf_emploi', $this->pdf_emploi)
                    ->set('duree_stage', $this->duree_stage)
                    ->set('duree_emploi', $this->duree_emploi)
					->insert($this->table);
		
			}


			// fonction qui charge toutes les autres informations pour faire le filtrage de donnee
			
			public function findAllInformationBd(){
				$data = $this->db->select('id,id_employer,entreprise_stage,annee_stage,poste_stage,entreprise_emploi,annee_emploi,poste_emploi,pdf_stage,pdf_emploi,duree_stage,duree_emploi,')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_employer']=$row->id_employer;
			       	$donnees[$i]['entreprise_stage']=$row->entreprise_stage;
			       	$donnees[$i]['annee_stage']=$row->annee_stage;
			       	$donnees[$i]['poste_stage']=$row->poste_stage;
			       	$donnees[$i]['entreprise_emploi']=$row->entreprise_emploi;
					$donnees[$i]['annee_emploi']=$row->annee_emploi;
                    $donnees[$i]['poste_emploi']=$row->poste_emploi;
                    $donnees[$i]['pdf_stage']=$row->pdf_stage;
                    $donnees[$i]['pdf_emploi']=$row->pdf_emploi;
                    $donnees[$i]['duree_stage']=$row->duree_stage;
                    $donnees[$i]['duree_emploi']=$row->duree_emploi;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}

			// fonction qui reccupère juste l'email d'un users

			// public function findUsersemail($id){
			// 	$data =$this->db->select('email')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();

								
			// 	foreach ($data as $row){
			//        	$donnees['email']=$row->email;
			// 	}

			// 	return $donnees['email'];
			// }

			// fonction qui reccupère juste le password d'un Users

			// public function findUserspassword($id){
			// 	$data =$this->db->select('password')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();

			// 		$donnees['password']='non';				
			// 	foreach ($data as $row){
			//        	$donnees['password']=$row->password;
			// 	}

			// 	return $donnees['password'];
			// }
			

		    // fonction qui reccupère juste le nom d'un Users
			
			// public function findUsersName($id){
			// 	$data =$this->db->select('nom')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();

			// 	foreach ($data as $row){
			//        	$donnees['nom']=$row->nom;
			// 	}

			// 	return $donnees['nom'];
			// }

   //          public function findUsersLastName($id){
			// 	$data =$this->db->select('prenom')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();

			// 	foreach ($data as $row){
			//        	$donnees['prenom']=$row->prenom;
			// 	}

			// 	return $donnees['prenom'];
			// }

			// fonction qui reccupère juste le profil d'un Users
			
			// public function findProfil($id){
			// 	$data =$this->db->select('profil')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();
						
			// 	foreach ($data as $row){
			//        	$donnees['profil']=$row->profil;
			// 	}

			// 	return $donnees['profil'];
			// }


			


			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setId_employer($id_employer){
				$this->id_employer=$id_employer;
			}
			
			public function setEntreprise_stage($entreprise_stage){
				$this->entreprise_stage=$entreprise_stage;
			}

			public function setAnnee_stage($annee_stage){
				$this->annee_stage=$annee_stage;
			}
            public function setPoste_stage($poste_stage){
				$this->poste_stage=$poste_stage;
			}
            public function setEntreprise_emploi($entreprise_emploi){
				$this->entreprise_emploi=$entreprise_emploi;
			}
			public function setAnnee_emploi($annee_emploi){
				$this->annee_emploi=$annee_emploil;
			}
			public function setPoste_emploi($poste_emploi){
				$this->poste_emploi=$poste_emploi;
			}
            public function setPdf_stage($pdf_stage){
				$this->pdf_stage=$pdf_stage;
			}
            public function setPdf_emploi($pdf_emploi){
				$this->pdf_emploi=$pdf_emploi;
			}
            public function setDuree_stage($duree_stage){
				$this->duree_stage=$duree_stage;
			}
            public function setDuree_emploi($duree_emploi){
				$this->duree_emploi=$duree_emploi;
			}
            

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getId_employer(){
				return $this->id_employer;
			
			}
            public function getEntreprise_stage(){
				return $this->entreprise_stage;
			
			}

			public function getAnnee_stage(){
				return $this->annee_stage;
			
			}

			public function getPoste_stage(){
				return $this->poste_stage;
			
			}
            public function getEntreprise_emploi(){
				return $this->entreprise_emploi;
			
			}
            public function getAnnee_emploi(){
				return $this->annee_emploi;
			
			}
            public function getPoste_emploi(){
				return $this->poste_emploi;
			
			}
			public function getPdf_stage(){
				return $this->pdf_stage;
			
			}
            public function getPdf_emploi(){
				return $this->pdf_emploi;
			
			}
            public function getDuree_stage(){
				return $this->duree_stage;
			
			}
				public function getDuree_emploi(){
				return $this->duree_emploi;
			
			}
		
						
	
}


?>