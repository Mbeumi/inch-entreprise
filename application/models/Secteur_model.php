<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Secteur_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un categorie

			private $id;
			private $nom;
			private $date_modif;
			private $date_creation;
			

			protected $table= 'secteur';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addSecteur(){

			    $this->db->set('id', $this->id)
			    	->set('nom', $this->nom)
			    	->set('date_modif', $this->date_modif)
					->set('date_creation', $this->date_creation)
					->insert($this->table);
		
			}


			// fonction qui charge tous les Categories pour faire le filtrage de donnee
			
			public function findAllSecteurBd(){
				$data = $this->db->select('*')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['date_modif']=$row->date_modif;
					   $donnees[$i]['date_creation']=$row->date_creation;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}


			public function findSecteur($id_secteur){
				$data =$this->db->select()
								->from($this->table)
								->where('id', $id_secteur)
								->get()
								->result();

				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}
				}
				return $donnees;	
			}
			
			public function UpdateSecteur($cible){
				$this->db->set('nom',$this->nom)
						->set('date_modif',$this->date_modif)
						->where('id',$cible)
						->update($this->table);
			}

			public function deleteSecteur($cible){
		    	$this->db->where('id',$cible)
		    			->delete($this->table);
		    }


			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setNom($nom){
				$this->nom=$nom;
			}
			
			public function setDate_modif($date_modif){
				$this->date_modif=$date_modif;
			}

			public function setDate_creation($date_creation){
				$this->date_creation=$date_creation;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getNom(){
				return $this->nom;
			
			}

			public function getDate_modif(){
				return $this->date_modif;
			
			}

			public function getDate_creation(){
				return $this->date_creation;
			
			}

						
	
}


?>