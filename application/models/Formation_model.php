<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Formation_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer la Formation de l'employer

			private $id;
			private $id_employer;
			private $etablissement;
			private $filiere;
			private $niveau;
            private $annee;
            private $pdf_certification;

			protected $table= 'formaction';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addFormation(){

			    $this->db->set('id', $this->id)
			    	->set('id_employer', $this->id_employer)
			    	->set('etablissement', $this->etablissement)
			    	->set('filiere', $this->filiere)
			    	->set('niveau', $this->niveau)
			    	->set('annee', $this->annee)
					->set('pdf_certification', $this->pdf_certification)
					->insert($this->table);
		
			}


			// fonction qui charge toutes les Formations pour faire le filtrage de donnee
			
			public function findAllFormationBd(){
				$data = $this->db->select('id,id_employer,etablissement,filiere,niveau,annee,pdf_certification,')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_employer']=$row->id_employer;
			       	$donnees[$i]['etablissement']=$row->etablissement;
			       	$donnees[$i]['filiere']=$row->filiere;
			       	$donnees[$i]['niveau']=$row->niveau;
			       	$donnees[$i]['annee']=$row->annee;
					$donnees[$i]['pdf_certification']=$row->pdf_certification;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}

			// fonction qui reccupère juste l'email d'un users

			// public function findUsersemail($id){
			// 	$data =$this->db->select('email')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();

								
			// 	foreach ($data as $row){
			//        	$donnees['email']=$row->email;
			// 	}

			// 	return $donnees['email'];
			// }

			// fonction qui reccupère juste le password d'un Users

			// public function findUserspassword($id){
			// 	$data =$this->db->select('password')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();

			// 		$donnees['password']='non';				
			// 	foreach ($data as $row){
			//        	$donnees['password']=$row->password;
			// 	}

			// 	return $donnees['password'];
			// }
			

		    // fonction qui reccupère juste le nom d'un Users
			
			// public function findUsersName($id){
			// 	$data =$this->db->select('nom')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();

			// 	foreach ($data as $row){
			//        	$donnees['nom']=$row->nom;
			// 	}

			// 	return $donnees['nom'];
			// }

   //          public function findUsersLastName($id){
			// 	$data =$this->db->select('prenom')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();

			// 	foreach ($data as $row){
			//        	$donnees['prenom']=$row->prenom;
			// 	}

			// 	return $donnees['prenom'];
			// }

			// // fonction qui reccupère juste le profil d'un Users
			
			// public function findProfil($id){
			// 	$data =$this->db->select('profil')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();
						
			// 	foreach ($data as $row){
			//        	$donnees['profil']=$row->profil;
			// 	}

			// 	return $donnees['profil'];
			// }


			


			// setteurs


			public function setId($id){
				$this->id=$id;
			}

			public function setId_employer($id_employer){
				$this->id_employer=$id_employer;
			}
			
			public function setEtablissement($etablissement){
				$this->etablissement=$etablissement;
			}

			public function setFiliere($filiere){
				$this->filiere=$filiere;
			}
            public function setNiveau($niveau){
				$this->niveau=$niveau;
			}
            public function setAnnee($annee){
				$this->annee=$annee;
			}
			public function setPdf_certification($pdf_certification){
				$this->pdf_certification=$pdf_certification;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getId_employer(){
				return $this->id_employer;
			
			}
            public function getNiveau(){
				return $this->niveau;
			
			}

			public function getFiliere(){
				return $this->filiere;
			
			}

			public function getEtablissement(){
				return $this->etablissement;
			
			}
            public function getAnnee(){
				return $this->annee;
			
			}
            public function getPdf_certification(){
				return $this->pdf_certification;
			
			}
						
	
}


?>