<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Admin_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un admin

			private $id;
			private $identifiant;
			private $password;
			private $email;
			private $profil;
			private $statut;
			private $telephone;

			protected $table= 'admin';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addAdmin(){

			    $this->db->set('id', $this->id)
			    	->set('identifiant', $this->identifiant)
			    	->set('password', $this->password)
			    	->set('email', $this->email)
			    	->set('profil', $this->profil)
			    	->set('statut', $this->statut)
					->set('telephone', $this->telephone)
					->insert($this->table);
		
			}


			// fonction qui charge tous les Admins pour faire le filtrage de donnee
			
			public function findAllAdminBd(){
				$data = $this->db->select('id,identifiant,password,email,profil,statut,telephone,')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['identifiant']=$row->identifiant;
			       	$donnees[$i]['password']=$row->password;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['profil']=$row->profil;
			       	$donnees[$i]['statut']=$row->statut;
					$donnees[$i]['telephone']=$row->telephone;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}

			// fonction qui reccupère juste l'email d'un admin

			public function findAdminemail($id){
				$data =$this->db->select('email')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

								
				foreach ($data as $row){
			       	$donnees['email']=$row->email;
				}

				return $donnees['email'];
			}

			// fonction qui reccupère juste le password d'un admin

			public function findAdminpassword($id){
				$data =$this->db->select('password')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

					$donnees['password']='non';				
				foreach ($data as $row){
			       	$donnees['password']=$row->password;
				}

				return $donnees['password'];
			}
			

		    // fonction qui reccupère juste le nom d'un admin
			
			public function findAdminName($id){
				$data =$this->db->select('identifiant')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

				foreach ($data as $row){
			       	$donnees['identifiant']=$row->identifiant;
				}

				return $donnees['identifiant'];
			}

			// fonction qui reccupère juste le profil d'un admin
			
			public function findProfil($id){
				$data =$this->db->select('profil')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();
						
				foreach ($data as $row){
			       	$donnees['profil']=$row->profil;
				}

				return $donnees['profil'];
			}


			// fonction qui reccupère juste le password et email d'un admin

			public function findAdminInfos($email,$password){
				$data =$this->db->select('password,email')
						->from($this->table)
						->where(array('password'=>$password,'email'=>$email))
						->limit(1)
						->get()
						->result();

				$donnees['data']='non';			
				foreach ($data as $row){
			       	$donnees['email']=$row->email;
			       	$donnees['password']=$row->password;
			       	$donnees['data']='ok';
				}

				return $donnees;
			}



			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setIdentifiant($identifiant){
				$this->identifiant=$identifiant;
			}
			
			public function setPassword($password){
				$this->password=$password;
			}

			public function setEmail($email){
				$this->email=$email;
			}
			public function setProfil($profil){
				$this->profil=$profil;
			}
			public function setStatut($statut){
				$this->statut=$statut;
			}
			public function setTelephone($telephone){
				$this->telephone=$telephone;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getIdentifiant(){
				return $this->identifiant;
			
			}

			public function getPassword(){
				return $this->password;
			
			}

			public function getEmail(){
				return $this->email;
			
			}
			public function getProfil(){
				return $this->profil;
			
			}
				public function getStatut(){
				return $this->statut;
			
			}
			public function getTelephone(){
				return $this->telephone;
			
			}
						
	
}


?>
