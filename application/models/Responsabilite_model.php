<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Responsabilite_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un responsabilite

			private $id;
			private $nom;
			private $reference;
			private $id_entreprise;
			private $statut;
            private $id_departement;
			private $description;
			private $date_creation;

			protected $table= 'responsabilite';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addResponsabilite(){

			    $this->db->set('id', $this->id)
			    	->set('nom', $this->nom)
			    	->set('reference', $this->reference)
			    	->set('id_entreprise', $this->id_entreprise)
					->set('statut', $this->statut)
					->set('id_departement', $this->id_departement)
					->set('description', $this->description)
					->set('date_creation', $this->date_creation)
					->insert($this->table);
		
			}


			// fonction qui charge tous les responsabilites pour faire le filtrage de donnee
			
			public function findAllResponsabiliteBd(){
				$data = $this->db->select()
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['reference']=$row->reference;
			       	$donnees[$i]['id_entreprise']=$row->id_entreprise;
					$donnees[$i]['statut']=$row->statut;
					$donnees[$i]['id_departement']=$row->id_departement;
					$donnees[$i]['description']=$row->description;
					$donnees[$i]['date_creation']=$row->date_creation;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}


			



			// fonctions pour recuperer tous les elements d'une responsabilité en fonction de l'entreprise et du departement 
			public function findResponsabilite($cible){
				$data = $this->db->select('*')
						->from($this->table)
						->where('id_departement',$cible)
						->get()
						->result();
				$i=0;
				$donnees['data']='non';	
				
				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$i][$key]=$value;
					}
					$donnees['data']='ok';
					$i++;
				}
				$donnees['total']=$i;
				return $donnees;
			}


			public function findResponsabiliteEntreprise($cible){
				$data = $this->db->select('*')
						->from($this->table)
						->where('id_entreprise',$cible)
						->get()
						->result();
				$i=0;
				$donnees['data']='non';	
				
				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$i][$key]=$value;
					}
					$donnees['data']='ok';
					$i++;
				}
				$donnees['total']=$i;
				return $donnees;
			}



			public function findResponsabiliteId($cible){
				$data = $this->db->select('*')
						->from($this->table)
						->where('id',$cible)
						->limit(1)
						->get()
						->result();	
				
				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}
				}

				return $donnees;
			}

			



			//  fonctions pour modifier les informations d'une responsabilite


			public function UpdateResponsabilite($cible){
				$this->db->set('nom',$this->nom)
						->set('reference',$this->reference)
						->set('id_departement',$this->id_departement)
						->set('description',$this->description)
						->set('statut',$this->statut)
						->where('id',$cible)
						->update($this->table);
			}


			//  fonction pour supprimer une responsabilite


			public function deleteResponsabilite($cible){
		    	$this->db->where('id',$cible)
		    			->delete($this->table);
		    }




			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setNom($nom){
				$this->nom=$nom;
			}
			
			public function setReference($reference){
				$this->reference=$reference;
			}
			public function setId_entreprise($id_entreprise){
				$this->id_entreprise=$id_entreprise;
			}
			public function setStatut($statut){
				$this->statut=$statut;
			}

			public function setId_departement($id_departement){
				$this->id_departement=$id_departement;
			}

			public function setDescription($description){
				$this->description=$description;
			}

			public function setDate_creation($date_creation){
				$this->date_creation=$date_creation;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getNom(){
				return $this->nom;
			
			}
			public function getReference(){
				return $this->reference;
			
			}
				public function getId_entreprise(){
				return $this->id_entreprise;
			
			}
			public function getStatut(){
				return $this->statut;
			
			}

			public function getId_departement(){
				return $this->id_departement;
			
			}


			public function getDescription(){
				return $this->description;
			
			}

			public function getDate_creation(){
				return $this->date_creation;
			
			}
						
	
}


?>