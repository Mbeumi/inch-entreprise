<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Proprietaire_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un proprietaire

			private $id;
			private $id_users;
			private $identifiant;
			private $password;
			private $statut;
			private $profession;

			protected $table= 'proprietaire';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}

     //  fonction pour ajouter un nouveau proprietaire

			public function addproprietaire(){

			    $this->db->set('id', $this->id)
			    	->set('id_users', $this->id_users)
			    	->set('identifiant', $this->identifiant)
			    	->set('password', $this->password)
					->set('statut', $this->statut)
					->set('profession', $this->profession)
					->insert($this->table);
		
			}


			// fonction qui charge tous les proprietaires pour faire le filtrage de donnee
			
			public function findAllproprietaireBd(){
				$data = $this->db->select('id,id_users,identifiant,password,statut,profession,')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_users']=$row->id_users;
			       	$donnees[$i]['identifiant']=$row->identifiant;
			       	$donnees[$i]['password']=$row->password;
					$donnees[$i]['statut']=$row->statut;
					$donnees[$i]['profession']=$row->profession;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}

	// fonction pour recuperer les information d'un proprietaire


			public function findProprietaireInfos($cible){
				$data = $this->db->select()
						->from($this->table)
						->where('id', $cible)
						->limit(1)
						->get()
						->result();
						
				foreach ($data as $row){
					$donnees['id']=$row->id;
					$donnees['id_users']=$row->id_users;
					$donnees['identifiant']=$row->identifiant;
					$donnees['password']=$row->password;
				 	$donnees['statut']=$row->statut;
					 $donnees['profession']=$row->profession;
				}

				return $donnees;
			}


			public function findpassword($cible){
				$data = $this->db->select('password')
						->from($this->table)
						->where('id_users', $cible)
						->limit(1)
						->get()
						->result();

				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}
				}
		
				return $donnees;
			}

			public function findpasswordUsers($cible){
				$data = $this->db->select('password')
						->from($this->table)
						->where('id', $cible)
						->limit(1)
						->get()
						->result();

				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}
				}
		
				return $donnees;
			}


			public function findId_users($cible){
				$data = $this->db->select('id_users')
						->from($this->table)
						->where('id', $cible)
						->limit(1)
						->get()
						->result();

				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}
				}
		
				return $donnees;
			}

	//  fonction pour mettre a jour le statut d'un propriétaire
			
			public function Updatestatutproprietaire($cible){
				$this->db->set('statut',$this->statut)
						->where('id',$cible)
						->update($this->table);
			}


			public function Updatepasswordproprietaire($cible){
				$this->db->set('password',$this->password)
						->where('id_users',$cible)
						->update($this->table);
			}

	//  fonction pour mettre a jour le profil d'un propriétaire

			public function Updateproprietaire($cible){
				$this->db->set('identifiant', $this->identifiant)
						 ->set('profession', $this->profession)
						 ->where('id',$cible)
						 ->update($this->table);
						 
			}


			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setId_users($id_users){
				$this->id_users=$id_users;
			}
			
			public function setIdentifiant($identifiant){
				$this->identifiant=$identifiant;
			}
			public function setPassword($password){
				$this->password=$password;
			}
			public function setStatut($statut){
				$this->statut=$statut;
			}
			public function setProfession($profession){
				$this->profession=$profession;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getId_users(){
				return $this->id_users;
			
			}
			public function getIdentifiant(){
				return $this->identifiant;
			
			}
				public function getPassword(){
				return $this->password;
			
			}
			public function getStatut(){
				return $this->statut;
			
			}

			public function getProfession(){
				return $this->profession;
			
			}
						
	
}


?>