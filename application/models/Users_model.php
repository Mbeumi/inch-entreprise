<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Users_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un user

			private $id;
			private $nom;
			private $prenom;
			private $date_naissance;
			private $sexe;
            private $telephone;
            private $profil;
            private $numero_cni;
            private $nationalite;
            private $age;
            private $regime_matrimo;
			private $etat_sante;
			private $nbre_enfant;
            private $email;

			protected $table= 'users';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addUsers(){

			    $this->db->set('id', $this->id)
			    	->set('nom', $this->nom)
			    	->set('prenom', $this->prenom)
			    	->set('date_naissance', $this->date_naissance)
			    	->set('sexe', $this->sexe)
			    	->set('telephone', $this->telephone)
					->set('profil', $this->profil)
                    ->set('numero_cni', $this->numero_cni)
                    ->set('nationalite', $this->nationalite)
                    ->set('age', $this->age)
                    ->set('regime_matrimo', $this->regime_matrimo)
                    ->set('etat_sante', $this->etat_sante)
                    ->set('nbre_enfant', $this->nbre_enfant)
                    ->set('email', $this->email)
					->insert($this->table);
		
			}


			// fonction qui charge tous les users pour faire le filtrage de donnee
			
			public function findAllUsersBd(){
				$data = $this->db->select()
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['prenom']=$row->prenom;
			       	$donnees[$i]['date_naissance']=$row->date_naissance;
			       	$donnees[$i]['sexe']=$row->sexe;
			       	$donnees[$i]['telephone']=$row->telephone;
					$donnees[$i]['profil']=$row->profil;
                    $donnees[$i]['numero_cni']=$row->numero_cni;
                    $donnees[$i]['nationalite']=$row->nationalite;
                    $donnees[$i]['age']=$row->age;
                    $donnees[$i]['regime_matrimo']=$row->regime_matrimo;
                    $donnees[$i]['etat_sante']=$row->etat_sante;
                    $donnees[$i]['nbre_enfant']=$row->nbre_enfant;
                    $donnees[$i]['email']=$row->email;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}
			

			public function findUsersemail($email){
				$data=$this->db->select()
								->from($this->table)
								->where('email',$email)
								->limit(1)
								->get()
								->result();
				$donnees['data']='non';
				foreach($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}
					$donnees['data']='ok';
				}
				return $donnees;
			}
			// fonction qui reccupère juste l'email d'un users

			public function findUsers($cible){
				$data =$this->db->select()
								->from($this->table)
								->where('id', $cible)
								->get()
								->result();

				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}  	
				}
				return $donnees;	
			}


			public function findUsersId($cible){
				$data =$this->db->select('email')
								->from($this->table)
								->where('id', $cible)
								->get()
								->result();

				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}  	
				}
				return $donnees;	
			}




           //  fonction pour recuperer l'id d-un utilisateur 

			public function findlastUser(){
				$data = $this->db->select('id')
								->from($this->table)
								->order_by('id','desc')
								->limit(1)
								->get()
								->result();
				foreach ($data as $row){
					$donnees['id_users'] = $row->id;
				}
				return $donnees;
			}
//   fonction pour modifier les informations d'un utilisateur

			public function UpdateUserinfo($cible){
				$this->db->set('nom', $this->nom)
						 ->set('prenom', $this->prenom)
						 ->set('date_naissance', $this->date_naissance)
						 ->set('sexe', $this->sexe)
						 ->set('telephone', $this->telephone)
						 ->set('profil', $this->profil)
						 ->set('numero_cni', $this->numero_cni)
						 ->set('nationalite', $this->nationalite)
						 ->set('age', $this->age)
						 ->set('regime_matrimo', $this->regime_matrimo)
						 ->set('etat_sante', $this->etat_sante)
						 ->set('nbre_enfant', $this->nbre_enfant)
						 ->set('email', $this->email)
						 ->where('id',$cible)
						 ->update($this->table);
			}


			public function UpdateUser($cible){
				$this->db->set('nom', $this->nom)
						 ->set('prenom', $this->prenom)
						 ->set('telephone', $this->telephone)
						 ->set('profil', $this->profil)
						 ->set('numero_cni', $this->numero_cni)
						 ->set('email', $this->email)
						 ->where('id',$cible)
						 ->update($this->table);
			}


			


			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setNom($nom){
				$this->nom=$nom;
			}
			
			public function setPrenom($prenom){
				$this->prenom=$prenom;
			}

			public function setDate_naissance($date_naissance){
				$this->date_naissance=$date_naissance;
			}
            public function setSexe($sexe){
				$this->sexe=$sexe;
			}
            public function setTelephone($telephone){
				$this->telephone=$telephone;
			}
			public function setProfil($profil){
				$this->profil=$profil;
			}
			public function setNumero_cni($numero_cni){
				$this->numero_cni=$numero_cni;
			}
            public function setNationalite($nationalite){
				$this->nationalite=$nationalite;
			}
            public function setAge($age){
				$this->age=$age;
			}
            public function setRegime_matrimo($regime_matrimo){
				$this->regime_matrimo=$regime_matrimo;
			}
            public function setEtat_sante($etat_sante){
				$this->etat_sante=$etat_sante;
			}
            public function setNbre_enfant($nbre_enfant){
				$this->nbre_enfant=$nbre_enfant;
			}
			public function setEmail($email){
				$this->email=$email;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getNom(){
				return $this->nom;
			
			}
            public function getPrenom(){
				return $this->prenom;
			
			}

			public function getDate_naissance(){
				return $this->date_naissance;
			
			}

			public function getSexe(){
				return $this->sexe;
			
			}
            public function getTelephone(){
				return $this->telephone;
			
			}
            public function getProfil(){
				return $this->profil;
			
			}
            public function getNumero_cni(){
				return $this->numero_cni;
			
			}
			public function getNationalite(){
				return $this->nationalite;
			
			}
            public function getAge(){
				return $this->age;
			
			}
            public function getRegime_matrimo(){
				return $this->regime_matrimo;
			
			}
				public function getEtat_sante(){
				return $this->etat_sante;
			
			}
			public function getNbre_enfant(){
				return $this->nbre_enfant;
			
			}
            public function getEmail(){
				return $this->email;
			
			}
						
	
}


?>
