<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Externe_user_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un externe_user

			private $id;
			private $id_users;
			private $id_entreprise;
			private $id_departement;
			private $entreprise;
			private $qualification;
			private $statut;

			protected $table= 'externe_user';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addEmployer(){

			    $this->db->set('id', $this->id)
			    	->set('id_users', $this->id_users)
			    	->set('id_entreprise', $this->id_entreprise)
			    	->set('id_departement', $this->id_departement)
			    	->set('entreprise', $this->entreprise)
			    	->set('qualification', $this->qualification)
					->set('statut', $this->statut)
					->insert($this->table);
		
			}


			// fonction qui charge tous les Employers pour faire le filtrage de donnee
			
			public function findAllEmployerBd(){
				$data = $this->db->select('id,id_users,id_entreprise,id_departement,entreprise,qualification,statut,')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_users']=$row->id_users;
			       	$donnees[$i]['id_entreprise']=$row->id_entreprise;
			       	$donnees[$i]['id_departement']=$row->id_departement;
			       	$donnees[$i]['entreprise']=$row->entreprise;
			       	$donnees[$i]['qualification']=$row->qualification;
					$donnees[$i]['statut']=$row->statut;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}


			



			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setId_users($id_users){
				$this->id_users=$id_users;
			}
			
			public function setId_entreprise($id_entreprise){
				$this->id_entreprise=$id_entreprise;
			}

			public function setId_departement($id_departement){
				$this->id_departement=$id_departement;
			}
			public function setEntreprise($entreprise){
				$this->entreprise=$entreprise;
			}
			public function setQualification($qualification){
				$this->qualification=$qualification;
			}
			public function setStatut($statut){
				$this->statut=$statut;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getId_users(){
				return $this->id_users;
			
			}

			public function getId_entreprise(){
				return $this->id_entreprise;
			
			}

			public function getId_departement(){
				return $this->id_departement;
			
			}
			public function getEntreprise(){
				return $this->entreprise;
			
			}
				public function getQualification(){
				return $this->qualification;
			
			}
			public function getStatut(){
				return $this->statut;
			
			}
						
	
}


?>