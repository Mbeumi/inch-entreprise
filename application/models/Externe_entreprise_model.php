<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Externe_entreprise_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un Externe_entreprise

			private $id;
			private $id_entreprise;
			private $id_departement;
			private $nom;
			private $telephone;
            private $email;
            private $numero_contrib;
            private $localisation;
            private $statut;
			private $reference;

			protected $table= 'externe_entreprise';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addExterne_entreprise(){

			    $this->db->set('id', $this->id)
			    	->set('id_entreprise', $this->id_entreprise)
			    	->set('id_departement', $this->id_departement)
			    	->set('nom', $this->nom)
			    	->set('telephone', $this->telephone)
					->set('email', $this->email)
                    ->set('numero_contrib', $this->numero_contrib)
                    ->set('localisation', $this->localisation)
                    ->set('statut', $this->statut)
                    ->set('reference', $this->reference)
					->insert($this->table);
		
			}


			// fonction qui charge tous les Externe_entreprises pour faire le filtrage de donnee
			
			public function findAllExterne_entrepriseBd(){
				$data = $this->db->select('id,id_entreprise,id_departement,nom,telephone,email,numero_contrib,localisation,statut,reference,')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_entreprise']=$row->id_entreprise;
			       	$donnees[$i]['id_departement']=$row->id_departement;
			       	$donnees[$i]['nom']=$row->nom;
                    $donnees[$i]['telephone']=$row->telephone;
                    $donnees[$i]['email']=$row->email;
                    $donnees[$i]['numero_contrib']=$row->numero_contrib;
                    $donnees[$i]['localisation']=$row->localisation;
					$donnees[$i]['statut']=$row->statut;
                    $donnees[$i]['reference']=$row->reference;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}


			



			// setteurs


			public function setId($id){
				$this->id=$id;
			}
			
			public function setId_entreprise($id_entreprise){
				$this->id_entreprise=$id_entreprise;
			}

			public function setId_departement($id_departement){
				$this->id_departement=$id_departement;
			}
			public function setNom($nom){
				$this->nom=$nom;
			}

			public function setTelephone($telephone){
				$this->telephone=$telephone;
			}

			public function setEmail($email){
				$this->email=$email;
			}
            public function setNumero_contrib($numero_contrib){
				$this->numero_contrib=$numero_contrib;
			}
            public function setLocalisation($localisation){
				$this->localisation=$localisation;
			}
            public function setStatut($statut){
				$this->statut=$statut;
			}
            public function setReference($reference){
				$this->reference=$reference;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			public function getId_entreprise(){
				return $this->id_entreprise;
			
			}

			public function getId_departement(){
				return $this->id_departement;
			
			}
			public function getNom(){
				return $this->nom;
			
			}
				public function getTelephone(){
				return $this->telephone;
			
			}
			public function getEmail(){
				return $this->email;
			
			}
						
			public function getNumero_contrib(){
				return $this->numero_contrib;
			
			}
            			
			public function getLocalisation(){
				return $this->localisation;
			
			}
            			
			public function getStatut(){
				return $this->statut;
			
			}
            			
			public function getReference(){
				return $this->reference;
			
			}
            			
						
	
}


?>