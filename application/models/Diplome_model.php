<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Diplome_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un Diplome de l'employer

			private $id;
			private $id_employer;
			private $diplome;
			private $annee;
			private $etablissement;
            private $filiere;
            private $pdf_diplome;

			protected $table= 'diplome';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addDiplome(){

			    $this->db->set('id', $this->id)
			    	->set('id_employer', $this->id_employer)
			    	->set('diplome', $this->diplome)
			    	->set('annee', $this->annee)
			    	->set('etablissement', $this->etablissement)
			    	->set('filiere', $this->filiere)
					->set('pdf_diplome', $this->pdf_diplome)
					->insert($this->table);
		
			}


			// fonction qui charge tous les Diplomes pour faire le filtrage de donnee
			
			public function findAllDiplomeBd(){
				$data = $this->db->select('id,id_employer,diplome,annee,etablissement,filiere,pdf_diplome,')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_employer']=$row->id_employer;
			       	$donnees[$i]['diplome']=$row->diplome;
			       	$donnees[$i]['annee']=$row->annee;
			       	$donnees[$i]['etablissement']=$row->etablissement;
			       	$donnees[$i]['filiere']=$row->filiere;
					$donnees[$i]['pdf_diplome']=$row->pdf_diplome;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}

			// fonction qui reccupère juste l'email d'un users

			// public function findUsersemail($id){
			// 	$data =$this->db->select('email')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();

								
			// 	foreach ($data as $row){
			//        	$donnees['email']=$row->email;
			// 	}

			// 	return $donnees['email'];
			// }

			// fonction qui reccupère juste le password d'un Users

			// public function findUserspassword($id){
			// 	$data =$this->db->select('password')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();

			// 		$donnees['password']='non';				
			// 	foreach ($data as $row){
			//        	$donnees['password']=$row->password;
			// 	}

			// 	return $donnees['password'];
			// }
			

		    // fonction qui reccupère juste le nom d'un Users
			
			// public function findUsersName($id){
			// 	$data =$this->db->select('nom')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();

			// 	foreach ($data as $row){
			//        	$donnees['nom']=$row->nom;
			// 	}

			// 	return $donnees['nom'];
			// }

   //          public function findUsersLastName($id){
			// 	$data =$this->db->select('prenom')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();

			// 	foreach ($data as $row){
			//        	$donnees['prenom']=$row->prenom;
			// 	}

			// 	return $donnees['prenom'];
			// }

			// // fonction qui reccupère juste le profil d'un Users
			
			// public function findProfil($id){
			// 	$data =$this->db->select('profil')
			// 					->from($this->table)
			// 					->where('id', $id)
			// 					->limit(1)
			// 					->get()
			// 					->result();
						
			// 	foreach ($data as $row){
			//        	$donnees['profil']=$row->profil;
			// 	}

			// 	return $donnees['profil'];
			// }


			


			// setteurs


			public function setId($id){
				$this->id=$id;
			}

			public function setId_employer($id_employer){
				$this->id_employer=$id_employer;
			}
			
			public function setDiplome($diplome){
				$this->diplome=$diplome;
			}

			public function setAnnee($annee){
				$this->annee=$annee;
			}
            public function setEtablissement($etablissement){
				$this->etablissement=$etablissement;
			}
            public function setFiliere($filiere){
				$this->filiere=$filiere;
			}
			public function setPdf_diplome($pdf_diplome){
				$this->pdf_diplome=$pdf_diplome;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getId_employer(){
				return $this->id_employer;
			
			}
            public function getDiplome(){
				return $this->diplome;
			
			}

			public function getAnnee(){
				return $this->annee;
			
			}

			public function getEtablissement(){
				return $this->etablissement;
			
			}
            public function getFiliere(){
				return $this->filiere;
			
			}
            public function getPdf_diplome(){
				return $this->pdf_diplome;
			
			}
						
	
}


?>