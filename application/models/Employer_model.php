<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Employer_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un employer

			private $id;
			private $id_users;
			private $id_entreprise;
			private $id_departement;
			private $identifiant;
			private $password;
			private $statut;
			private $id_responsabilite;


			protected $table= 'employer';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addEmployer(){

			    $this->db->set('id', $this->id)
			    	->set('id_users', $this->id_users)
			    	->set('id_entreprise', $this->id_entreprise)
			    	->set('id_departement', $this->id_departement)
			    	->set('identifiant', $this->identifiant)
			    	->set('password', $this->password)
					->set('statut', $this->statut)
					->set('id_responsabilite', $this->id_responsabilite)
					->insert($this->table);
		
			}


			// fonction qui charge tous les Employers pour faire le filtrage de donnee
			
			public function findAllEmployerBd(){
				$data = $this->db->select()
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_users']=$row->id_users;
			       	$donnees[$i]['id_entreprise']=$row->id_entreprise;
			       	$donnees[$i]['id_departement']=$row->id_departement;
			       	$donnees[$i]['identifiant']=$row->identifiant;
			       	$donnees[$i]['password']=$row->password;
					$donnees[$i]['statut']=$row->statut;
					$donnees[$i]['id_responsabilite']=$row->id_responsabilite;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}


			public function findEmployer($cible){
				$data = $this->db->select('*')
						->from($this->table)
						->where('id_entreprise',$cible)
						->get()
						->result();
				$i=0;
				$donnees['data']='non';	
				
				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$i][$key]=$value;
					}
					$donnees['data']='ok';
					$i++;
				}
				$donnees['total']=$i;
				return $donnees;
			}


			public function findDepartementEmployer($cible){
				$data = $this->db->select('id_departement')
						->from($this->table)
						->where('id',$cible)
						->get()
						->result();
				
				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}

				}
				
				return $donnees;
			}


			public function findResposabiliteIdEmployer($cible){
				$data = $this->db->select('id_responsabilite')
						->from($this->table)
						->where('id_users',$cible)
						->get()
						->result();
				
				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}

				}
				
				return $donnees;
			}

			public function findAllInfosEmployer($cible){
				$data = $this->db->select('*')
						->from($this->table)
						->where('id_users',$cible)
						->get()
						->result();
				
				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}

				}
				
				return $donnees;
			}
			

			public function findResponsabiliteEmployer($cible){
				$data = $this->db->select('id_responsabilite')
						->from($this->table)
						->where('id',$cible)
						->get()
						->result();
				
				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}

				}
				
				return $donnees;
			}


			public function UpdateEmployer($cible){
				$this->db->set('id_entreprise', $this->id_entreprise)
				 ->set('id_departement', $this->id_departement)
				 ->set('identifiant', $this->identifiant)
				 ->set('id_responsabilite', $this->id_responsabilite)
				 ->where('id_users',$cible)
				 ->update($this->table);
				 
			}


			public function Updatestatutemployer($cible){
				$this->db->set('statut',$this->statut)
						->where('id',$cible)
						->update($this->table);
			}



			



			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setId_users($id_users){
				$this->id_users=$id_users;
			}
			
			public function setId_entreprise($id_entreprise){
				$this->id_entreprise=$id_entreprise;
			}

			public function setId_departement($id_departement){
				$this->id_departement=$id_departement;
			}
			public function setIdentifiant($identifiant){
				$this->identifiant=$identifiant;
			}
			public function setPassword($password){
				$this->password=$password;
			}  
			public function setStatut($statut){
				$this->statut=$statut;
			}
			public function setId_responsabilite($id_responsabilite){
				$this->id_responsabilite=$id_responsabilite;
			}

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getId_users(){
				return $this->id_users;
			
			}

			public function getId_entreprise(){
				return $this->id_entreprise;
			
			}

			public function getId_departement(){
				return $this->id_departement;
			
			}
			public function getIdentifiant(){
				return $this->identifiant;
			
			}
				public function getPassword(){
				return $this->password;
			
			}
			public function getStatut(){
				return $this->statut;
			
			}
			
			public function getId_responsabilite(){
				return $this->id_responsabilite;
			
			}
	
}


?>