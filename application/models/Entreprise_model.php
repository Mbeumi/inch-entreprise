<?php 
	if (!defined('BASEPATH')) exit('No direct script access allowed');

class Entreprise_model extends CI_Model{
	
	function __construct()
			{
			
			}
		
			// gerer une entreprise 

			private $id;
			private $nom;
			private $reference;
			private $localisation;
			private $date_creation;
            private $id_proprietaire;
            private $id_categorie;
            private $numero_contrib;
            private $statut;
			private $id_secteur;
			private $profil;

			protected $table= 'entreprise';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addEntreprise(){

			    $this->db->set('id', $this->id)
			    	->set('nom', $this->nom)
			    	->set('reference', $this->reference)
			    	->set('localisation', $this->localisation)
			    	->set('date_creation', $this->date_creation)
			    	->set('id_proprietaire', $this->id_proprietaire)
					->set('id_categorie', $this->id_categorie)
                    ->set('numero_contrib', $this->numero_contrib)
                    ->set('statut', $this->statut)
					->set('id_secteur', $this->id_secteur)
					->set('profil', $this->profil)
					->insert($this->table);
		
			}


			// fonction pour recuperer les informations de toutes les entreprises enregistrés
			
			public function findAllEntrepriseBd(){
				$data = $this->db->select('id,nom,reference,localisation,date_creation,id_proprietaire,id_categorie,numero_contrib,statut,id_secteur,profil,')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['reference']=$row->reference;
			       	$donnees[$i]['localisation']=$row->localisation;
			       	$donnees[$i]['date_creation']=$row->date_creation;
			       	$donnees[$i]['id_proprietaire']=$row->id_proprietaire;
					$donnees[$i]['id_categorie']=$row->id_categorie;
                    $donnees[$i]['numero_contrib']=$row->numero_contrib;
                    $donnees[$i]['statut']=$row->statut;
					$donnees[$i]['id_secteur']=$row->id_secteur; 
					$donnees[$i]['profil']=$row->profil;
                    
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}

	//  fonction pour recperer les informations d'une entreprise specifique en fonction d'un proprietaire


			public function findEntrepriseInfos($cible){
				$data = $this->db->select('*')
						->from($this->table)
						->where('id_proprietaire', $cible)
						->get()
						->result();
	
					
			 	foreach ($data as $row){
			 		foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}
			 	}
				return $donnees;
			 }



		// recuperer les entreprises qui ont une meme categories



			public function findEntreprisecategories($cible){
				$data = $this->db->select()
						->from($this->table)
						->where('id_categorie', $cible)
						->get()
						->result();
				$i=0;
				$donnees['data']='non';

				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$i][$key]=$value;
					}
				   $donnees['data']='ok';
				   $i++;
				}
				$donnees['total']=$i;
				return $donnees;
			}


			public function findEntreprisesecteur($cible){
				$data = $this->db->select()
						->from($this->table)
						->where('id_secteur', $cible)
						->get()
						->result();
				$i=0;
				$donnees['data']='non';

				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$i][$key]=$value;
					}
				   $donnees['data']='ok';
				   $i++;
				}
				$donnees['total']=$i;
				return $donnees;
			}

		//  fonction pour recperer les informations d'une entreprise specifique 

			public function findEntreprise($cible){
				$data = $this->db->select()
						->from($this->table)
						->where('id', $cible)
						->limit(1)
						->get()
						->result();
						
				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}
				}
				return $donnees;
			}

	//  fonction pour mettre a jour les informations d'une entreprise 
	
			public function UpdateEntreprise($cible){
				$this->db->set('nom', $this->nom)
				 ->set('reference', $this->reference)
				 ->set('localisation', $this->localisation)
				 ->set('id_proprietaire', $this->id_proprietaire)
				 ->set('id_categorie', $this->id_categorie)
				 ->set('id_secteur', $this->id_secteur)
				 ->set('numero_contrib', $this->numero_contrib)
				 ->set('profil', $this->profil)
				 ->where('id',$cible)
				 ->update($this->table);
				 
			}


			public function UpdateEntrepriseproprietaire($cible){
				$this->db->set('nom', $this->nom)
				 ->set('localisation', $this->localisation)
				 ->set('profil', $this->profil)
				 ->where('id',$cible)
				 ->update($this->table);
				 
			}

				//  fonction pour mettre a jour le statut d'une entreprise 
			
			public function Updatestatutentreprise($cible){
				$this->db->set('statut',$this->statut)
						 ->where('id',$cible)
						 ->update($this->table);
			}


			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setNom($nom){
				$this->nom=$nom;
			}
			
			public function setReference($reference){
				$this->reference=$reference;
			}

			public function setLocalisation($localisation){
				$this->localisation=$localisation;
			}
            public function setDate_creation($date_creation){
				$this->date_creation=$date_creation;
			}
            public function setId_proprietaire($id_proprietaire){
				$this->id_proprietaire=$id_proprietaire;
			}
			public function setId_categorie($id_categorie){
				$this->id_categorie=$id_categorie;
			}
			public function setNumero_contrib($numero_contrib){
				$this->numero_contrib=$numero_contrib;
			}
            public function setStatut($statut){
				$this->statut=$statut;
			}
			public function setId_secteur($id_secteur){
				$this->id_secteur=$id_secteur;
			}

			public function setProfil($profil){
				$this->profil=$profil;
			}


			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getNom(){
				return $this->nom;
			
			}
            public function getReference(){
				return $this->reference;
			
			}

			public function getLocalisation(){
				return $this->localisation;
			
			}

			public function getDate_creation(){
				return $this->date_creation;
			
			}
            public function getId_proprietaire(){
				return $this->id_proprietaire;
			
			}
            public function getId_categorie(){
				return $this->id_categorie;
			
			}
            public function getNumero_contrib(){
				return $this->numero_contrib;
			
			}
			public function getStatut(){
				return $this->statut;
			
			}

			public function getId_secteur(){
				return $this->id_secteur;
			
			}

			public function getProfil(){
				return $this->profil;
			
			}			
	
}

?>