<?php 
	if (!defined('BASEPATH')) exit('No direct script access allowed');

class Departement_model extends CI_Model{
	
	function __construct()
			{
			
			}
		
			// gerer un departement 

			private $id;
			private $nom;
			private $reference;
			private $chef_departement;
			private $id_entreprise;
			private $date_creation;
            private $statut;

			protected $table= 'departement';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addDepartement(){

			    $this->db->set('id', $this->id)
			    	->set('nom', $this->nom)
			    	->set('reference', $this->reference)
			    	->set('id_entreprise', $this->id_entreprise)
                    ->set('statut', $this->statut)
					->set('chef_departement', $this->chef_departement)
					->set('date_creation', $this->date_creation) 
					->insert($this->table);
		
			}


			// fonction qui charge tous les Departement pour faire le filtrage de donnee
			
			public function findAllDepartementBd(){
				$data = $this->db->select()
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['reference']=$row->reference;
			       	$donnees[$i]['id_entreprise']=$row->id_entreprise;
                    $donnees[$i]['statut']=$row->statut;
					$donnees[$i]['chef_departement']=$row->chef_departement;
					$donnees[$i]['date_creation']=$row->date_creation;
                    
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}


			// fonctions pour recuperer tous les elements d'un departement en fonction de l'entreprise

			public function findDepartement($cible){
				$data = $this->db->select()
						->from($this->table)
						->where('id_entreprise', $cible)
						->get()
						->result();
				$i=0;
				$donnees['data']='non';		
				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$i][$key]=$value;
					}
					$donnees['data']='ok';
					$i++;
				}
				$donnees['total']=$i;
				return $donnees;
			}
             

			public function findDepartementAllId($cible){
				$data = $this->db->select()
						->from($this->table)
						->where('id', $cible)
						->get()
						->result();
				$i=0;
				$donnees['data']='non';		
				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$i][$key]=$value;
					}
					$donnees['data']='ok';
					$i++;
				}
				$donnees['total']=$i;
				return $donnees;
			}

			public function findDepartementId($cible){
				$data = $this->db->select()
						->from($this->table)
						->where('id', $cible)
						->limit(1)
						->get()
						->result();
	
				foreach ($data as $row){
					foreach($row as $key=>$value){
						$donnees[$key]=$value;
					}

				}
				
				return $donnees;
			}


			//  fonctions pour modifier les informations d'un departement


			public function UpdateDepartement($cible){
				$this->db->set('nom',$this->nom)
						->set('reference',$this->reference)
						->where('id',$cible)
						->update($this->table);
			}

			//  fonction pour supprimer un departement


			public function deleteDepartement($cible){
		    	$this->db->where('id',$cible)
		    			->delete($this->table);
		    }

			


			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setNom($nom){
				$this->nom=$nom;
			}
			
			public function setReference($reference){
				$this->reference=$reference;
			}

            public function setId_entreprise($id_entreprise){
				$this->id_entreprise=$id_entreprise;
			}
            public function setStatut($statut){
				$this->statut=$statut;
			}

			public function setChef_departement($chef_departement){
				$this->chef_departement=$chef_departement;
			}

			public function setDate_creation($date_creation){
				$this->date_creation=$date_creation;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getNom(){
				return $this->nom;
			
			}
            public function getReference(){
				return $this->reference;
			
			}

			public function getId_entreprise(){
				return $this->id_entreprise;
			
			}
			
			public function getStatut(){
				return $this->statut;
			
			}

			public function getChef_departement(){
				return $this->chef_departement;
			
			}

			public function getDate_creation(){
				return $this->date_creation;
			
			}
						
	
}

?>