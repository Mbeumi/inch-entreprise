<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ADMIN | IncH Entreprise</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <?php echo admin_bt_css("css/bootstrap.min"); ?>
  <!-- Theme style -->
  <?php echo admin_dist_css("css/AdminLTE.min"); ?>
  <?php echo admin_dist_css("css/skins/_all-skins.min"); ?>
  <!-- iCheck -->
  <?php echo admin_plugins_css("iCheck/flat/blue"); ?>
  <!-- Morris chart -->
  <?php echo admin_plugins_css("morris/morris"); ?>
  <!-- jvectormap -->
  <?php echo admin_plugins_css("jvectormap/jquery-jvectormap-1.2.2"); ?>
  <!-- Date Picker -->
  <?php echo admin_plugins_css("datepicker/datepicker3"); ?>
  <!-- Daterange picker -->
  <?php echo admin_plugins_css("daterangepicker/daterangepicker"); ?>
  <!-- bootstrap wysihtml5 - text editor -->
  <?php echo admin_plugins_css("bootstrap-wysihtml5/bootstrap3-wysihtml5.min"); ?>
  <?php echo css('app/admin_style'); ?>
  <?php echo css('app/admin_style'); ?>
  <?php echo js('app/jquery.min'); ?>

</head>
<body class="hold-transition login-page">
      <div class="login-box">
        <div class="login-logo">
          <a href="<?php echo site_url(array('Welcome','index')); ?>"><b>ADMIN |  </b>IncH Entreprise</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
          <p class="login-box-msg">Connectez vous pour démarrer votre session</p>

          <form action="<?php echo site_url(array('Administration','manageConnexion')) ?>" method="post">
            <div class="input-group mb-3">
              <input type="email" class="form-control" placeholder="Adresse e-mail"  name="email" required>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" class="form-control" placeholder="mot de passe"  name="password" required>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-8">
                <div class="icheck-primary">
                  <input type="checkbox" id="remember">
                  <label for="remember">
                   Remember Me
                  </label>
                </div>
              </div>
                <!-- /.col -->
              <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block">Connexion</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
        </div>
      </div>

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url()."assets/admin/"; ?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url()."assets/admin/"; ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url()."assets/admin/"; ?>dist/js/adminlte.min.js"></script>
</body>
</html>

