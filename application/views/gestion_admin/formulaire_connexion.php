<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ADMIN | IncH Entreprise</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <?php echo admin_plugins_css('datatables-bs4/css/dataTables.bootstrap4.min'); ?>
  <?php echo admin_plugins_css('datatables-responsive/css/responsive.bootstrap4.min'); ?>
  <?php echo admin_plugins_css('datatables-buttons/css/buttons.bootstrap4.min'); ?>
  <?php echo admin_plugins_css('fontawesome-free/css/all.min'); ?>
  <?php echo admin_plugins_css('sweetalert2-theme-bootstrap-4/bootstrap-4.min'); ?>
  <?php echo admin_plugins_css('toastr/toastr.min'); ?>
  <!-- Theme style -->
  <?php echo admin_dist_css("css/AdminLTE.min"); ?>
  <?php echo admin_dist_css("css/skins/_all-skins.min"); ?>
  <!-- iCheck -->
  <?php echo admin_plugins_css("iCheck/flat/blue"); ?>
  <!-- Morris chart -->
  <?php echo admin_plugins_css("morris/morris"); ?>
  <!-- jvectormap -->
  <?php echo admin_plugins_css("jvectormap/jquery-jvectormap-1.2.2"); ?>
  <!-- Date Picker -->
  <?php echo admin_plugins_css("datepicker/datepicker3"); ?>
  <!-- Daterange picker -->
  <?php echo admin_plugins_css("daterangepicker/daterangepicker"); ?>
  <!-- bootstrap wysihtml5 - text editor -->
  <?php echo admin_plugins_css("bootstrap-wysihtml5/bootstrap3-wysihtml5.min"); ?>
  <?php echo css('app/admin_style'); ?>
  <!-- style -->
  <?php echo css('style'); ?>
  <?php
  echo js('app/jquery.min');
  ?>

</head>

<body class="">
  <div class="container">
    <section>
      <div class="row">
        <nav class="navbar navbar-expand-lg">
          <div class="navbar-brand">
            <div class="container">
              <?php echo img('logo.png', 'logo', 'imglogo')?>
              <h1 class="nomlogo">INCH | ENTREPRISE</h1>
            </div>

            <div class="hero-unit text-center">
              <span class="desclogo"> Bienvenu dans votre espace d'Administration</span>
            </div>
          </div>
        </nav>
      </div>
    </section>
    <section>
      <div class="row cnte1">
        <div class="col-md-6">
          <?php echo img('bg3.png', 'img', 'img3') ?>
        </div>
        <div class="col-md-6">

          <div class="shadow p-4 mb-4 bg-white rounded-lg border border-warning border-top-0 border-bottom-0">
            <div class="login-logo">
              <a href="<?php echo site_url(array('Welcome', 'index')); ?>"><b>ADMIN | </b>IncH Entreprise</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
            <?php  if (isset($_SESSION['message_error'])){ ?>
            <p class="login-box-msg "  style="color:red; text-align:center !important;"><span class="fas fa-exclamation-triangle"></span>
              <?php  echo $_SESSION['message_error']; } ?> 
            </p>
              <p class="login-box-msg">Connectez vous pour démarrer votre session</p>

              <form action="<?php echo site_url(array('Administration', 'manageConnexion')) ?>" method="post">
                <div class="container">
                  <div class="input-group mb-3">
                    <input type="email" class="form-control" placeholder="Adresse e-mail" name="email" required>
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                      </div>
                    </div>
                  </div>
                  <div class="input-group mb-3">
                    <input type="password" class="form-control" placeholder="mot de passe" name="password" required>
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-8">
                      <div class="icheck-primary">
                        <input type="checkbox" id="remember">
                        <label for="remember">
                          Se souvenir de moi
                        </label>
                      </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                      <button type="submit" class="btn btn-warning btn-block">Connexion</button>
                    </div>
                    <!-- /.col -->
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- Main Footer -->
  <div class="fixed-bottom">
    <div class="d-flex flex-column"></div>
    <hr class="hr1">
    <footer class="footer py-2 d-flex justify-content-center">
      <div class="container">
        <div class="row">
          <div class="col-6 dy-flex align-Items-center">
            <p class="text-black">
              <strong>Copyright &copy; 2021 <a class="foot" href="https://InchEntreprise.com">Inch Entreprise</a></strong>
            </p>
          </div>
          <div class="col-6">
            <div class="float-right d-none d-sm-inline-block">
              <b>Version</b> 1.0.0
            </div>
          </div>
        </div>
      </div>
    </footer>

  </div>

  <!-- jQuery -->
  <script src="../../plugins/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url() . "assets/admin/"; ?>plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url() . "assets/admin/"; ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../../dist/js/adminlte.min.js"></script>
  <script src="<?php echo base_url() . "assets/admin/"; ?>dist/js/adminlte.min.js"></script>
</body>

</html>