<!-- Content Ajouter une entreprise  -->
<div class="content-wrapper">
  <form action="<?php echo site_url(array('Administration', 'EnregEntreprise')) ?>" method="post" enctype="multipart/form-data">
    <div class="container-fluid">
      <div class="content-header">
        <div class="row mb-2">
          <div class="col-sm-6">

          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a class="foot" href="<?php echo site_url(array('Administration', 'index')) ?>">Tableau de bord</a></li>
              <li class="breadcrumb-item active">Ajouter une entreprise</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div>
      <div class="card ">
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              <?php if (isset($_SESSION['message_save'])) { ?>
                <span class="fas fa-check-circle" style="color:green; padding-left:20px;">
                <?php echo $_SESSION['message_save'];
              } ?>
                </span>


                <?php if (isset($_SESSION['message_error'])) { ?>
                  <span style="color:red;" class="fas fa-exclamation-triangle">
                  <?php echo $_SESSION['message_error'];
                } ?>
                  </span>
            </div>
          </div>
        </div>
      </div>
      <div class="card card-warning">
        <div class="card-header">
          <h3 class="" style="text-align:center !important; font-weight:bold; color:#263238;">Formulaire de création d'entreprise</h3>
        </div>

        <div class="row">
          <div class="card-body">
            <div class="row">
              <div class="col-6">
                <label for="nom"> Nom</label>
                <input type="text" class="form-control" placeholder="Entrer votre nom" name="nom" id="nom">
              </div>
              <div class="col-3 float-right">
                <label for="profil">Photo de profil</label>
                <input type="file" required="" placeholder="Entrez une image de profil" name="profil" class="form-control" id="profil" accept="image/*" onchange="loadFile(event)">
              </div>
              <div class="col-3 float-right">
                <img id="img" style="width: 150px; height: 150px;">
              </div>
            </div>
            <div class="row">
              <div class="col-6">
                <div class="form-group">
                  <label for="categorie"> Categorie </label>
                  <select id="categorie" class="form-control select2" style="width: 100%;" required="" name="id_categorie">
                    <option disabled selected="selected">choix obligatoire</option>
                    <?php
                    for ($i = 0; $i < $Allcategorie['total']; $i++) {  ?>
                      <option value="<?php echo $Allcategorie[$i]['id'] ?>"><?php echo $Allcategorie[$i]['nom'] ?></option>
                    <?php  } ?>
                  </select>
                </div>
              </div>
              <div class="col-6">
                <label for="reference">Reference</label>
                <input type="text" class="form-control" placeholder="Entrer votre reference" name="reference" id="reference">
              </div>
            </div>
            <div class="row">
              <div class="col-6">
                <div class="form-group">
                  <label for="secteur"> Secteur </label>
                  <select id="secteur" class="form-control select2" style="width: 100%;" required="" name="id_secteur">
                    <option disabled selected="selected">choix obligatoire</option>
                    <?php
                    for ($i = 0; $i < $Allsecteur['total']; $i++) {  ?>
                      <option value="<?php echo $Allsecteur[$i]['id'] ?>"><?php echo $Allsecteur[$i]['nom'] ?></option>
                    <?php  } ?>
                  </select>

                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <label for="proprietaire"> Proprietaire </label>
                  <select id="proprietaire" class="form-control select2" style="width: 100%;" required="" name="id_proprietaire">
                    <option disabled selected="selected">choix obligatoire</option>
                    <?php
                    for ($i = 0; $i < $Allproprietaire['total']; $i++) {  ?>
                      <option value="<?php echo $Allproprietaire[$i]['id'] ?>"><?php echo $Allproprietaire[$i]['infos']['nom'] . ' ' . $Allproprietaire[$i]['infos']['prenom'] ?></option>
                    <?php  } ?>
                  </select>
                </div>

              </div>
            </div>
            <div class="row">
              <div class="col-6">
                <label for="numero_contrib"> Numero du Contribuable</label>
                <input type="text" class="form-control" placeholder="Entrer numero du Contribuable" id="numero_contrib" name="numero_contrib">
                <input type="hidden" class="form-control" value="1" name="statut">
              </div>
              <!-- <div class="col-4">
                <label for="date_creation">Date de Creation</label>
                <input type="date" class="form-control" placeholder="Date de création" id="date_creation" name="date_creation">
              </div> -->
              <div class="col-6">
                <label for="localisation">Localisation</label>
                <input type="text" class="form-control" placeholder="Entrer votre localisation" name="localisation" id="localisation">
              </div>
            </div>
            </br>
            <div class="card ">
              <div class="card-body">

                <div class="row">
                  <div class="col-4">
                  </div>
                  <div class="col-4">
                    <input type="submit" class="form-control btn btn-warning" style="text-align:center !important; font-weight:bold; color:#263238;">
                  </div>
                  <div class="col-4">
                  </div>
                </div>
              </div>
            </div>

            <!-- /.card-body -->
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<?php unset($_SESSION['message_save'], $_SESSION['message_error']); ?>
<script type="text/javascript">
  var loadFile = function(event) {
    var profil = document.getElementById('img');
    profil.src = URL.createObjectURL(event.target.files[0]);
  };
</script>