<style>
  .overlay {
      position: fixed;
      left: 0px;
      top:0px;
      width:100%;
      background-color: rgba(0,0 ,0 , 0.5);
      z-index:1;
      display:none;
  }


  .btnClose {
      float: right;
      font-size:16pt;
      cursor: pointer;
      color: rgb(26, 26, 26);
  }
</style>

<div class="content-wrapper">
<section class="content">
      <div class="container-fluid">
      <div class="content-header">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0">Tableau de bord</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a class="text-warning" href="<?php echo site_url(array('Administration','index')) ?>">Tableau de bord</a></li>
                <li class="breadcrumb-item "><a class="text-warning" href="<?php echo site_url(array('Administration','touteslesentreprises')) ?>">Toutes les entreprises</a></li>
                <li class="breadcrumb-item active">Profil_entrep.</li>
              </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div>
      <div class="row">
          <div class="col-sm-4  col-md-8"></div>
          <div class="col-sm-8 col-sx-12 col-md-4">
            
         
              <form action="<?php echo site_url(array('Administration','modifProfilEntreprises')) ?>" method="POST" style="display:inline-block;">
                <input type="hidden" name="cible" value="<?php echo $Allentreprise['id']?>">
                <button class="btn btn-warning" type="submit">Modifier</button>
              </form>
              <button class="btn btn-warning "  id="btnPopup2" <?php if($Allentreprise['statut']==1){ echo "disabled=''"; }else{  } ?>>Activer</button>
              <button class="btn btn-warning" id="btnPopup3" <?php if($Allentreprise['statut']==2){ echo "disabled=''"; }else{  } ?>>Désactiver</button> 
                
              <!--  Pop up pour activer une entreprise -->

                <div id="overlay3" class="overlay content-wrapper" >
                  <div class="content-fluid" style="width:100% !important;">
                    <div id="popup" class="row " style="margin-top:300px; width:100% !important;">
                      <div class="col-4"></div>
                      <div class="col-4" style="background-color:white; border-radius:10px;">
                        <h2>
                          <span id="btnClose2" class="btnClose">&times;</span>
                        </h2>
                        <p style="text-align:center; font-size:20px; ">Voulez-vous véritablement Activer cette entreprise ?</p> 
                        <center style="margin-bottom:10px;">
                          <form action="<?php echo site_url(array('Administration','ActivationEntreprise')) ?>" method="POST" style="display:inline-block;">
                            <input type="hidden" name="cible" value="<?php echo $Allentreprise['id'] ?>">
                            <input type="hidden"  name="statut"  value="1">
                            <Button class="btn btn-warning mr-1 " type="submit">Oui</Button>
                            <?php  $_SESSION['message_save'] = "Activation réalisée avec succes";
					                  $this->session->mark_as_flash('message_save'); ?>
                          </form>

                          <Button class="btn btn-warning float-center" id="btnClose2">Non</Button>
                        </center>
                      </div>
                      <div class="col-4"></div>
                    </div>
                  </div>
                </div>

                <!--  Pop up pour Desactivation d'une entreprise -->

                <div id="overlay4" class="overlay content-wrapper" >
                  <div class="content-fluid" style="width:100% !important;">
                    <div id="popup" class="row " style="margin-top:300px; width:100% !important;">
                      <div class="col-4"></div>
                      <div class="col-4" style="background-color:white; border-radius:10px;">
                        <h2>
                          <span id="btnClose3" class="btnClose">&times;</span>
                        </h2>
                        <p style="text-align:center; font-size:20px; ">Voulez-vous véritablement Desactiver cette entreprise ?</p> 
                        <center style="margin-bottom:10px;">
                          <form action="<?php echo site_url(array('Administration','DesactivationEntreprise')) ?>" method="POST" style="display:inline-block;">
                            <input type="hidden" name="cible" value="<?php echo $Allentreprise['id'] ?>">
                            <input type="hidden"  name="statut" value="2">
                            <Button class="btn btn-warning mr-1  " style="color:black" type="submit">Oui</Button>
                            <?php  $_SESSION['message_save'] = "Desactivation réalisée avec succes";
					                  $this->session->mark_as_flash('message_save'); ?>
                          </form>
                          
                          <Button class="btn btn-warning float-center" id="btnClose3"><?php  echo "<a style='color:black !important;' href=\"javascript:history.go(-1)\">Non</a>" ?></Button>
                        </center> 
                      </div>
                      <div class="col-4"></div>
                    </div>
                  </div>
                  
                </div>
          </div>
      </div><br>
          <div class="row">
            <!-- Profile Image -->
            <div class="col-12 card card-warning card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img style="border-radius:50%; height:70px; width:70px;"
                  src="<?php echo img_url('user_profil/'.$Allentreprise['profil']) ?>"
                  alt="User profile picture">
                </div>
                <div class="row">
                  <div class="col-3"></div>
                  <div class="col-md-6 col-sx-12 col-sm-12">
                    <h3 class="profile-username text-center"><?php echo $Allentreprise['nom'] ?></h3>
                    <p class="text-muted text-center"><?php echo $Allentreprise['reference'] ?></p>
                    <ul class="list-group list-group-unbordered ">
                      <li class="list-group-item">
                        <b>date:</b><a class="float-right"><?php echo $Allentreprise['date_creation'] ?></a>
                      </li>
                      <li class="list-group-item">
                        <b>Email: </b><a class="float-right"><?php echo $Allproprietaire['infos']['email'] ?></a>
                      </li>
                    </ul>
                  </div>
                  <div class="col-3"></div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          
       
          <div class="row">

            <!-- Profile Image -->
            <div class="col-12 card card-warning ">
              <div class="card-body box-profile">
                <div class="row align-items-center">
                  <div class="col-1"></div>
                  <div class="col-md-5 col-sx-12 col-sm-6 ">
                    <p><b> Nom:  </b>  <?php echo $Allentreprise['nom']?></p><br>
                    <p><b> Reference:  </b>  <?php echo $Allentreprise['reference']?></p><br>
                    <p><b> Proprietaire:  </b>  <?php echo $Allproprietaire['infos']['nom']." ".$Allproprietaire['infos']['prenom']?></p><br>
                    <p><b>Telephone:  </b><?php echo $Allproprietaire['infos']['telephone'] ?></p><br>
                  </div>
                  <div class="col-md-5 col-sx-12 col-sm-6 align-items-center">
                    <p><b> Numero contribuable:  </b>  <?php echo $Allentreprise['numero_contrib']?></p><br>
                    <p><b> Categorie:  </b>  <?php echo $Allcategorie['nom']?></p><br>
                    <p><b> Secteur:  </b>  <?php echo $Allsecteur['nom']?></p><br>
                    <p><b> Localisation:  </b>  <?php echo $Allentreprise['localisation']?></p>
                  </div>
                  <div class="col-1"></div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>     
          
      </div><!-- /.container-fluid -->
    </section>
    </div>
  
    
    </div>
  </div>
</div>


 <!--  javascript pop up pour Activer une entreprise -->
 <script>
  var btnPopup2 = document.getElementById('btnPopup2');
  var overlay3 = document.getElementById('overlay3');
  btnPopup2.addEventListener('click',openMoadl);
  function openMoadl() {
  overlay3.style.display='block';
  }
  var btnClose2 = document.getElementById('btnClose2');
  btnClose2.addEventListener('click',closeModal);
  function closeModal() {
  overlay3.style.display='none';
  }
</script>

<!--  javascript pop up pour Desactiver une entreprise -->
<script>
  var btnPopup3 = document.getElementById('btnPopup3');
  var overlay4 = document.getElementById('overlay4');
  btnPopup3.addEventListener('click',openMoadl);
  function openMoadl() {
  overlay4.style.display='block';
  }
  var btnClose3 = document.getElementById('btnClose3');
  btnClose3.addEventListener('click',closeModal);
  function closeModal() {
  overlay4.style.display='none';
  }
</script>