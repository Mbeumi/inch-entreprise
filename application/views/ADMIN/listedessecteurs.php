<style>

  .overlay3{
      position: fixed;
      left: 0px;
      top:0px;
      width:100%;
      background-color: rgba(0,0 ,0 , 0.5);
      z-index:1;
      display:none;
  }


  .btnClose3 {
      float: right;
      font-size:16pt;
      cursor: pointer;
      color: rgb(26, 26, 26);
  }
</style>


<div class="content-wrapper">
  <div class="content-fluid">
    <div class="content-header">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0">Tableau de bord</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a class="text-warning" href="<?php echo site_url(array('Administration','index')) ?>">Tableau de bord</a></li>
              <li class="breadcrumb-item active">listes des secteurs</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    <div class="card ">
      <div class="card-body">
        <div class="row">
          <div class="col-md-11 col-sm-11 col-xs-11">
            <?php   if (isset($_SESSION['message_save'])){ ?>
              <span class="fas fa-check-circle" style="color:green; padding-left:20px;">
                <?php    echo $_SESSION['message_save'];} ?>
              </span>
            <?php  if (isset($_SESSION['message_error'])){?>
              <span class="fas fa-exclamation-triangle" style="color:red; padding-left:20px;">
                <?php    echo $_SESSION['message_error'];}?>
              </span>
          </div>
        </div>
      </div>
    </div>
    <div class="card card-warning">
        <div class=" card-header ">
          <h3 class="" style="text-align:center !important; font-weight:bold; color:#263238;">Secteurs d'activités</h3>
        </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped ">
                  <thead>
                    <div class="float-right btn btn-warning" data-toggle="modal" data-target="#modal-danger" type="button">
                      <i class="fas fa-plus">Ajouter un secteur</i>
                    </div><br><br>
                    <tr>
                      <th class="text-center">N° </th>
                      <th class="text-center">Intitulé </th>
                      <th class="text-center">Date_creation </th>
                      <th class="text-center">Date_modification </th>
                      <th class="text-center">Action </th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php  $j=1;  
                    if ($Allsecteur['data']=='ok') {
                        for($i=0; $i<$Allsecteur['total']; $i++) {?>
                            <tr>
                                <td class="text-center"> <?php  echo $j; ?> </td>
                                <td class="text-center"> <?php echo $Allsecteur[$i]['nom'];  ?> </td>
                                <td class="text-center"> <?php  echo $Allsecteur[$i]['date_creation']; ?></td>
                                <td class="text-center"> <?php  echo $Allsecteur[$i]['date_modif']; ?></td>
                                <td class="text-center">
                                  <form action="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','formulairemodifSecteur')); }?>" method="post" style="display:inline-block;">
                                    
                                    <button type="button" title="Editer " class="bouttom"  data-toggle="modal" data-target="#modal-primary-<?php  echo $i ?>"><i class="fas fa-edit"></i> </button> 
                                  </form>
                                    
                                    <button type="button" id="btnPopup3-<?php echo $i ?>" title="SUPPRIMER" ><i class="fa fa-trash"></i> </button>
                                </td>
                            </tr>
                            
                            <div class="modal fade" id="modal-primary-<?php  echo $i ?>">
                              <br><br><br><br><br><br><br><br>
                              <div class="modal-dialog modal-info">
                                <div class="modal-content">   
                                  <div class="modal-header">
                                    <h4 class="modal-title">Modifier un secteur</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action="<?php echo site_url(array('Administration','formulairemodifSecteur')) ?>" method="post" enctype="multipart/form-data">
                                    <div class="modal-body">
                                      <input type="hidden" value="<?php echo $Allsecteur[$i]['id'] ?>" name="id">
                                      <label for="nom">Intitule</label>
                                      <input class="form-control" id="nom" type="text" value="<?php echo $Allsecteur[$i]['nom'] ?>" name="nom" required="">
                                      <br>
                                      <input type="hidden" value="<?php echo date('Y-m-d H:i:s') ?>" name="date_modif">                         
                                    </div>
                                    <div class="modal-footer ">
                                      <button type="submit" class=" form-control btn btn-warning">Modifier</button>
                                    </div>
                            
                                  </form>
                                </div>
                              </div>
                            </div>
                            <div id="overlay3-<?php echo $i ?>" class="overlay3 content-wrapper" >
                              <div class="content-fluid" style="width:100% !important;">
                                <div id="popup" class="row " style="margin-top:300px; width:100% !important;">
                                
                                  <div class="col-4"></div>
                                  <div class="col-4" style="background-color:white; ">
                                    <h2>
                                      <span id="btnClose3-<?php echo $i ?>" class="btnClose3">&times;</span>
                                    </h2>
                                    <p style="text-align:center; font-size:20px; ">Voulez-vous véritablement Supprimer cette categorie?</p> 
                                    <center style="margin-bottom:10px;">
                                      <form action="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','supprimerSecteur')); } ?>" method="post" style="display:inline-block;">
                                        <input type="hidden" value="<?php if($Allentreprise[$i]['total']==0){
                                           echo $Allsecteur[$i]['id']; 
                                           }else{ echo "0"; }  ?>" name="id">
                                        <Button class="btn btn-warning mr-1 " type="submit">Oui</Button>
                                      </form>
                                      
                                      <Button class="btn btn-warning float-center" id="btnClose4-<?php echo $i ?>">Non</Button>
                                    </center>
                          
                                    
                                  </div>
                                  <div class="col-4"></div>
                              </div>
                            </div>
                            <?php $j++ ?>
                            <script>
                              var btnPopup3 = document.getElementById('btnPopup3-<?php echo $i ?>');
                                var overlay3 = document.getElementById('overlay3-<?php echo $i ?>');
                                btnPopup3.addEventListener('click',openMoadl);
                                function openMoadl() {
                                overlay3.style.display='block';
                                }
                                var btnClose3 = document.getElementById('btnClose3-<?php echo $i ?>');
                                btnClose3.addEventListener('click',closeModal);
                                function closeModal() {
                                overlay3.style.display='none';
                                }
                                var btnClose4 = document.getElementById('btnClose4-<?php echo $i ?>');
                                btnClose4.addEventListener('click',closeModal);
                                function closeModal() {
                                overlay3.style.display='none';
                                }
                            </script>
                        <?php } ?>
                      <?php }else{
                      $_SESSION['message erreur']="Aucun secteur existant";
                    } ?>
                  </tbody>
                </table>
                <div class="modal fade modal-info" id="modal-danger">
              <br><br><br><br><br><br><br><br>
                <div class="modal-dialog modal-info">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ajouter un secteur</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form action="<?php echo site_url(array('Administration','EnregSecteur')) ?>" method="post" enctype="multipart/form-data">
                      <div class="modal-body">
                        <label for="nom">Intitulé</label>
                        <input class="form-control" type="text" placeholder="Intitulé" id="nom" name="nom" required="">
                        <br>
                        <input type="hidden"  name="date_modif"  value="Aucun modification">                         
                      </div>
                      <div class="modal-footer ">
                        <button type="submit" class=" form-control btn btn-warning">Ajouter</button>
                      </div>
                    </form>
                  </div>
                </div>
    </div>
              </div>  
            </div>
      </div>
  </div>


