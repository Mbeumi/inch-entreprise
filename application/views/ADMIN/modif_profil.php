<div class="content-wrapper" >
  <div class="container-fluid">  
    <div class="content-header">
          <div class="row mb-2">
            <div class="col-sm-6">
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a class="text-warning" href="<?php echo site_url(array('Administration','index')) ?>">Tableau de bord</a></li>
                <li class="breadcrumb-item "><a class="text-warning" href="<?php echo site_url(array('Administration','Allproprietaire')) ?>">Tous les proprietaires</a></li>
                <li class="breadcrumb-item "><a class="text-warning" href="<?php echo site_url(array('Administration','Allproprietaire')) ?>">Profil</a></li>
                <li class="breadcrumb-item active">Modification</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div>
    <form action="<?php echo site_url(array('Administration','EnregmodifProprietaire')) ?>" method="post" enctype="multipart/form-data">

      <div class="card card-warning">
              <div class="card-header">
                <h3 class="" style="text-align:center !important; font-weight:bold; color:#263238;"> Modification du profil </h3>
              </div>
              <div class="card-body">

                <div class="row">
                  <div class="col-5">
                    <label for="nom"> Nom</label>
                    <input type="text" class="form-control" placeholder="Entrer votre nom" name="nom" id="nom" required="" value="<?php echo $Allproprietaire['infos']['nom']?>">
                  </div>
                  <div class="col-5">
                      <label for="profil">Photo de profil</label>
                      <input type="hidden"   name="profil_pass" value="<?php echo $Allproprietaire['infos']['profil'] ?>" >
                      <input type="file"  placeholder="Entrez une image de profil"   name="profil" class="form-control" id="profil" accept="image/*" onchange="loadFile(event)">
                  </div>
                  <div class="col-2">
                    <img id="img" src="<?php echo img_url('user_profil/'.$Allproprietaire['infos']['profil']) ?>" style="width: 150px; height: 150px;">
                  </div>
                </div>
                <div class="row">
                  <div class="col-3">
                    <label for="prenom"> Prenom</label>
                    <input type="text" class="form-control" placeholder="Entrer votre prenom" name="prenom" id="prenom" required=""  value="<?php echo $Allproprietaire['infos']['prenom']?>">
                  </div>
                  <div class="col-3">
                    <label for="date_naissance"> Date de naissance</label>
                    <input type="date" class="form-control" placeholder="Date de naissance" id="date_naissance" name="date_naissance" required=""  value="<?php echo $Allproprietaire['infos']['date_naissance']?>">
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="sexe"> sexe</label>
                      <select class="form-control select2" style="width: 100%;" required="" name="sexe"  value="<?php echo $Allproprietaire['infos']['sexe']?>">
                        <option selected="selected"><?php echo $Allproprietaire['infos']['sexe']?></option>
                        <option>Homme</option>
                        <option>Femme</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <label for="Age"> Age</label>
                    <input type="number" class="form-control" placeholder="Exemple: 18 ans" id="age" name="age" required=""  value="<?php echo $Allproprietaire['infos']['age']?>"> 
                  </div>
                </div>
                <div class="row">
                  <div class="col-4">
                    <div class="form-group">
                      <label for="nationalite"> Nationalité</label>
                      <select class="form-control select2" style="width: 100%;" required="" name="nationalite"  value="<?php echo $Allproprietaire['infos']['nationalite']?>">
                        <option selected="selected"><?php echo $Allproprietaire['infos']['nationalite']?></option>
                        <option>Camerounaise</option>
                        <option>Congolaise</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="numero_cni"> Numero_cni</label>
                      <input type="text" class="form-control" placeholder="Entrer votre numero de cni" id="numero_cni" name="numero_cni" required=""  value="<?php echo $Allproprietaire['infos']['numero_cni']?>">
                    </div>
                     
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="telephone"> Telephone</label>
                      <input type="tel" class="form-control" placeholder="Entrer numero de telephone" id="telephone" name="telephone" required=""  value="<?php echo $Allproprietaire['infos']['telephone']?>">
                    </div>
                     
                  </div>
                </div>
                <div class="row">
                  <div class="col-4">
                    <label for="etat_civil"> Etat civil</label>
                    <select id="etat_civil" class="form-control select2" style="width: 100%;" required="" name="regime_matrimo"  value="<?php echo $Allproprietaire['infos']['regime_matrimo']?>">
                        <option selected="selected"><?php echo $Allproprietaire['infos']['regime_matrimo']?></option>
                        <option>Celibataire</option>
                        <option>Marié</option>
                        <option>Divorcé</option>
                    </select>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="etat_physique"> Etat physique </label>
                      <select id="etat_physique" class="form-control select2" style="width: 100%;" required="" name="etat_sante"  value="<?php echo $Allproprietaire['infos']['etat_sante']?>">
                        <option selected="selected"><?php echo $Allproprietaire['infos']['etat_sante']?></option>
                        <option>Apte</option>
                        <option>Handicape</option>
                      </select>
                    </div>
                    <input type="hidden"  name="nbre_enfant" value="Aucune reponse">
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="profession"> Profession</label>
                      <input type="text" class="form-control" placeholder="Entrer votre profession" id="profession" name="profession" required=""  value="<?php echo $Allproprietaire['profession'] ?>">
                    </div>
                  </div>  
                  </div>
                </div>
              </div>
              <div class="card ">
                <div class="card-header">
                  <h3 class="card-title font-weight-bold">Informations de connexion</h3>
                </div>
                <div class="card-body">
                  <div class="row">
                  <div class="col-4">
                    <label for="identifiant"> Identifiant</label>
                    <input type="text" class="form-control" placeholder="Entrer un identifiant" name="identifiant" id="identifiant" required=""  value="<?php echo $Allproprietaire['identifiant'] ?>">
                  </div>
                  <div class="col-4">
                    <label for="email"> Email</label>
                    <input type="email" class="form-control" placeholder="Entrer votre email" name="email" id="email" required=""  value="<?php echo $Allproprietaire['infos']['email'] ?>">
                  </div>
                  <div class="col-4">
                    <label for="password"> Password</label>
                    <input type="password" class="form-control" placeholder="Entrer un mot de pass" name="password" id="password" required=""  value="<?php echo $Allproprietaire['password'] ?>">
                    <input type="hidden"  name="statut" value="1">
                    <input type="hidden" name="id_users" value="<?php echo $Allproprietaire['id_users'] ?>">
                    <input type="hidden" name="id" value="<?php echo $Allproprietaire['id'] ?>">
                  </div>
                </div>
              </div>
            </div>
              </br>
              <div class="card ">
                <div class="card-body">

                  <div class="row">
                    <div class="col-4">
                      
                    </div>
                    <div class="col-4">
                      <input type="submit" class="form-control btn btn-warning" style="text-align:center !important; font-weight:bold; color:#263238;" value="Modifier">
                    </div>
                    <div class="col-4">
                     
                    </div>
                  </div>
                
                     
                  
                </div>
              </div>
              <!-- /.card-body -->
        </div>
      </div>
    </form>
    <?php unset($_SESSION['message_save'],$_SESSION['message_error'] ); ?>
  <script type="text/javascript">
    var loadFile = function(event) {
    var profil = document.getElementById('img');
    profil.src = URL.createObjectURL(event.target.files[0]);
    };
  </script>