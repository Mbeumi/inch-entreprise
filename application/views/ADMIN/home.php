<div class="content-wrapper">
  <br>
    <section class="content ">
      <div class="container-fluid ">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <a href="<?php echo site_url(array('Administration','listedescategories')) ?>">
              <div class="info-box">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-list-alt"></i></span>
                <div class="info-box-content">
                  <span class="text-secondary">Catégories d'entreprises</span>
                  <span class="text-secondary info-box-number" style="font-size:20px;"><?php echo $Allcategorie['total']?> </span>
                </div>
              
              </div>
            </a>
            

           
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <a href="<?php echo site_url(array('Administration','listedessecteurs')) ?>">
              <div class="info-box mb-3">
                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-tasks"></i></span>

                <div class="info-box-content">
                  <span class="text-secondary info-box-text">Secteurs d'activités</span>
                  <span class="text-secondary info-box-number" style="font-size:20px;"><?php echo $Allsecteur['total']?></span>
                </div>
              </div>
            </a>

            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <a href="<?php echo site_url(array('Administration','Allproprietaire')) ?>">
              <div class="info-box mb-3">
                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>

                <div class="info-box-content">
                  <span class="text-secondary info-box-text">Propriétaires</span>
                  <span class="text-secondary info-box-number" style="font-size:20px;"><?php echo $Allproprietaire['total']?></span>
                </div>
              </div>
            </a>

            
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <a href="<?php echo site_url(array('Administration','touteslesentreprises')) ?>">
              <div class="info-box mb-3">
                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-industry"></i></span>

                <div class="info-box-content">
                  <span class="text-secondary info-box-text">Entreprises</span>
                  <span class="text-secondary info-box-number" style="font-size:20px;"><?php echo $Allentreprise['total']?></span>
                </div>
              </div>
            </a>

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">
          <!-- Left col -->

          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>

 
  </div>


   
  