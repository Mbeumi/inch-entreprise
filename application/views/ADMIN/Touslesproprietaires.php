<div class="content-wrapper">
    <div class="content">
		<div class="content-header">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!-- <h1 class="m-0">Tableau de bord</h1> -->
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a class="text-warning" href="<?php echo site_url(array('Administration','index')) ?>">Tableau de bord</a></li>
                        <li class="breadcrumb-item active">Tous les propriétaires</li>
                    </ol>
                    </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
        <div class="card ">
          <div class="card-body">
              <div class="row">
                    <div class="col-12">
                     <?php   if (isset($_SESSION['message_save'])){ ?>
                      <span class="fas fa-check-circle" style="color:green; padding-left:20px;">
                         <?php    echo $_SESSION['message_save'];
                          } ?>
                      </span>
                      
                          
                      <?php if (isset($_SESSION['message_error'])){?>
                            <span style="color:red;" class="fas fa-exclamation-triangle">
                            <?php  echo $_SESSION['message_error'];
                          } ?>
                      </span>
                    </div>
                  </div>
                </div>
        </div>
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6">
                <div class="form-group">
                    <div class="input-group input-group-lg">
                        <input type="text" class="form-control form-control-lg" placeholder="Recherche" id="auto1">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-lg btn-default">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-3"></div>
        </div>
		<br>
        <div class="row">
            <?php 
                for ($i=0; $i <$Allproprietaire['total']; $i++) { ?>
                    <div class="col-md-3 ">
                        <div class="card card-warning card-outline">
                            <div class="card-body box-profile ">
                                <div class="text-center">
                                    <img style="border-radius:50%; height:60px; width:60px;"
                                    src="<?php echo img_url('user_profil/'.$Allproprietaire[$i]['infos']['profil']) ?>"
                                    alt="User profile picture">
                                </div>

                                <h3 class="profile-username text-center"><?php echo $Allproprietaire[$i]['infos']['nom'].' '.$Allproprietaire[$i]['infos']['prenom'] ?></h3>

                                <p class="text-muted text-center"><?php echo $Allproprietaire[$i]['profession'] ?><br>
                                <?php if ($Allproprietaire[$i]['statut'] == 1){ 
							          	      	echo " <span style='color:green;'>Actif</span>" ;
							          	      } else {
							          	      		echo " <span style='color:red;'>Bloqué</span>";
							          	      } ?> </p>
                                <form action="<?php echo site_url(array('Administration','profil_proprietaire')) ?>" method="post">
                                    <input type="hidden" value="<?php echo $Allproprietaire[$i]['id']?>" name="cible"/>
                                    <button type="submit" class="btn btn-warning btn-block" style="font-weight:bold">Profil</button>
                                </form> 
                            </div>
                        </div>
                    </div>   
            <?php  } ?>
            
        </div>
    </div> 
</div>
<script>
    var states = [
	'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
	'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
	'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
	'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
	'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
	'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
	'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
	'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
	'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
    ];

    $('#auto1').autocomplete({
	source:[states]
    });
    /*********************************** local end *****************************************************/


    /*********************************** remote start *****************************************************/
    $('#remote_input').autocomplete({
    valueKey:'title',
    source:[{
	url:"http://xdsoft.net/jquery-plugins/?task=demodata&s=%QUERY%",
	type:'remote',
	getValueFromItem:function(item){
		return item.title
	},
	ajax:{
		dataType : 'jsonp'	
	}
    }]});

    $('#open').click(function(){
	    $('#remote_input').trigger('updateContent.xdsoft');
	    $('#remote_input').trigger('open.xdsoft');
	    $('#remote_input').focus();
    });
/*********************************** remote end *****************************************************/

    $('#auto2').autocomplete({
	    source:[{
		    data:states,
            preparse:function(items,query,sourse){
                for(i in items)
                    items[i]=items[i].replace('a','b');
                return items;
            },
            filter:function(items,query){
                var res = [];
                for(i in items){
                    if( items[i]==query )
                        res.push(items[i]);
                }
                return res;
            }
        }]
    });


    $('#combine').autocomplete();

    $('#combine')
        .autocomplete('setSource',[
            {
                valueKey:'title',
                url:"",
                type:'remote',
                getValue:function(item){
                    return item.title
                },
                ajax:{
                    dataType : 'jsonp'
                }
            },
            [],
            []
        ]);
	
    $('#combine')
        .autocomplete('setSource',states,1)
        .autocomplete('setSource',['one','two','three','for','five','six','seven','eight','nine','zero'],2);
        
    var first = $('#combine')
        .autocomplete('getSource',0);
    first.url = "http://xdsoft.net/jquery-plugins/?task=demodata&s=%QUERY%";
    
    var states1 = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California']
    var states2 = ['Colorado', 'Connecticut']
    $("#select_course").autocomplete({source:[states1], visibleHeight:200});
    $("#select_unit").autocomplete({source:[states1]});
    $('#select_course').on('selected.xdsoft', function(event, item){
    $("#select_unit").autocomplete('setSource',states2,0); 
    });
</script>