<!-- Content Ajouter une entreprise  -->
<div class="content-wrapper">
  <form action="<?php echo site_url(array('Administration', 'EnregmodifEntreprise')) ?>" method="post" enctype="multipart/form-data">
    <div class="container-fluid">
      <div class="content-header">
          <div class="row mb-2">
            <div class="col-md-6 col-sm-3 col-sx-12">
              
            </div><!-- /.col -->
            <div class="col-md-6 col-sm-9 col-sx-12">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a class="text-warning" href="<?php echo site_url(array('Administration','index')) ?>">Tableau de bord</a></li>
                <li class="breadcrumb-item"> <a class="text-warning" href="<?php echo site_url(array('Administration','touteslesentreprises')) ?>">Toutes les entreprises</a></li>
                <li class="breadcrumb-item "><a class="text-warning" href="<?php echo site_url(array('Administration','touteslesentreprises')) ?>">Profil</a></li>
                <li class="breadcrumb-item active">Modification </li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div>

        <div class="card card-warning">
          <div class="card-header">
            <h3 class="" style="text-align:center !important; font-weight:bold; color:#263238;">Formulaire de modification d'entreprise</h3>
          </div>
          
            <div class="row">
              <div class="card-body">
                <div class="row">
                  <div class="col-6">
                    <label for="nom"> Nom</label>
                    <input type="text" class="form-control" placeholder="Entrer votre nom" name="nom" id="nom" value="<?php echo $Allentreprise['nom'] ?>">
                  </div>
                  <div class="col-3 float-right">
                    <label for="profil">Photo de profil</label>
                    <input type="hidden"  name="profil_pass" value="<?php echo $Allentreprise['profil'] ?>">
                    <input type="file" placeholder="Entrez une image de profil" name="profil" class="form-control" id="profil" accept="image/*" onchange="loadFile(event)">
                  </div>
                  <div class="col-3 float-right">
                    <img id="img"  style="width: 150px; height: 150px;"
                  src="<?php echo img_url('user_profil/'.$Allentreprise['profil']) ?>">
                  </div>
                </div>
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label for="categorie"> Categorie </label>
                      <select id="categorie" class="form-control select2" style="width: 100%;"   name="id_categorie">
                        <option  value="<?php echo $Allcategorie['id']?>"   selected="selected"><?php echo $Allcategorie['nom']?></option>
                            <?php 
                              for ($i=0; $i <$Allcategorie1['total'] ; $i++) {  ?>
                                <option value="<?php echo $Allcategorie1[$i]['id'] ?>"><?php echo $Allcategorie1[$i]['nom'] ?></option>
                            <?php  }?>
                      </select>
                    </div>
                  </div>
                  <div class="col-6">
                    <label for="reference">Reference</label>
                    <input type="text" class="form-control" value="<?php echo $Allentreprise['reference']?>" placeholder="Entrer votre reference" name="reference" id="reference">
                  </div>
              </div>
            <div class="row">
              <div class="col-6">
                <div class="form-group">
                  <label for="secteur"> Secteur </label>
                  <select id="secteur" class="form-control select2" style="width: 100%;"  name="id_secteur">
                    <option  value="<?php echo $Allsecteur['id']?>" selected="selected"><?php echo $Allsecteur['nom']?></option>
                    <?php
                    for ($i = 0; $i < $Allsecteur1['total']; $i++) {  ?>
                      <option value="<?php echo $Allsecteur1[$i]['id'] ?>"><?php echo $Allsecteur1[$i]['nom'] ?></option>
                    <?php  } ?>
                  </select> 

                 </div>
              </div> 
              <div class="col-6">
                <div class="form-group">
                  <label for="proprietaire"> Proprietaire </label>
                  <select id="proprietaire" class="form-control select2" style="width: 100%;"  name="id_proprietaire">
                    <option   value="<?php echo $Allproprietaire['id'] ?>"  selected="selected"><?php echo $Allproprietaire['infos']['nom'] . ' ' . $Allproprietaire['infos']['prenom'] ?></option>
                    <?php
                    for ($i = 0; $i < $Allproprietaire1['total']; $i++) {  ?>
                      <option value="<?php echo $Allproprietaire1[$i]['id'] ?>"><?php echo $Allproprietaire1[$i]['infos']['nom'] . ' ' . $Allproprietaire1[$i]['infos']['prenom'] ?></option>
                    <?php  } ?>
                  </select>
                </div>

              </div>
            </div>
            <div class="row">
              <div class="col-4">
                <label for="numero_contrib"> Numero du Contribuable</label>
                <input type="number" class="form-control" value="<?php echo $Allentreprise['numero_contrib']?>" placeholder="Entrer numero du Contribuable" id="numero_contrib" name="numero_contrib">
                <input type="hidden" class="form-control" value="1" name="statut">
              </div>
              <div class="col-4">
                <label for="date_creation">Date de Creation</label>
                <input class="form-control" name="date_creation" id="date_creation" value="<?php echo $Allentreprise['date_creation']?>"  disabled >
              </div>
              <div class="col-4">
                <label for="localisation">Localisation</label>
                <input type="text" class="form-control" value="<?php echo $Allentreprise['localisation']?>"  placeholder="Entrer votre localisation" name="localisation" id="localisation">
              </div>
            </div> 
            </br>
            <div class="card ">
              <div class="card-body">

                <div class="row">
                  <div class="col-4">
                  </div>
                  <div class="col-4">
                    <input type="hidden" name="id" value="<?php echo $Allentreprise['id'] ?>">
                    <input type="submit" class="form-control btn btn-warning" style="text-align:center !important; font-weight:bold; color:#263238;" value="Modifier">
                  </div>
                  <div class="col-4">
                  </div>
                </div>
              </div>
            </div>
          
          <!-- /.card-body -->
        </div>
      </div>
    </div>
  </div>
  </form>
</div>
  <?php unset($_SESSION['message_save'],$_SESSION['message_error'] ); ?>
  <script type="text/javascript">
    var loadFile = function(event) {
    var profil = document.getElementById('img');
    profil.src = URL.createObjectURL(event.target.files[0]);
    };
  </script>
