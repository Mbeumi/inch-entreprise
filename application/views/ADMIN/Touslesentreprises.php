<div class="content-wrapper">
    <div class="content">
        <div class="content-header">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1 class="m-0">Tableau de bord</h1> -->
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a class="text-warning" href="<?php echo site_url(array('Administration','index')) ?>">Tableau de bord</a></li>
                            <li class="breadcrumb-item active">Toutes les entreprise</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
        </div>
        <div class="card ">
          <div class="card-body">
              <div class="row">
                    <div class="col-12">
                     <?php   if (isset($_SESSION['message_save'])){ ?>
                      <span class="fas fa-check-circle" style="color:green; padding-left:20px;">
                         <?php    echo $_SESSION['message_save'];
                          } ?>
                      </span>
                      
                          <?php
                          if (isset($_SESSION['message_error'])){?>
                            <span style="color:red;" class="fas fa-exclamation-triangle">
                            <?php    echo $_SESSION['message_error'];
                          } ?>
                      </span>
                    </div>
                  </div>
                </div>
        </div>
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6">
                <div class="form-group">
                    <div class="input-group input-group-lg">
                        <input type="text" class="form-control form-control-lg" placeholder="Recherche" id="auto1">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-lg btn-default">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3"></div>
        </div>
        <br>
        <div class="row">
            <?php 
                for ($i=0; $i <$Allentreprise['total']; $i++) { ?>
                    <div class="col-md-3 ">
                        <div class="card card-warning card-outline">
                            <div class="card-body box-profile ">
                                <div class="text-center">
                                    <img style="border-radius:50%; height:80px; width:80px;"
                                    src="<?php echo img_url('user_profil/'.$Allentreprise[$i]['profil']) ?>"
                                    alt="User profile picture">
                                </div>

                                <h3 class="profile-username text-center"><?php echo $Allentreprise[$i]['nom'] ?></h3>

                                <p class="text-muted text-center"><?php echo $Allentreprise[$i]['reference'] ?><br>
                                <?php if ($Allentreprise[$i]['statut'] == 1){ 
							          	      	echo " <span style='color:green;'>Activé</span>" ;
							          	      } else {
							          	      		echo " <span style='color:red;'>Desactivé</span>";
							          	      } ?> </p>

                                <!-- <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item text-center">
                                    <b>Followers</b> <a class="float-right">1,322</a>
                                    </li>
                                    <li class="list-group-item text-center">
                                        <b>Following</b> <a class="float-right">543</a>
                                    </li>
                                    <li class="list-group-item text-center">
                                        <b>Friends</b> <a class="float-right">13,287</a>
                                    </li>
                                </ul> -->
                                <form action="<?php echo site_url(array('Administration','profilentreprises')) ?>" method="post">
                                    <input type="hidden" value="<?php echo $Allentreprise[$i]['id']?>" name="cible"/>
                                    <button type="submit" class="btn btn-warning btn-block" style="font-weight:bold">Profil</button>
                                </form> 
                            </div>
                        </div>
                    </div>   
            <?php  } ?>
            
        </div>
    </div> 
</div>