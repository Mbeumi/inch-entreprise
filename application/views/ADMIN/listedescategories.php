<style>

  .overlay2{
      position: fixed;
      left: 0px;
      top:0px;
      width:100%;
      background-color: rgba(0,0 ,0 , 0.5);
      z-index:1;
      display:none;
  }


  .btnClose2 {
      float: right;
      font-size:16pt;
      cursor: pointer;
      color: rgb(26, 26, 26);
  }
</style>


<div class="content-wrapper">
  <div class="content-fluid">
    <div class="content-header">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0">Tableau de bord</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a class="text-warning" href="<?php echo site_url(array('Administration','index')) ?>">Tableau de bord</a></li>
              <li class="breadcrumb-item active">listes categories</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    <div class="card ">
      <div class="card-body">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <?php   if (isset($_SESSION['message_save'])){ ?>
              <span class="fas fa-check-circle" style="color:green; padding-left:20px;">
                <?php    echo $_SESSION['message_save']; } ?>
              </span>     
            <?php  if (isset($_SESSION['message_error'])){?>
              <span class="fas fa-exclamation-triangle" style="color:red; padding-left:20px;">
                <?php    echo $_SESSION['message_error']; }?>
              </span>
          </div>         
        </div>
      </div>
    </div>

    <div class="card card-warning">
      <div class=" card-header ">
        <h3 class="" style="text-align:center !important; font-weight:bold; color:#263238;"> Catégories d'entreprises</h3>
      </div>
              <!-- /.card-header -->
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped ">
          <thead>
            <div class="float-right btn btn-warning" data-toggle="modal" data-target="#modal-primary" type="button">
              <i class="fas fa-plus"> Ajouter une categorie</i>
            </div><br><br>
            <tr>
              <th class="text-center">N° </th>
              <th class="text-center">Intitulé </th>
              <th class="text-center">Date_creation </th>
              <th class="text-center">Date_modification </th>
              <th class="text-center">Action </th>
            </tr>
            </thead>
            <tbody>
                  <?php $j=1;  
                    if ($Allcategorie['data']=='ok') {
                          for($i=0; $i<$Allcategorie['total']; $i++) {?>
                            <tr>
                                <td class="text-center"> <?php  echo $Allcategorie[$i]['id']; ?> </td>
                                <td class="text-center"> <?php echo $Allcategorie[$i]['nom'];  ?> </td>
                                <td class="text-center"> <?php  echo $Allcategorie[$i]['date_creation']; ?></td>
                                <td class="text-center"> <?php  echo $Allcategorie[$i]['date_modif']; ?></td>
                                <td class="text-center">
                                  <form action="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','formulairemodifCategorie')); }?>" method="post" style="display:inline-block;">
                                    
                                    <button type="button" title="Editer " class="bouttom" data-toggle="modal" data-target="#modal-primary-<?php  echo $i ?>"><i class="fas fa-edit"></i></button> 
                                  </form>
                                
                                    
                                    <button  type="button" id="btnPopup2-<?php echo $i ?>" title="SUPPRIMER"><i class="fa fa-trash"></i></button>
                                    
                    
                                </td>
                            </tr>

                            <div class="modal fade" id="modal-primary-<?php  echo $i ?>">
                              <br><br><br><br><br><br><br><br>
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h4 class="modal-title">Modifier une catégorie</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action="<?php echo site_url(array('Administration','formulairemodifCategorie')) ?>" method="post" enctype="multipart/form-data">
                                    <div class="modal-body">
                                      <input type="hidden" value="<?php echo $Allcategorie[$i]['id'] ?>" name="id">
                                      <label for="nom">Intitule</label>
                                      <input class="form-control" id="nom
                                      " type="text" value="<?php echo $Allcategorie[$i]['nom'] ?>" name="nom" required="">
                                      <br>
                                      <input type="hidden" value="<?php echo date('Y-m-d H:i:s') ?>" name="date_modif">                                                 
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                      <button type="submit" class="form-control  btn btn-warning">Modifier</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            <div id="overlay2-<?php echo $i ?>" class="overlay2 content-wrapper" >
                              <div class="content-fluid" style="width:100% !important;">
                                <div id="popup" class="row " style="margin-top:300px; width:100% !important;">
                                
                                  <div class="col-4"></div>
                                  <div class="col-4" style="background-color:white;">
                                    <h2>
                                      <span id="btnClose2-<?php echo $i ?>" class="btnClose2">&times;</span>
                                    </h2>
                                    <p style="text-align:center; font-size:20px; ">Voulez-vous véritablement Supprimer cette categorie?</p> 
                                    <center style="margin-bottom:10px;">
                                      <form action="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','supprimerCategorie')); } ?>" method="post" style="display:inline-block;"> 
                                        <input type="hidden" value="<?php if($Allentreprise[$i]['total']==0){
                                           echo $Allcategorie[$i]['id']; 
                                           }else{ echo "0"; }  ?>" name="id">
                                        <Button class="btn btn-warning mr-1 " type="submit">Oui</Button>
                                      </form>
                                      
                                      <Button class="btn btn-warning float-center" id="btnClose3-<?php echo $i ?>">Non</Button>
                                    </center>
                          
                                    
                                  </div>
                                  <div class="col-4"></div>
                              </div>
                            </div>
                  
                

                          </div>
                          <script>
                            var btnPopup2 = document.getElementById('btnPopup2-<?php echo $i ?>');
                              var overlay2 = document.getElementById('overlay2-<?php echo $i ?>');
                              btnPopup2.addEventListener('click',openMoadl);
                              function openMoadl() {
                              overlay2.style.display='block';
                              }
                              var btnClose2 = document.getElementById('btnClose2-<?php echo $i ?>');
                              btnClose2.addEventListener('click',closeModal);
                              function closeModal() {
                              overlay2.style.display='none';
                              }
                              var btnClose3 = document.getElementById('btnClose3-<?php echo $i ?>');
                              btnClose3.addEventListener('click',closeModal);
                              function closeModal() {
                              overlay2.style.display='none';
                              }
                          </script>
                        <?php } ?>
                    <?php $j++ ?>
                    <?php } else{
                      $_SESSION['message erreur']="Aucune categorie existante";
                    } ?>
          </tbody>
        </table> 
                <div class="modal fade" id="modal-primary">
                    <br><br><br><br><br><br><br><br>
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title">Ajouter une catégorie</h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <form action="<?php echo site_url(array('Administration','EnregCategorie')) ?>" method="post" enctype="multipart/form-data">
                          <div class="modal-body">
                            <label for="nom">Intitule</label>
                            <input class="form-control" id="nom" type="text" placeholder="Intitule" name="nom" required="">
                            <br>
                            <input type="hidden"  name="date_modif"  value="Aucun modification">                         
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="submit" class="form-control  btn btn-warning">Ajouter</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
              </div>
              
      </div>
  </div>
</div>


