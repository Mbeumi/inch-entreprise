
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<br><br><br><br><br><br><br><br>
    <!-- Content Header (Page header) --> 
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-3">
          <div class="center col-2" >
          </div>
          <div class="center col-4" >
            <div class="card card-info">
              <div class="card-header">
                <h2 class="" style="text-align:center !important;">Creer une categorie d'entreprise</h2>
              </div>
              <div class="card-body">
                  <form action="<?php echo site_url(array('Administration','EnregCategorie')) ?>" method="post" enctype="multipart/form-data">
                     <input class="form-control" type="text" placeholder="Nom" name="nom" required="">
                     <input type="hidden"  name="date_modif"  value="Aucun modification">
                    <br>
                    <input class="form-control btn btn-info " type="submit" >                      
                  </form>

              </div>
            </div>
          </div>
          <div class="center col-4" >
            <div class="card card-info">
              <div class="card-header">
                <h2 class="" style="text-align:center !important;">Creer un secteur <br> d'entreprise </h2>
              </div>
              <div class="card-body">
                  <form action="<?php echo site_url(array('Administration','EnregSecteur')) ?>" method="post" enctype="multipart/form-data">
                     <input class="form-control" type="text" placeholder="Nom" name="nom" required="">
                    <br>
                    <input type="hidden"  name="date_modif"  value="Aucun modification">
                    <input class="form-control btn btn-info " type="submit"  >                      
                  </form>

              </div>
            </div>
          </div>
          <div class="center col-2" >
          </div>
        </div>
      </div>
    </div>
  </div>
