<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-warning " style="background-color: black  ;">
  <!-- Brand Logo --> 
  <a href="<?php echo site_url(array('Employer', 'index')) ?>" class="brand-link ml-3">
    <img src="<?php echo img_url('user_profil/'.$Allentreprise['profil']) ?>" style="border-radius:50%; height:35px; width: 35px;" style="opacity: .8">
    <span class="brand-text font-weight-bold"><?php echo $Allentreprise['nom'] ?></span>
  </a>
  <br>

  <!-- Sidebar -->
  <div class="sidebar">
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item ">
          <a href="#" class="nav-link ">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Tableau de bord
              <!-- <i class="right fas fa-angle-left"></i> -->
            </p>
          </a>
        </li>
        <li class="nav-item  ">
          <a href="#" class="nav-link  ">
            <i class="nav-icon fas fa-user"></i>
            <p>
              Mon profil
              <span class="badge badge-info right"></span>
            </p>
          </a>
        </li>
        <li class="nav-item  ">
          <a href="<?php echo site_url(array('Employer', 'Adddemandeconge')) ?>" class="nav-link  ">
            <i class="nav-icon fas fa-edit"></i>
            <p>
              Demande de congé
              <span class="badge badge-info right"></span>
            </p>
          </a>
        </li>
        <!-- <li class="nav-item">
          <a href="#" class="nav-link  ">
            <i class="nav-icon fas fa-industry"></i>
            <p>
              Gestion des R.H
              <i class="fas fa-angle-left right"></i>
              <span class="badge badge-info right"></span>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item  ">
              <a href="#" class="nav-link  ">
                <i class="nav-icon fas fa-industry"></i>
                <p>
                  Mon entreprise
                  <i class="fas fa-angle-left right"></i>
                  <span class="badge badge-info right"></span>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="" class="nav-link ">
                    <i class="fas fa-eye nav-icon"></i>
                    <p>Details</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="" class="nav-link ">
                    <i class="fas fa-edit nav-icon"></i>
                    <p>Modification</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="" class="nav-link ">
                    <i class="fas fa-user nav-icon"></i>
                    <p>Mon Profil</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item ">
              <a href="#" class="nav-link ">
                <i class="nav-icon fas fa-users"></i>
                <p>
                  Employers
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="" class="nav-link ">
                    <i class="fas fa-plus nav-icon"></i>
                    <p>Ajouter</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="" class="nav-link ">
                    <i class="fas fa-user nav-icon"></i>
                    <p>Les employers</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-book"></i>
                <p>
                  Externes
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="pages/examples/invoice.html" class="nav-link">
                    <i class="fas fa-users nav-icon"></i>
                    <p>Employers</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="pages/examples/e-commerce.html" class="nav-link">
                    <i class="fas fa-industry nav-icon"></i>
                    <p>Entreprises</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item ">
              <a href="#" class="nav-link ">
                <i class="fas fa-cog nav-icon"></i>
                <p>
                  Parametres
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a  href="" class="nav-link ">
                      <i class="fas fa-list nav-icon"></i>
                      <p>Departements</p>
                    </a>
                </li>
                <li class="nav-item">
                  <a href="" class="nav-link ">
                    <i class="fas fa-list nav-icon"></i>
                    <p>Responsabilites</p>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li> -->

      </ul>
    </nav>
  </div>
  <!-- /.sidebar -->
</aside>
