<div class="content-wrapper">
  <br>
    <section class="content ">
      <div class="container-fluid ">
      <div class="row">
            <!-- Profile Image -->
            <div class="col-12 card card-warning card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img style="border-radius:50%; height:120px; width:120px;"
                  src="<?php echo img_url('user_profil/'.$Alluser['profil']) ?>"
                  alt="User profile picture">
                </div>
                <div class="row">
                  <div class="col-3"></div>
                  <div class="col-md-6 col-sx-12 col-sm-6">
                    <h3 class="profile-username text-center"><?php echo $Alluser['nom'] ?></h3>
                    <p class="text-muted text-center"><?php echo $responsabilite['nom'] ?></p>
                    <ul class="list-group list-group-unbordered ">
                      <li class="list-group-item">
                        <b>Departement: </b><a class="float-right"><?php echo $departement['nom'] ?></a>
                      </li>
                      <li class="list-group-item">
                        <b>Email: </b><a class="float-right"><?php echo $Alluser['email'] ?></a>
                      </li>
                    </ul>
                  </div>
                  <div class="col-3"></div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          
       
          <div class="row">

            <!-- Profile Image -->
            <div class="col-12 card card-warning ">
              <div class="card-body box-profile">
                <div class="row">
                  <div class="col-md-1  "></div>
                  <div class=" ml-3 col-md-5 col-sx-12 col-sm-10">
                    <p class="ml-3 "><b> Nom:  </b>  <?php echo $Alluser['nom']?></p><br>
                    <p class="ml-3 "><b> Departement:  </b>  <?php echo $departement['nom']?></p><br>
                    <p class="ml-3 "><b> Sexe:  </b>  <?php echo $Alluser['sexe']?></p><br>
                    <p class="ml-3 "><b>Telephone:  </b><?php echo $Alluser['telephone'] ?></p>
                  </div>
                  <div class="col-offset-md-1 ml-3   col-md-4 col-sx-10 col-sm-10">
                    <p class="ml-3"><b> Prenom:  </b>  <?php echo $Alluser['prenom']?></p><br>
                    <p class="ml-3 "><b> Responsabilité:  </b>  <?php echo $responsabilite['nom']?></p><br>
                    <p class="ml-3 "><b> Numero de CNI:  </b>  <?php echo $Alluser['numero_cni']?></p><br>
                    <p class="ml-3 "><b> Status:  </b>  <?php echo $Alluser['regime_matrimo']?></p>
                  </div>
                  <div class="col-md-1 col-sm-1"></div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>     
      </div><!--/. container-fluid -->
    </section>
  </div>
   
  