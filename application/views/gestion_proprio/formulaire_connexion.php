<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PROPRIAITE | IncH</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 4 -->
  <?php echo admin_plugins_css('datatables-bs4/css/dataTables.bootstrap4.min'); ?>
  <?php echo admin_plugins_css('datatables-responsive/css/responsive.bootstrap4.min'); ?>
  <?php echo admin_plugins_css('datatables-buttons/css/buttons.bootstrap4.min'); ?>
  <?php echo admin_plugins_css('fontawesome-free/css/all.min'); ?>
  <?php echo admin_plugins_css('sweetalert2-theme-bootstrap-4/bootstrap-4.min'); ?>
  <?php echo admin_plugins_css('toastr/toastr.min'); ?>
  <!-- Theme style -->
  <?php echo admin_dist_css("css/AdminLTE.min"); ?>
  <?php echo admin_dist_css("css/skins/_all-skins.min"); ?>
  <!-- iCheck -->
  <?php echo admin_plugins_css("iCheck/flat/blue"); ?>
  <!-- Morris chart -->
  <?php echo admin_plugins_css("morris/morris"); ?>
  <!-- jvectormap -->
  <?php echo admin_plugins_css("jvectormap/jquery-jvectormap-1.2.2"); ?>
  <!-- Date Picker -->
  <?php echo admin_plugins_css("datepicker/datepicker3"); ?>
  <!-- Daterange picker -->
  <?php echo admin_plugins_css("daterangepicker/daterangepicker"); ?>
  <!-- bootstrap wysihtml5 - text editor -->
  <?php echo admin_plugins_css("bootstrap-wysihtml5/bootstrap3-wysihtml5.min"); ?>
  <?php echo css('app/admin_style'); ?>
  <!-- style -->
  <?php echo css('style'); ?>
  <?php echo js('app/jquery.min'); ?>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>
  <section>

  </section>
  <div class="container">
    <div class="row">
      <nav class="navbar navbar-expand-lg">
        <div class="navbar-brand">
          <div class="container">
            <?php echo img('logo.png', 'logo', 'imglogo') ?>
            <h1 class="nomlogo">INCH | ENTREPRISE</h1>
          </div>

          <h3 class="desclogo"> Prenez en main votre Entreprise</h3>
        </div>
      </nav>
    </div>
    <div class="row cnte1">
      <div class="col-md-6">
        <?php echo img('bg1.png', 'img', 'img1') ?>
      </div>
      <div class="col-md-6">

        <div class="shadow p-4 mb-4 bg-white rounded-lg border border-warning border-top-0 border-bottom-0">
          <div class="">
            <p class="text2">CONNEXION</p>
          </div>
          <!-- /.login-logo -->
          <div class="">
            <?php if (isset($_SESSION['message_error'])) { ?>
              <p class="login-box-msg " style="color:red; text-align:center !important;"><span class="fas fa-exclamation-triangle"></span>
              <?php echo $_SESSION['message_error'];
            } ?>
              </p>

              <?php if (isset($_SESSION['message_save'])) { ?>
                <p class="login-box-msg " style="color:green; text-align:center !important;"><span class="fas fa-check-circle"></span>
                 <?php echo $_SESSION['message_save'];
              } ?>
               </p>
              
              <p class="text1">Connectez-vous pour démarrer votre session</p>

              <form action="<?php echo site_url(array('Proprietaire', 'manageConnexion')) ?>" class="needs-validated" novalidate method="post">
                <div class="container">

                  <div class="form-group ">

                    <input type="text" class="form-control" placeholder="identifiant ou email" name="identifiant" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    <div class="valid-feedback">Bien.</div>
                  
                  </div>
                  <div class="form-group">

                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                    <div class="valid-feedback">Bien.</div>
                  </div>

                  <div class="row">
                    <div class="col-8">
                      <a class="text-warning" href="<?php echo site_url(array('Proprietaire', 'forgetpassword')) ?>">Mot de passe oublié !</a>
                    </div>
                    <div class="col-4 form-group justify-content-center">

                      <div class="">
                        <button type="submit" class="btn btn-warning btn-block btn-flat rounded">Connecter</button>
                      </div>
                      <!-- /.col -->
                    </div>
                  </div>

                </div>
              </form>


          </div>
          <!-- /.login-box-body -->

        </div>
      </div>
    </div>


  </div>
  <!-- Main Footer -->
  <div class="fixed-bottom">
    <div class="d-flex flex-column"></div>
    <hr class="hr1">
    <footer class="footer py-2 d-flex justify-content-center">
      <div class="container">
        <div class="row">
          <div class="col-6 dy-flex align-Items-center">
            <p class="text-black">
              <strong>Copyright &copy; 2021 <a class="foot" href="https://InchEntreprise.com">Inch Entreprise</a></strong>
            </p>
          </div>
          <div class="col-6">
            <div class="float-right d-none d-sm-inline-block">
              <b>Version</b> 1.0.0
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>

</body>

</html>