<div class="content-wrapper">
  <section class="content">
      <div class="container-fluid">
        <div class="content-header">
          <div class="row mb-2">
            <div class="col-md-6 col-sm-3 col-sx-12">
              
            </div><!-- /.col -->
            <div class="col-md-6 col-sm-9 col-sx-12">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a class="text-warning" href="<?php echo site_url(array('Proprietaire','index')) ?>">Tableau de bord</a></li>
                <li class="breadcrumb-item active">Profil entreprise </li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div>
        <div class="card ">
          <div class="card-body">
              <div class="row">
                    <div class="col-md-11 col-sm-11 col-xs-11">
                     <?php   if (isset($_SESSION['message_save'])){ ?>
                      <span class="fas fa-check-circle" style="color:green; padding-left:20px;">
                         <?php    echo $_SESSION['message_save'];
                          } ?>
                      </span>
                          <?php
                          if (isset($_SESSION['message_error'])){?>
                              <span style="color:red;" class="fas fa-exclamation-triangle">
                              <?php  echo $_SESSION['message_error'];
                          } ?>
                      </span>
                    </div>
                  </div>
                </div>
        </div>
          
          <div class="row">
            <!-- Profile Image -->
            <div class="col-12 card card-warning card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img style="border-radius:50%; height:120px; width:120px;"
                  src="<?php echo img_url('user_profil/'.$Allentreprise['profil']) ?>"
                  alt="User profile picture">
                </div>
                <div class="row">
                  <div class="col-3"></div>
                  <div class="col-md-6 col-sx-12 col-sm-6">
                    <h3 class="profile-username text-center"><?php echo $Allentreprise['nom'] ?></h3>
                    <p class="text-muted text-center"><?php echo $Allentreprise['reference'] ?></p>
                    <p class="text-muted text-center"><?php if($Allentreprise['statut']==1){
                      echo "<span class='text-sucess font-weight-bold'>Activé</span>";
                    }else{echo "<span class='text-danger font-weight-bold'>Desactivé</span>";}  ?></p>
                    <ul class="list-group list-group-unbordered ">
                      <li class="list-group-item">
                        <b>date:</b><a class="float-right"><?php echo $Allentreprise['date_creation'] ?></a>
                      </li>
                      <li class="list-group-item">
                        <b>Email: </b><a class="float-right"><?php echo $Alluser['email'] ?></a>
                      </li>
                    </ul>
                  </div>
                  <div class="col-3"></div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          
       
          <div class="row">

            <!-- Profile Image -->
            <div class="col-12 card card-warning ">
              <div class="card-body box-profile">
                <div class="row">
                  <div class="col-md-2  "></div>
                  <div class="  col-md-4 col-sx-12 col-sm-10">
                    <p ><b> Nom:  </b>  <?php echo $Allentreprise['nom']?></p><br>
                    <p ><b> Reference:  </b>  <?php echo $Allentreprise['reference']?></p><br>
                    <p ><b> Proprietaire:  </b>  <?php echo $Alluser['nom']." ".$Alluser['prenom']?></p><br>
                    <p ><b>Telephone:  </b><?php echo $Alluser['telephone'] ?></p>
                  </div>
                  <div class="col-offset-md-1    col-md-4 col-sx-10 col-sm-10">
                    <p ><b> Numero contribuable:  </b>  <?php echo $Allentreprise['numero_contrib']?></p><br>
                    <p ><b> Categorie:  </b>  <?php echo $Allcategorie['nom']?></p><br>
                    <p ><b> Secteur:  </b>  <?php echo $Allsecteur['nom']?></p><br>
                    <p ><b> Localisation:  </b>  <?php echo $Allentreprise['localisation']?></p>
                  </div>
                  <div class="col-md-1 col-sm-1"></div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>     
          
      </div><!-- /.container-fluid -->
    </section>
  </div>
  
    
  