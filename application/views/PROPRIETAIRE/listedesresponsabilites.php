<style>

  .overlay3{
      position: fixed;
      left: 0px;
      top:0px;
      width:100%;
      background-color: rgba(0,0 ,0 , 0.5);
      z-index:1;
      display:none;
  }


  .btnClose3 {
      float: right;
      font-size:16pt;
      cursor: pointer;
      color: rgb(26, 26, 26);
  }
</style>


<div class="content-wrapper">
  <div class="content-fluid">
    <div class="content-header">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0">Tableau de bord</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo site_url(array('Proprietaire','index')) ?>">Tableau de bord</a></li>
              <li class="breadcrumb-item active">listes des responsabilites</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    <div class="card ">
      <div class="card-body">
        <div class="row">
          <div class="col-md-11 col-sm-11 col-xs-11">
            <?php   if (isset($_SESSION['message_save'])){ ?>
              <span class="fas fa-check-circle" style="color:green; padding-left:20px;">
                <?php    echo $_SESSION['message_save'];} ?>
              </span>
            <?php if (isset($_SESSION['message_error'])){ ?>
              <span style="color:red; padding-left:20px;" class="fas fa-exclamation-triangle">
                <?php   echo $_SESSION['message_error'];} ?>
              </span> 
          </div>
        </div>
      </div>
    </div>
    <div class="card card-warning">
        <div class=" card-header ">
          <h3 class="" style="text-align:center !important; font-weight:bold; color:#263238;">Liste des responsabilites</h3>
        </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped ">
                  <thead>
                    <div class=" btn btn-warning float-right" data-toggle="modal" data-target="#modal-danger" type="button">
                      <i class="fas fa-plus">Ajouter une responsabilite</i>
                    </div><br><br>
                    <tr>
                      <th class="text-center">N° </th>
                      <th class="text-center">Intitulé </th>
                      <th class="text-center">Departement</th>
                      <th class="text-center">Date_creation</th>
                      <th class="text-center">niveau access</th>
                      <th class="text-center">Action </th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php  $j=1; 
                  if ($responsabilite['data']=='ok') {
                    for($i=0; $i<$responsabilite['total']; $i++) {   
                        
                          ?> 
                            <tr>
                                <td class="text-center"> <?php  echo $j; ?> </td>
                                <td class="text-center"> <?php  echo $responsabilite[$i]['nom'];  ?> </td>
                                <td class="text-center"> 
                                  <?php if($Alldepartement['data']=='ok'){
                                            echo $departement[$i]['nom']; 
                                        }else{ echo "Departement vide";}
                                  ?>
                                 </td>
                                <td class="text-center"> <?php  echo $responsabilite[$i]['date_creation']; ?></td>
                                <td class="text-center"> <?php  echo $responsabilite[$i]['statut']; ?></td>
                                <td class="text-center">
                                    
                                    <button type="button" title="Editer " class="bouttom"  data-toggle="modal" data-target="#modal-primary-<?php  echo $i ?>"><i class="fas fa-edit"></i> </button> 
              
                                    <button type="button" id="btnPopup3-<?php  echo $i ?>" title="SUPPRIMER" ><i class="fa fa-trash"></i> </button>
                                </td>
                            </tr>
                            
                            <div class="modal fade" id="modal-primary-<?php  echo $i ?>">
                              <br>
                              <div class="modal-dialog modal-info">
                                <div class="modal-content">   
                                  <div class="modal-header">
                                    <h4 class="modal-title">Modifier une responsabilite</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action="<?php echo site_url(array('Proprietaire','formulairemodifresponsabilite')) ?>" method="post" enctype="multipart/form-data">
                                    <div class="modal-body">
                                      <input type="hidden" value="<?php echo $responsabilite[$i]['id'] ?>" name="id">
                                      <label for="nom">Intitulé</label>
                                      <input class="form-control" type="text" placeholder="Intituler la responsabilite" name="nom" value="<?php echo $responsabilite[$i]['nom'] ?>" required="">
                                      <br>
                                      <label for="reference">Reference</label>
                                      <input type="text"  name="reference" id="reference" placeholder="Entrer la reference" value="<?php echo $responsabilite[$i]['reference'] ?>" required="" class="form-control"> 
                                      <br>
                                      <label for="departement">Departement</label>
                                      <select class="form-control select2" id="departement" style="width: 100%;" required="" value="<?php echo $responsabilite[$i]['id_departement'] ?>" name="id_departement">
                                        <option  value="<?php echo $responsabilite[$i]['id_departement'] ?>"><?php echo $departement[$i]['nom'] ?></option>
                                      <?php for($k=0; $k<$Alldepartement['total']; $k++){  ?>
                                        <option value="<?php echo $Alldepartement[$k]['id'] ?>"><?php echo $Alldepartement[$k]['nom'] ?></option><?php  } ?>
                                      </select>
                                      <br>
                                      <label for="statut">Niveau d'access</label>
                                      <select class="form-control select2" style="width: 100%;" required="" id="statut" value="<?php echo $responsabilite[$i]['statut'] ?>" name="statut">
                                        <option disabled="" value="<?php echo $responsabilite[$i]['statut'] ?>" selected="selected"><?php echo $responsabilite[$i]['statut'] ?></option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                      </select> 
                                      <br>
                                      <label for="description">Description</label>
                                      <textarea class="form-control" name="description" required="" id="description" cols="5" rows="5"><?php echo $responsabilite[$i]['description'] ?></textarea>
                                      <input type="hidden"  name="id_entreprise"  value="<?php echo $Allentreprise['id']?>">                        
                                    </div>
                                    <div class="modal-footer ">
                                      <button type="submit" class=" form-control btn btn-warning">Modifier</button>
                                    </div>
                            
                                  </form>
                                </div>
                              </div>
                            </div>
                            <div id="overlay3-<?php  echo $i ?>" class="overlay3 content-wrapper" >
                              <div class="content-fluid" style="width:100% !important;">
                                <div id="popup" class="row " style="margin-top:300px; width:100% !important;">
                                
                                  <div class="col-4"></div>
                                  <div class="col-4" style="background-color:white; border-radius:10px;">
                                    <h2>
                                      <span id="btnClose3-<?php  echo $i ?>" class="btnClose3">&times;</span>
                                    </h2>
                                    <p style="text-align:center; font-size:20px; ">Voulez-vous véritablement Supprimer cette responsabilité?</p> 
                                    <center style="margin-bottom:10px;">
                                      <form action="<?php if(isset($_SESSION['PROPRIETAIRE'])) { echo site_url(array('Proprietaire','supprimerresponsabilite')); } ?>" method="post" style="display:inline-block;">
                                        <input type="hidden" value="<?php echo $responsabilite[$i]['id']?>" name="id">
                                        <Button class="btn btn-warning mr-1 swalDefaultWarning" type="submit">Oui</Button>
                                      </form>
                                      
                                      <Button class="btn btn-warning float-center" id="btnClose4-<?php  echo $i ?>">Non</Button>
                                    </center>
                          
                                    
                                  </div>
                                  <div class="col-4"></div>
                              </div>
                            </div>
                          <?php $j++ ?>
                          <script>
                            var btnPopup3 = document.getElementById('btnPopup3-<?php  echo $i ?>');
                              var overlay3 = document.getElementById('overlay3-<?php  echo $i ?>');
                              btnPopup3.addEventListener('click',openMoadl);
                              function openMoadl() {
                              overlay3.style.display='block';
                              }
                              var btnClose3 = document.getElementById('btnClose3-<?php  echo $i ?>');
                              btnClose3.addEventListener('click',closeModal);
                              function closeModal() {
                              overlay3.style.display='none';
                              }
                              var btnClose4 = document.getElementById('btnClose4-<?php  echo $i ?>');
                              btnClose4.addEventListener('click',closeModal);
                              function closeModal() {
                              overlay3.style.display='none';
                              }
                          </script>
                    <?php  } 
                      }else{
                      ?>
                  </tbody>
                <?php } ?>
                </table> 
                <div class="modal fade modal-info" id="modal-danger">
                <div class="modal-dialog modal-info">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ajouter une responsabilite</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form action="<?php echo site_url(array('Proprietaire','Enregresponsabilite')) ?>" method="post" enctype="multipart/form-data">
                      <div class="modal-body">
                        <label for="nom">Intitulé</label>
                        <input class="form-control" type="text" placeholder="Intituler la responsabilite" name="nom" required="">
                        <br>
                        <label for="reference">Reference</label>
                        <input type="text"  name="reference" id="reference" placeholder="Entrer une reference" required="" class="form-control"> 
                        <br>
                        <label for="departement">Departement</label>
                        <select class="form-control select2" id="departement" style="width: 100%;" required="" name="id_departement">
                          <option disabled="" selected="selected">choix obligatoire</option>
                          <?php for($i=0; $i<$Alldepartement['total']; $i++){  ?>
                            <option value="<?php echo $Alldepartement[$i]['id'] ?>"><?php echo $Alldepartement[$i]['nom'] ?></option>
                           <?php  } ?>
                          
                          
                        </select>
                        <br>
                        <label for="statut">Niveau d'access</label>
                        <select class="form-control select2" style="width: 100%;" required="" id="statut" name="statut">
                          <option disabled="" selected="selected">choix obligatoire</option>
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                        </select> 
                        <br>
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" required="" id="description" cols="5" rows="5"></textarea>
                        <input type="hidden"  name="id_entreprise"  value="<?php echo $Allentreprise['id']?>">                       
                      </div>
                      <div class="modal-footer ">
                        <button type="submit" class=" form-control btn btn-warning">Ajouter</button>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
              </div>  
            </div>
      </div>
  </div>


