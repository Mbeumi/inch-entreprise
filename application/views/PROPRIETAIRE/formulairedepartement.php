<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <!-- general form elements -->
            <div class="card card-warning">
                <div class="card-header">
                    <h3 class="card-title">Ajouter un departement </h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="<?php echo site_url(array('Proprietaire','EnregDepartement')) ?>" method="post" enctype="multipart/form-data">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nom">Nom</label>
                            <input type="text" class="form-control" id="nom" name="nom" placeholder="Entrer le nom">
                        </div>
                        <div class="form-group">
                            <label for="reference">Reference</label>
                            <input type="reference" class="form-control" id="reference" name="reference" placeholder="reference">
                        </div>
                        <div class="form-group">
                            <input type="hidden" value="1" name="statut">
                        </div>
                        <div class="form-group">
                            <input type="hidden" value="<?php echo $Allentreprise ['id']; ?>" name="id_entreprise" >
                        </div>

                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <input type="submit" value="Envoyer" class="btn btn-warning">
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>

    </div>


</div>