<div class="content-wrapper">
  <section class="content">
      <div class="container-fluid">
        <div class="content-header">
          <div class="row mb-2">
            <div class="col-sm-6">
            <!-- <h1 class="m-0">Tableau de bord</h1> -->
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a class="text-warning" href="<?php echo site_url(array('Proprietaire','index')) ?>">Tableau de bord</a></li>
                <li class="breadcrumb-item active">Profil</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div>
        <div class="card ">
          <div class="card-body">
              <div class="row">
                    <div class="col-md-12 col-sm-12 col-sx-12">
                     <?php   if (isset($_SESSION['message_save'])){ ?>
                      <span class="fas fa-check-circle text-center" style="color:green; padding-left:20px; font-size:20px;">
                         <?php    echo $_SESSION['message_save'];
                          } ?>
                      </span>
                          <?php
                          if (isset($_SESSION['message_error'])){?>
                              <span style="color:red; padding-left:20px; font-size:20px;" class="fas fa-exclamation-triangle text-center">
                              <?php  echo $_SESSION['message_error'];
                          } ?>
                      </span>
                    </div>
                  </div>
                </div>
        </div>
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-warning card-outline">
              <div class="card-body box-profile">
              <div class="text-center">
                  <img style="border-radius:50%; height:75px; width:75px; "
                  src="<?php echo img_url('user_profil/'.$Alluser['profil']) ?>"
                  alt="User profile picture">
              </div>
              <h3 class="profile-username text-center"><?php echo $Alluser['nom'].' '.$Alluser['prenom'] ?></h3>
              <p class="text-muted text-center"><?php echo $_SESSION['PROPRIETAIRE']['profession'] ?></p>
                <ul class="list-group list-group-unbordered ">
                  <li class="list-group-item">
                    <b>Status</b><a class="float-right"><?php echo $Alluser['regime_matrimo'] ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Profession: </b><a class="float-right"><?php echo $_SESSION['PROPRIETAIRE']['profession'] ?></a>
                  </li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-body">
                <div class="tab-content">
                  <div class="row">
                    <div class="col-md-6 ">
                      <br>
                      <p><b> Nom:  </b>  <?php echo $Alluser['nom']?></p><br>
                      <p><b> Date de naissance:  </b>  <?php echo $Alluser['date_naissance']?></p><br>
                      <p><b> Numero de CNI:  </b>  <?php echo $Alluser['numero_cni']?></p><br>
                      <p><b>Telephone:  </b><?php echo $Alluser['telephone'] ?></p>
                      
                      
                    </div>
                    <div class="col-md-6">
                      <br>
                      <p><b> Prenom:  </b>  <?php echo $Alluser['prenom']?></p><br>
                      <p><b> Nationalité:  </b>  <?php echo $Alluser['nationalite']?></p><br>
                      <p><b> Sexe:  </b>  <?php echo $Alluser['sexe']?></p><br>
                      <p><b>Email:  </b><?php echo $Alluser['email'] ?></p>
                    </div>
                  </div>
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <button class="btn btn-warning float-right" data-toggle="modal" data-target="#modal-danger" type="button">Modification du profil</button>
            <br>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- modal de modification des informations d'un proprietaire -->
        
        <div class="modal fade modal-info" id="modal-danger">
        <br>
                <div class="modal-dialog modal-info">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Modification du profil</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form action="<?php echo site_url(array('Proprietaire','EnregmodifProprietaire')) ?>" method="post" enctype="multipart/form-data">
                      <div class="modal-body">
                        <label for="nom">Nom</label>
                        <input class="form-control" type="text" value="<?php echo $Alluser['nom'] ?>" name="nom" required="">
                        <br>
                        <label for="nom">Prenom</label>
                        <input class="form-control" type="text" value="<?php echo $Alluser['prenom'] ?>" name="prenom" required="">
                        <br>
                        <label for="profil">Photo de profil</label>
                        <input type="hidden"  name="profil_pass" value="<?php echo $Alluser['profil'] ?>" >
                        <input type="file" placeholder="Entrez une image de profil" name="profil" class="form-control" id="profil" accept="image/*" onchange="loadFile(event)">
                        <p class="text-center"><img id="img"  src="<?php echo img_url('user_profil/'.$Alluser['profil']) ?>" style="width: 200px; height: 150px;"></p>
                        
                        <label for="numero_cni"> Numero_cni</label>
                        <input type="text" class="form-control" value="<?php echo $Alluser['numero_cni'] ?>" id="numero_cni" name="numero_cni" required="">
                        <br>
                        <label for="telephone"> Telephone</label>
                        <input type="tel" class="form-control" value="<?php echo $Alluser['telephone'] ?>" id="telephone" name="telephone" required="">
                        <br>
                        <label for="email"> Email</label>
                        <input type="email" class="form-control"value="<?php echo $Alluser['email'] ?>" name="email" id="email" required="">
                        
                        <input type="hidden"  name="id"  value="<?php echo $_SESSION['PROPRIETAIRE']['id_users']?>">                       
                      </div>
                      <div class="modal-footer ">
                        <button type="submit" class=" form-control btn btn-warning">Modifier</button>
                      </div>
                    </form>
                  </div>
                </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  </div>

<script type="text/javascript">
  var loadFile = function(event) {
    var profil = document.getElementById('img');
    profil.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
<script>
  var btnPopup3 = document.getElementById('btnPopup3');
  var overlay3 = document.getElementById('overlay3');
  btnPopup3.addEventListener('click',openMoadl);
  function openMoadl() {
  overlay3.style.display='block';
  }
  var btnClose3 = document.getElementById('btnClose3');
  btnClose3.addEventListener('click',closeModal);
  function closeModal() {
  overlay3.style.display='none';
  }
  var btnClose4 = document.getElementById('btnClose4');
  btnClose4.addEventListener('click',closeModal);
  function closeModal() {
  overlay3.style.display='none';
  }
</script>
  

