<style>





  .overlay {
      position: fixed;
      left: 0px;
      top:0px;
      width:100%;
      background-color: rgba(0,0 ,0 , 0.5);
      z-index:1;
      display:none;
  }


  .btnClose {
      float: right;
      font-size:16pt;
      cursor: pointer;
      color: rgb(26, 26, 26);
  }
</style>

<div class="content-wrapper">
  <section class="content">
    <div class="container-fluid">
      <div class="content-header">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0">Tableau de bord</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?php echo site_url(array('Proprietaire','index')) ?>">Tableau de bord</a></li>
                <li class="breadcrumb-item "><a href="<?php echo site_url(array('Proprietaire','Touslesemployers')) ?>">Tous les employers</a></li>
                <li class="breadcrumb-item active">Profil</li>
              </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div>
      <div class="row">   
          <div class="col-sm-4  col-md-8"></div>
          <div class="col-sm-8 col-sx-12 col-md-4">
             <!-- <a href="<?php echo site_url(array('Proprietaire','Addproprietairebis')) ?>"><button class="btn btn-warning"><span class="fas fa-plus"></span> Ajouter</button></a>  -->
         
              <form action="<?php echo site_url(array('Proprietaire','ModifprofilEmployer')) ?>" method="POST" style="display:inline-block;">
                <input type="hidden" name="id" value="<?php echo $EmployerPerso['id'] ?>">
                <input type="hidden" name="id_users" value="<?php echo $EmployerPerso['id_users'] ?>">
                <input type="hidden" name="id_entreprise" value="<?php echo $Allentreprise['id'] ?>">
                <button class="btn btn-warning" type="submit">Modifier</button>
               
              </form>
                <button class="btn btn-warning "  id="btnPopup1" <?php if($EmployerPerso['statut']==2){ echo "disabled=''"; }else{  } ?>>Bloquer</button>
                <button class="btn btn-warning" id="btnPopup" <?php if($EmployerPerso['statut']==1){ echo "disabled=''"; }else{  } ?>>Restaurer</button> 
              
                <!--  Pop up pour bloquer un proprietaire -->
                <div id="overlay1" class="overlay content-wrapper" >
                  <div class="content-fluid" style="width:100% !important;">
                    <div id="popup" class="row " style="margin-top:300px; width:100% !important;">
                    
                      <div class="col-4"></div>
                      <div class="col-4" style="background-color:white; border-radius:10px;">
                        <h2>
                          <span id="btnClose1" class="btnClose ">&times;</span>
                        </h2>
                        <p style="text-align:center; font-size:20px; ">Voulez-vous véritablement bloquer cet utilisateur ?</p> 
                        <center style="margin-bottom:10px;">
                          <form action="<?php echo site_url(array('Proprietaire','Bloqueremployer')) ?>" method="POST" style="display:inline-block;">
                            <input type="hidden" name="cible" value="<?php echo $EmployerPerso['id'] ?>">
                            <input type="hidden"  name="statut"  value="2">
                            <Button class="btn btn-warning mr-1 " style="color:black !important;"  type="submit">Oui</Button>
                          </form>

                          <Button class="btn btn-warning float-center" id="btnClose2"><?php  echo "<a style='color:black !important;' href=\"javascript:history.go(-1)\">Non</a>" ?></Button>
                        </center>
              
                        
                      </div>
                      <div class="col-4"></div>
                    </div>
                  </div>
                  
                </div>

                <!--  Pop up pour restaurer un proprietaire -->

                <div id="overlay" class="overlay content-wrapper" >
                  <div class="content-fluid" style="width:100% !important;">
                    <div id="popup" class="row " style="margin-top:300px; width:100% !important;">
                    
                      <div class="col-4"></div>
                      <div class="col-4" style="background-color:white; border-radius:10px;">
                        <h2>
                          <span id="btnClose" class="btnClose">&times;</span>
                        </h2>
                        <p style="text-align:center; font-size:20px; ">Voulez-vous véritablement Restaurer cet utilisateur ?</p> 
                        <center style="margin-bottom:10px;">
                          <form action="<?php echo site_url(array('Proprietaire','Restaureremployer')) ?>" method="POST" style="display:inline-block;">
                            <input type="hidden" name="cible" value="<?php echo $EmployerPerso['id'] ?>">
                            <input type="hidden"  name="statut" value="1">
                            <Button class="btn btn-warning mr-1  " style='color:black !important;' type="submit">Oui</Button>
                          </form>
                          
                          <Button class="btn btn-warning float-center" id="btnClose"><?php  echo "<a style='color:black !important;' href=\"javascript:history.go(-1)\">Non</a>" ?></Button>
                        </center>
              
                        
                      </div>
                      <div class="col-4"></div>
                    </div>
                  </div>
                  
                </div>
          </div>
      </div>
      <br>
      <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-warning card-outline">
              <div class="card-body box-profile">
              <div class="text-center">
                  <img style="border-radius:50%; height:78px; width:78px;"
                  src="<?php echo img_url('user_profil/'.$Employer['profil']) ?>"
                  alt="User profile picture">
              </div>
              <h3 class="profile-username text-center"><?php echo $Employer['nom'].' '.$Employer['prenom'] ?></h3>
              <p class="text-muted text-center"><?php echo $Departement['nom'] ?></p>
                <ul class="list-group list-group-unbordered ">
                  <li class="list-group-item">
                    <b>Status</b><a class="float-right"><?php echo $Employer['regime_matrimo'] ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Responsabilite: </b><a class="float-right"><?php echo $Responsabilite['nom'] ?></a>
                  </li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-8">
            <div class="card">
              <div class="card-body">
                <div class="tab-content">
                  <div class="row">
                    <div class="col-6">
                      <br>
                      <p><b> Nom:  </b>  <?php echo $Employer['nom']?></p><br>
                      <p><b> Date de naissance:  </b>  <?php echo $Employer['date_naissance']?></p><br>
                      <p><b> Numero de CNI:  </b>  <?php echo $Employer['numero_cni']?></p><br>
                      <p><b>Telephone:  </b><?php echo $Employer['telephone'] ?></p>
                    </div>
                    <div class="col-6">
                      <br>
                      <p><b> Prenom:  </b>  <?php echo $Employer['prenom']?></p><br>
                      <p><b> Nationalité:  </b>  <?php echo $Employer['nationalite']?></p><br>
                      <p><b> Sexe:  </b>  <?php echo $Employer['sexe']?></p><br>
                      <p><b>Email:  </b><?php echo $Employer['email'] ?></p>
                    </div>
                  </div>
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  

</div>

 <!--  javascript pop up pour bloquer un proprietaire -->
<script>
  var btnPopup1 = document.getElementById('btnPopup1');
  var overlay1 = document.getElementById('overlay1');
  btnPopup1.addEventListener('click',openMoadl);
  function openMoadl() {
  overlay1.style.display='block';
}
var btnClose1 = document.getElementById('btnClose1');
btnClose1.addEventListener('click',closeModal);
function closeModal() {
overlay1.style.display='none';
}
</script>

 <!--  javascript pop up pour Restaurer un proprietaire -->

<script>
  var btnPopup = document.getElementById('btnPopup');
  var overlay = document.getElementById('overlay');
  btnPopup.addEventListener('click',openMoadl);
  function openMoadl() {
  overlay.style.display='block';
}
var btnClose = document.getElementById('btnClose');
btnClose.addEventListener('click',closeModal);
function closeModal() {
overlay.style.display='none';
}
var btnClose2 = document.getElementById('btnClose2');
btnClose2.addEventListener('click',closeModal);
function closeModal() {
overlay.style.display='none';
}
</script>

<!-- retoruner a la page précédente -->

<script>
function rtn() {
   window.history.back();
}
</script>



  
    
    
 