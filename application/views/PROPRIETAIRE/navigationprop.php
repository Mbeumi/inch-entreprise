<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-warning " style="background-color: black  ;">
  <!-- Brand Logo --> 
  <a href="<?php echo site_url(array('Proprietaire', 'index')) ?>" class="brand-link ml-3">
    <img src="<?php echo img_url('user_profil/'.$Allentreprise['profil']) ?>" style="border-radius:50%; height:35px; width: 35px;" style="opacity: .8">
    <span class="brand-text font-weight-bold"><?php echo $Allentreprise['nom'] ?></span>
  </a>
  <br>

  <!-- Sidebar -->
  <div class="sidebar">
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item ">
          <a href="<?php echo site_url(array('Proprietaire','index')) ?>" class="nav-link <?php if($active==1){ echo "active";} ?>">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Tableau de bord
              <!-- <i class="right fas fa-angle-left"></i> -->
            </p>
          </a>
        </li>
        <li class="nav-item <?php if($active==2 OR $active==3 OR $active==4 OR $active==5 OR $active==6 OR $active==7 OR $active==10){ echo "menu-open";} ?>">
          <a href="#" class="nav-link <?php if($active==2 OR $active==3 OR $active==4 OR $active==5 OR $active==6 OR $active==7 OR $active==10){ echo "active";} ?>">
            <i class="nav-icon fas fa-users"></i>
            <p class="font-weight-bold text-white">
              Gestion des R.H
              <i class="fas fa-angle-left right"></i>
              <span class="badge badge-info right"></span>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item <?php if($active==2 OR $active==3 OR $active==10){ echo "menu-open";} ?>">
              <a href="#" class="nav-link <?php if($active==2 OR $active==3 OR $active==10){ echo "active";} ?> ">
                <i class="nav-icon fas fa-industry"></i>
                <p>
                  Mon entreprise
                  <i class="fas fa-angle-left right"></i>
                  <span class="badge badge-info right"></span>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo site_url(array('Proprietaire', 'profilentreprises')) ?>" class="nav-link <?php if($active==2){ echo "active";} ?>">
                    <i class="fas fa-eye nav-icon"></i>
                    <p class="font-italic">Details</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo site_url(array('Proprietaire', 'Modifprofilentreprise')) ?>" class="nav-link <?php if($active==3){ echo "active";} ?>">
                    <i class="fas fa-edit nav-icon"></i>
                    <p class="font-italic">Modification</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo site_url(array('Proprietaire', 'profilproprietaire')) ?>" class="nav-link <?php if($active==10){ echo "active";} ?>">
                    <i class="fas fa-user nav-icon"></i>
                    <p class="font-italic">Mon Profil</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item <?php if($active==4 OR $active==5){ echo "menu-open";} ?>">
              <a href="#" class="nav-link <?php if($active==4 OR $active==5){ echo "active";} ?>">
                <i class="nav-icon fas fa-users"></i>
                <p>
                  Employers
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo site_url(array('Proprietaire', 'AddEmployer')) ?>" class="nav-link <?php if($active==4){ echo "active";} ?>">
                    <i class="fas fa-plus nav-icon"></i>
                    <p class="font-italic">Ajouter</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo site_url(array('Proprietaire', 'Touslesemployers')) ?>" class="nav-link <?php if($active==5){ echo "active";} ?>">
                    <i class="fas fa-user nav-icon"></i>
                    <p class="font-italic">Les employers</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-book"></i>
                <p>
                  Externes
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo site_url(array('Proprietaire', 'Addemployerexterne')) ?>" class="nav-link">
                    <i class="fas fa-users nav-icon"></i>
                    <p class="font-italic">Employers</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo site_url(array('Proprietaire', 'Addemployerexterne')) ?>" class="nav-link">
                    <i class="fas fa-industry nav-icon"></i>
                    <p class="font-italic">Entreprises</p>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link  ">
            <i class="nav-icon fas fa-building"></i>
            <p class="font-weight-bold text-white">
              Gestion patrimoine
              <i class="fas fa-angle-left right"></i>
              <span class="badge badge-info right"></span>
            </p>
          </a>
          <ul class="nav nav-treeview">
            
          </ul>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link  ">
            <i class="nav-icon fas fa-coins"></i>
            <p class="font-weight-bold text-white">
              Gestion finances
              <i class="fas fa-angle-left right"></i>
              <span class="badge badge-info right"></span>
            </p>
          </a>
          <ul class="nav nav-treeview">
            
          </ul>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link  ">
            <i class="nav-icon fas fa-wallet"></i>
            <p class="font-weight-bold text-white">
              Gestion projets
              <i class="fas fa-angle-left right"></i>
              <span class="badge badge-info right"></span>
            </p>
          </a>
          <ul class="nav nav-treeview">
            
          </ul>
        </li>
        <li class="nav-item <?php if($active==8 OR $active==9){ echo "menu-open";} ?>">
              <a href="#" class="nav-link <?php if($active==8 OR $active==9){ echo "active";} ?>">
                <i class="fas fa-cog nav-icon"></i>
                <p class="font-weight-bold text-white">
                  Parametres
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a  href="<?php echo site_url(array('Proprietaire', 'listedesdepartements')) ?>" class="nav-link <?php if($active==8){ echo "active";} ?>">
                      <i class="fas fa-list nav-icon"></i>
                      <p class="font-italic">Departements</p>
                    </a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo site_url(array('Proprietaire', 'listedesresponsabilites')) ?>" class="nav-link <?php if($active==9){ echo "active";} ?>">
                    <i class="fas fa-list nav-icon"></i>
                    <p class="font-italic">Responsabilites</p>
                  </a>
                </li>
              </ul>
            </li>

      </ul>
    </nav>
  </div>
  <!-- /.sidebar -->
</aside>
