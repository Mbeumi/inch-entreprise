<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" >
  <form action="<?php echo site_url(array('Proprietaire','EnregEmployers')) ?>" method="post" enctype="multipart/form-data">
    <div class="container-fluid">  
      <div class="content-header">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a class="text-warning" href="<?php echo site_url(array('Proprietaire','index')) ?>">Tableau de bord</a></li>
              <li class="breadcrumb-item active">Ajouter un employer </li>
            </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
      </div>
      <div class="card ">
        <div class="card-body">
          <div class="row">
            <div class="col-12">   
              <?php if (isset($_SESSION['message_save'])){ ?>
                <span class="fas fa-check-circle" style="color:green; padding-left:20px;">
                  <?php  echo $_SESSION['message_save'];} ?>
                </span>
              <?php  if (isset($_SESSION['message_error'])){ ?>
                <span style="color:red; padding-left:20px;" class="fas fa-exclamation-triangle">
                  <?php  echo $_SESSION['message_error']; } ?>
                </span>
            </div>
          </div> 
        </div>
      </div>
      <div class="card card-warning">
        <div class="card-header">
          <h3 class="" style="text-align:center !important; font-weight:bold; color:#263238;"> Formulaire d'ajout d'un employer </h3>
        </div>
        <div class="card-body">

          <div class="row">
            <div class="col-5">
              <label for="nom"> Nom</label>
              <input type="text" class="form-control" placeholder="Entrer votre nom" name="nom" id="nom" required="">
            </div>
            <div class="col-5">
              <label for="profil">Photo de profil</label>
              <input type="file" required="" placeholder="Entrez une image de profil" name="profil" class="form-control" id="profil" accept="image/*" onchange="loadFile(event)">
            </div>
            <div class="col-2">
              <img id="img"  style="width: 150px; height: 150px;">
            </div>
          </div>
          <div class="row">
            <div class="col-3">
              <label for="prenom"> Prenom</label>
              <input type="text" class="form-control" placeholder="Entrer votre prenom" name="prenom" id="prenom" required="">
            </div>
            <div class="col-3">
              <label for="date_naissance"> Date de naissance</label>
              <input type="date" class="form-control" placeholder="Date de naissance" id="date_naissance" name="date_naissance" required="">
            </div>
            <div class="col-3">
              <div class="form-group">
                <label for="sexe"> sexe</label>
                <select class="form-control select2" style="width: 100%;" required="" name="sexe">
                    <option disabled="" selected="selected">choix obligatoire</option>
                    <option>Homme</option>
                    <option>Femme</option>
                </select>
              </div>
            </div>
            <div class="col-3">
              <label for="Age"> Age</label>
              <input type="number" class="form-control" placeholder="Exemple: 18 ans" id="age" name="age" required=""> 
            </div>
          </div>
                <div class="row">
                  <div class="col-3">
                    <div class="form-group">
                      <label for="nationalite"> Nationalité</label>
                      <select class="form-control select2" style="width: 100%;" required="" name="nationalite">
                        <option selected="selected">choix obligatoire</option>
                        <option>Camerounaise</option>
                        <option>Congolaise</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="numero_cni"> Numero_cni</label>
                      <input type="text" class="form-control" placeholder="Entrer votre numero de cni" id="numero_cni" name="numero_cni" required="">
                    </div>
                     
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="telephone"> Telephone</label>
                      <input type="tel" class="form-control" placeholder="Entrer numero de telephone" id="telephone" name="telephone" required="">
                    </div>
                  </div>
                  <div class="col-3">
                    <label for="etat_civil"> Etat civil</label>
                    <select id="etat_civil" class="form-control select2" style="width: 100%;" required="" name="regime_matrimo">
                        <option selected="selected">choix obligatoire</option>
                        <option>Celibataire</option>
                        <option>Marié</option>
                        <option>Divorcé</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-3">
                    <div class="form-group">
                      <label for="etat_physique"> Nombre d'enfant </label>
                      <select id="etat_physique" class="form-control select2" style="width: 100%;" required="" name="nbre_enfant">
                        <option selected="selected">choix obligatoire</option>
                        <option>Aucun</option>
                        <?php for($i=1; $i<11; $i++ ){ ?>
                        <option><?php echo $i; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <label for="physique"> Etat physique</label>
                    <select class="form-control select2" style="width: 100%;" required="" name="etat_sante">
                      <option disabled="" selected="selected">choix obligatoire</option>
                      <option>Apte</option>
                      <option>Handicap</option>
                </select>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="etat_physique"> Departement </label>
                      <select id="etat_physique" placeholder="Choix obligatoire" class="form-control select2" style="width: 100%;" required="" name="id_departement">
                        <?php  for ($i = 0; $i < $Alldepartement['total']; $i++) {  ?>
                          <option value="<?php echo $departement[$i]['id'] ?>"><?php echo $departement[$i]['nom'] ?></option>
                        <?php  } ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="form-group">
                      <label for="profession"> Responsabilité</label>
                      <select id="etat_physique" placeholder="Choix obligatoire" class="form-control select2" style="width: 100%;" required="" name="id_responsabilite">
                        <option  disabled="" selected="selected">choix obligatoire</option>
                        <?php  for ($i = 0; $i < $Allresponsabilite['total']; $i++) {  ?>

                          <option value="<?php echo $responsabilite[$i]['id'] ?>"><?php echo $responsabilite[$i]['nom'] ?></option>
                        <?php  } ?>
                      </select>
                    </div>
                  </div>  
          </div>
        </div>
        </div>
        <div class="card card-warning ">
                <div class="card-header">
                  <h3 class="card-title font-weight-bold" style="font-size:20px;">Informations de connexion</h3>
                </div>
                <div class="card-body">
                  <div class="row">
                  <div class="col-4">
                    <label for="identifiant"> Identifiant</label>
                    <input type="text" class="form-control" placeholder="Entrer un identifiant" name="identifiant" id="identifiant" required="">
                  </div>
                  <div class="col-4">
                    <label for="email"> Email</label>
                    <input type="email" class="form-control" placeholder="Entrer votre email" name="email" id="email" required="">
                  </div>
                  <div class="col-4">
                    <label for="password"> Password</label>
                    <input type="password" class="form-control" placeholder="Entrer un mot de pass" name="password" id="password" required="">
                    <input type="hidden"  name="statut" value="1">
                    <input type="hidden"  name="id_entreprise" value="<?php echo $Allentreprise['id']?>">
                  </div>
                </div>
              </div>
        </div>

              <br>
              <div class="card ">
                <div class="card-body">

                  <div class="row">
                    <div class="col-4">
                      
                    </div>
                    <div class="col-4">
                      <input type="submit" class="form-control btn btn-warning" style="text-align:center !important; font-weight:bold; color:#263238;">
                    </div>
                    <div class="col-4">
                     
                    </div>
                  </div>
                
                     
                  
                </div>
              </div>
              <!-- /.card-body -->
        </div>
      

  </form>
</div>

  <script type="text/javascript">
    var loadFile = function(event) {
    var profil = document.getElementById('img');
    profil.src = URL.createObjectURL(event.target.files[0]);
    };
  </script>