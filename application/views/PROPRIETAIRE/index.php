<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Gestion P.M.E | D.Z</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <?php echo admin_plugins_css('fontawesome-free/css/all'); ?>
  <!-- IonIcons -->
  <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- Theme style -->
  <!-- <link rel="stylesheet" href="dist/css/adminlte.min.css"> -->
  <?php echo admin_dist_css('css/adminlte'); ?>
  <?php echo admin_dist_css('css/adminlte.min'); ?>
  <?php echo css("jquery.dataTables.min"); ?>
  <!-- DataTables -->
  <?php echo admin_plugins_css('datatables-bs4/css/dataTables.bootstrap4.min'); ?>
  <?php echo admin_plugins_css('datatables-responsive/css/responsive.bootstrap4.min'); ?>
  <?php echo admin_plugins_css('datatables-buttons/css/buttons.bootstrap4.min'); ?>
  <?php echo admin_plugins_css('fontawesome-free/css/all.min'); ?>
  <?php echo admin_plugins_css('sweetalert2-theme-bootstrap-4/bootstrap-4.min'); ?>
  <?php echo admin_plugins_css('toastr/toastr.min'); ?>
  <!-- Theme style -->
  <?php echo css("jquery.dataTables");  ?>
  <?php echo css("jquery.dataTables.min"); ?>
  <?php echo css("dataTables.bootstrap.min"); ?>
  <?php echo css("dataTables.bootstrap"); ?>

  <!-- autocomplete pour les recherches -->
  <link rel="stylesheet" href="./demo/prettify.css">
  <link href='http://fonts.googleapis.com/css?family=Cinzel+Decorative:400,700,900' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800|Roboto+Slab:400,300,700,100' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <?php echo css("jquery.autocomplete"); ?>
  <?php echo css("demo/demo"); ?>
  <?php echo css("demo/autocomplete"); ?>



  <?php echo css("style"); ?>

</head>
<!--
`body` tag options:

  Apply one or more of the following classes to to the body tag
  to get the desired effect

  * sidebar-collapse
  * sidebar-mini
-->

<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="<?php echo base_url() . "assets/admin/"; ?>#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="<?php echo site_url(array('Proprietaire', 'index')) ?>" class="nav-link">Home</a>
        </li>
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->


        <!-- <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="<?php echo base_url() . "assets/admin/"; ?>#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">15 Notifications</span>
            <div class="dropdown-divider"></div>
            <a href="<?php echo base_url() . "assets/admin/"; ?>#" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i> 4 new messages
              <span class="float-right text-muted text-sm">3 mins</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="<?php echo base_url() . "assets/admin/"; ?>#" class="dropdown-item">
              <i class="fas fa-users mr-2"></i> 8 friend requests
              <span class="float-right text-muted text-sm">12 hours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="<?php echo base_url() . "assets/admin/"; ?>#" class="dropdown-item">
              <i class="fas fa-file mr-2"></i> 3 new reports
              <span class="float-right text-muted text-sm">2 days</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="<?php echo base_url() . "assets/admin/"; ?>#" class="dropdown-item dropdown-footer">See All Notifications</a>
          </div>
        </li> -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <img src="<?php echo img_url('user_profil/' . $Alluser['profil']) ?>" style="border-radius:50%; height:20px; width: 20px;" alt="User Image">
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <div class="dropdown-divider"></div>
            <center>
              <img src="<?php echo img_url('user_profil/' . $Alluser['profil']) ?>" class="mt-3 mb-3" style="border-radius:50%; height:70px; width: 70px;" alt="User Image">
            </center>
            <div class="dropdown-divider"></div>
            <center>
               <p class="d-block font-weight-bold center-block" style="color:black">
                <?php echo $Alluser['nom'] ?>
              </p>
              <p class="d-block mb-3" style="color:black">
                <?php echo $Alluser['prenom'] ?>
              </p>
            </center>
            <div class="dropdown-divider"></div>
            
            <center>
              <button class="btn btn-warning justify-content-center" title="Profil"><a href="<?php echo site_url(array('Proprietaire', 'profilproprietaire')) ?>"><span class="fas fa-user-circle" style="font-size:20px; color:white;"></span></a></button>
              <button class="btn btn-warning justify-content-center" title="Deconnexion"><a href="<?php echo site_url(array('Proprietaire', 'deconnexion')) ?>"><span class="fas fa-key" style="font-size:20px; color:white;"></span></a></button>
            </center>
            

          </div>
        </li>
        <li>
          <!-- <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="<?php echo base_url() . "assets/admin/"; ?>#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?php echo base_url() . "assets/admin/"; ?>#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?php echo base_url() . "assets/admin/"; ?>#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?php echo base_url() . "assets/admin/"; ?>#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div> -->
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="<?php echo base_url() . "assets/admin/"; ?>#" role="button">
            <i class="fas fa-th-large"></i>
          </a>
        </li> -->
      </ul>
    </nav>
    <!-- /.navbar -->