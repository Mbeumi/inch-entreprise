<div class="content-wrapper">
    <div class="content">
		<div class="content-header">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!-- <h1 class="m-0">Tableau de bord</h1> -->
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a class="text-warning" href="<?php echo site_url(array('Proprietaire','index')) ?>">Tableau de bord</a></li>
                        <li class="breadcrumb-item active">Tous les employers</li>
                    </ol>
                    </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
        <div class="card ">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-11 col-sm-11 col-xs-11">
                        <?php   if (isset($_SESSION['message_save'])){ ?>
                            <span class="fas fa-check-circle" style="color:green; padding-left:20px;">
                                <?php    echo $_SESSION['message_save']; } ?>
                            </span>
                        <?php if (isset($_SESSION['message_error'])){?>
                            <span style="color:red;" class="fas fa-exclamation-triangle">
                                <?php  echo $_SESSION['message_error']; } ?>
                            </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6">
                <div class="form-group">
                    <div class="input-group input-group-lg">
                        <input type="text" class="form-control form-control-lg" placeholder="Recherche" id="auto1">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-lg btn-default">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-3"></div>
        </div>
		<br>
        <div class="row">
            <?php 
                for ($i=0; $i <$Allemployers['total']; $i++) { ?>
                    <div class="col-md-3 ">
                        <div class="card card-warning card-outline">
                            <div class="card-body box-profile ">
                                <div class="text-center">
                                    <img style="border-radius:50%; height:60px; width:60px;"
                                    src="<?php echo img_url('user_profil/'.$Employer[$i]['profil']) ?>"
                                    alt="User profile picture">
                                </div>

                                <h3 class="profile-username text-center"><?php echo $Employer[$i]['nom'].' '.$Employer[$i]['prenom'] ?></h3>

                                <p class="text-muted text-center"><?php echo $Departement[$i]['nom'] ?><br>
                                <?php if ($Allemployers[$i]['statut'] == 1){ 
							          	      	echo " <span style='color:green;'>Actif</span>" ;
							          	      } else {
							          	      		echo " <span style='color:red;'>Bloqué</span>";
							          	      } ?> </p>
                                <form action="<?php echo site_url(array('Proprietaire','profil_employer')) ?>" method="post">
                                    <input type="hidden" value="<?php echo $Allemployers[$i]['id_users']?>" name="id_users"/>
                                    <input type="hidden" value="<?php echo $Allentreprise['id']?>" name="id_entreprise"/>
                                    <button type="submit" class="btn btn-warning btn-block" style="font-weight:bold">Profil</button>
                                </form> 
                            </div>
                        </div>
                    </div>   
            <?php  } ?>
            
        </div>
    </div> 
</div>