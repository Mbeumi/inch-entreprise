<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Gestion P.M.E | D.Z</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <?php echo admin_plugins_css('fontawesome-free/css/all'); ?>
  <!-- IonIcons -->
  <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- Theme style -->
  <!-- <link rel="stylesheet" href="dist/css/adminlte.min.css"> -->
  <?php echo admin_dist_css('css/adminlte'); ?>
  <?php echo admin_dist_css('css/adminlte.min'); ?>
  <?php echo css("jquery.dataTables.min"); ?>
  <!-- DataTables -->
  <?php echo admin_plugins_css('datatables-bs4/css/dataTables.bootstrap4.min'); ?>
  <?php echo admin_plugins_css('datatables-responsive/css/responsive.bootstrap4.min'); ?>
  <?php echo admin_plugins_css('datatables-buttons/css/buttons.bootstrap4.min'); ?>
  <?php echo admin_plugins_css('fontawesome-free/css/all.min'); ?>
  <?php echo admin_plugins_css('sweetalert2-theme-bootstrap-4/bootstrap-4.min'); ?>
  <?php echo admin_plugins_css('toastr/toastr.min'); ?>
  <!-- Theme style -->
  <?php echo css("jquery.dataTables");  ?>
  <?php echo css("jquery.dataTables.min"); ?>
  <?php echo css("dataTables.bootstrap.min"); ?>
  <?php echo css("dataTables.bootstrap"); ?>

  <!-- autocomplete pour les recherches -->
  <link rel="stylesheet" href="./demo/prettify.css">
	<link href='http://fonts.googleapis.com/css?family=Cinzel+Decorative:400,700,900' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800|Roboto+Slab:400,300,700,100' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<?php echo css("jquery.autocomplete"); ?>
  <?php echo css("demo/demo"); ?>
  <?php echo css("demo/autocomplete"); ?>




  <?php echo css("style"); ?>

</head>
<!--
`body` tag options:

  Apply one or more of the following classes to to the body tag
  to get the desired effect

  * sidebar-collapse
  * sidebar-mini
-->
<body>
  <div class="row">
    <div class="col-md-3 col-sm-3 col-sx-3"></div>
    <div class="col-md-6 col-sm-6 col-sx-6">
      <br><br><br><br><br><br><br>
      <div class="shadow p-4 mb-4 bg-white rounded-lg border border-warning border-top-0 border-bottom-0">
          <div class="">
            <p class="text2">Modification du mot de passe!</p>
          </div>
          <!-- /.login-logo -->
          <div class="">
            <?php if (isset($_SESSION['message_error'])) { ?>
              <p class="login-box-msg " style="color:red; text-align:center !important;"><span class="fas fa-exclamation-triangle"></span>
              <?php echo $_SESSION['message_error'];
            } ?>
              </p>
              <p class="text1">Vous avez reçu un email veillez entrer le code secret</p>
              <p>         
                <?php if (isset($_SESSION['message_error'])){ ?>
                 <span style="color:red; padding-left:20px;" class="fas fa-exclamation-triangle">
                <?php   echo $_SESSION['message_error'];
                }?>
               </span>
              </p>
              <form action="<?php echo site_url(array('Proprietaire', 'confirmcodemail')) ?>" class="needs-validated" novalidate method="post">
                <div class="container">

                  <div class="form-group ">

                    <label for="email">Entrer le code secret</label>
                    <input type="text" class="form-control" placeholder="Entrer votre email" name="code" required>
                    <input type="hidden" name="id" value="<?php echo $proprietaire['id']?>">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    <div class="valid-feedback">Bien.</div>
                  
                  </div>

                  <div class="row">
                    <div class="col-3">
                    </div>
                    <div class="col-6 form-group justify-content-center">

                      <div class="">
                        <button type="submit" class=" from-control btn btn-warning btn-block btn-flat rounded font-weight-bold">Envoyer</button>
                      </div>
                      <div class="col-3"></div>
                      <!-- /.col -->
                    </div>
                  </div>

                </div>
              </form>


          </div>
          <!-- /.login-box-body -->

        </div>
     </div>
    <div class="col-md-3 col-sm-3 col-sx-3"></div>
  </div>


