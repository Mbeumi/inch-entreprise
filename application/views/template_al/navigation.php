<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-warning ">
    <!-- Brand Logo -->
    <a href="<?php echo site_url(array('Administration','index')) ?>" class="brand-link">
    <?php echo img('logo.png', 'logo', 'imglogo') ?>
      <span class="brand-text font-weight-bold">Inch Entreprise</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3  mb-3 d-flex">
        <?php if (isset($_SESSION['ADMIN'])) {?> 
          <div class="image">
            <img src="<?php echo img_url('user_profil/'.$Alladmin[0]['profil']) ?>" style="border-radius:50%; height:35px; width: 35px;" alt="User Image">
            </div>
            <div class="info">                 
                      <p class="d-block font-weight-bold" style="color:white">
                        <?php echo $Alladmin[0]['identifiant']?>
                      </p>
              
            </div>
        <?php } ?>
          </div>

      <!-- SidebarSearch Form -->
      <!-- <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo site_url(array('Administration','index')) ?>" class="nav-link <?php if($active==1){ echo "active";} ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Tableau de bord
                
              </p>
            </a>
          </li>
          <li class="nav-item <?php if($active==2 OR $active==3 OR $active==4 OR $active==5){ echo "menu-open";} ?>">
            <a href="#" class="nav-link <?php if($active==2 OR $active==3 OR $active==4 OR $active==5){ echo "active";} ?> ">
              <i class="nav-icon fas fa-users "></i>
              <p class="font-weight-bold text-white">
                Gestion des R.H
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right"></span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item <?php if($active==2 OR $active==3){ echo "menu-open";} ?>">
                <a href="#" class="nav-link <?php if($active==2 OR $active==3){ echo "active";} ?>">
                  <i class="nav-icon fas fa-users"></i>
                  <p>
                    Proprietaires
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?php echo site_url(array('Administration','Addproprietaire')) ?>" class="nav-link <?php if($active==2){ echo "active";} ?>">
                      <i class="fas fa-plus nav-icon"></i>
                      <p class="font-italic">Ajout proprietaire</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url(array('Administration','Allproprietaire')) ?>" class="nav-link <?php if($active==3){ echo "active";} ?>">
                      <i class="fas fa-user nav-icon"></i>
                      <p class="font-italic">Tous les proprietaires</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="nav-item <?php if($active==4 OR $active==5){ echo "menu-open";} ?>">
                <a href="#" class="nav-link <?php if($active==4 OR $active==5){ echo "active";} ?>">
                  <i class="nav-icon fas fa-industry"></i>
                  <p>
                    Entreprises
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?php echo site_url(array('Administration','AddEntreprise')) ?>" class="nav-link <?php if($active==4){ echo "active";} ?>">
                      <i class="fas fa-plus nav-icon"></i>
                      <p class="font-italic">Ajout entreprise</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url(array('Administration','touteslesentreprises')) ?>" class="nav-link <?php if($active==5){ echo "active";} ?>">
                      <i class="fas fa-industry nav-icon"></i>
                      <p class="font-italic">Toutes les entreprises</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link  ">
              <i class="nav-icon fas fa-building"></i>
              <p class="font-weight-bold text-white">
                Gestion patrimoine
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right"></span>
              </p>
            </a>
            <ul class="nav nav-treeview">

            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link  ">
              <i class="nav-icon fas fa-coins"></i>
              <p class="font-weight-bold text-white">
                Gestion finances
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right"></span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link  ">
              <i class="nav-icon fas fa-wallet"></i>
              <p class="font-weight-bold text-white">
                Gestion projets
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right"></span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
            </ul>
          </li>
          <li class="nav-item <?php if($active==6 OR $active==7){ echo "menu-open";} ?>">
                <a href="#" class="nav-link <?php if($active==6 OR $active==7){ echo "active";} ?>">
                  <i class="nav-icon fas fa-cog"></i>
                  <p class="font-weight-bold text-white">
                    Parametres
                    <i class="fas fa-angle-left right"></i>
                    <span class="badge badge-info right"></span>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <!-- <li class="nav-item">
                    <a href="<?php echo site_url(array('Administration','AddCategorie')) ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Ajout categorie/secteur</p>
                    </a>
                  </li> -->
                  <li class="nav-item">
                    <a href="<?php echo site_url(array('Administration','listedescategories')) ?>" class="nav-link <?php if($active==6){ echo "active";} ?>">
                      <i class="fas fa-list nav-icon"></i>
                      <p class="font-italic"> Categories</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url(array('Administration','listedessecteurs')) ?>" class="nav-link <?php if($active==7){ echo "active";} ?>">
                      <i class="fas fa-list nav-icon"></i>
                      <p class="font-italic">Secteurs</p>
                    </a>
                  </li>
                </ul>
          </li>
          <li class="nav-item">
            <a href="<?php echo site_url(array('Administration','deconnexion')) ?>" class="nav-link">
              <i class="nav-icon fas fa-key"></i>
              <p class="font-weight-bold text-white">
                Deconnexion
              </p>
            </a>
            <!-- <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/forms/general.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>General Elements</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/forms/advanced.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Advanced Elements</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/forms/editors.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Editors</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/forms/validation.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Validation</p>
                </a>
              </li>
            </ul> -->
          </li> 
        </ul>
      </nav>
    </div>
    <!-- /.sidebar -->
  </aside>