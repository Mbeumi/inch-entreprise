<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Administration extends CI_Controller
{




	public function index()
	{


		if (isset($_SESSION['ADMIN'])) {
			$data['Alladmin'] = $this->Admin->findAllAdminBd();
			$data['Allentreprise'] = $this->Entreprise->findAllEntrepriseBd();
			$data['Allproprietaire'] = $this->Proprietaire->findAllproprietaireBd();
			$data['Allcategorie'] = $this->Categorie->findAllCategorieBd();
			$data['Allsecteur'] = $this->Secteur->findAllSecteurBd();
			$data['active'] = 1;
			$this->load->view('ADMIN/index', $data);
			$this->load->view('template_al/navigation', $data);
			$this->load->view('ADMIN/home');
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}



	public function Addproprietaire()
	{

		if (isset($_SESSION['ADMIN'])) {
			$data['Alladmin'] = $this->Admin->findAllAdminBd();
			$data['active'] = 2;
			$this->load->view('ADMIN/index', $data);
			$this->load->view('template_al/navigation', $data);
			$this->load->view('ADMIN/formulaireProprietaire');
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}


	public function Allproprietaire()
	{

		if (isset($_SESSION['ADMIN'])) {
			$data['Alladmin'] = $this->Admin->findAllAdminBd();
			$data['active'] = 3;
			$data1['Allproprietaire'] = $this->Proprietaire->findAllproprietaireBd();
			for ($i = 0; $i < $data1['Allproprietaire']['total']; $i++) {
				$data1['Allproprietaire'][$i]['infos'] = $this->Users->findUsers($data1['Allproprietaire'][$i]['id_users']);
			}
			$this->load->view('ADMIN/index', $data);
			$this->load->view('template_al/navigation', $data);
			$this->load->view('ADMIN/Touslesproprietaires', $data1);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}


	public function Addproprietairebis()
	{

		if (isset($_SESSION['ADMIN'])) {
			$data['Alladmin'] = $this->Admin->findAllAdminBd();
			$this->load->view('ADMIN/index', $data);
			$this->load->view('template_al/navigation', $data);
			$this->load->view('ADMIN/formulaireProprietairebis');
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}



	public function AddEntreprise()
	{

		if (isset($_SESSION['ADMIN'])) {
			$data['Alladmin'] = $this->Admin->findAllAdminBd();
			$data1['Allcategorie'] = $this->Categorie->findAllCategorieBd();
			$data1['Allsecteur'] = $this->Secteur->findAllSecteurBd();
			$data['active'] = 4;
			$data1['Allproprietaire'] = $this->Proprietaire->findAllproprietaireBd();
			for ($i = 0; $i < $data1['Allproprietaire']['total']; $i++) {
				$data1['Allproprietaire'][$i]['infos'] = $this->Users->findUsers($data1['Allproprietaire'][$i]['id_users']);
			}
			$this->load->view('ADMIN/index', $data);
			$this->load->view('template_al/navigation', $data);
			$this->load->view('ADMIN/formulaireEntreprise', $data1);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}



	// fonction pour afficher toutes les entreprises enregistrés

	public function touteslesentreprises()
	{


		if (isset($_SESSION['ADMIN'])) {
			$data['Alladmin'] = $this->Admin->findAllAdminBd();
			$data1['Allentreprise'] = $this->Entreprise->findAllEntrepriseBd();
			$data['active'] = 5;
			$this->load->view('ADMIN/index', $data);
			$this->load->view('template_al/navigation', $data);
			$this->load->view('ADMIN/Touslesentreprises', $data1);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}


	//   fonction pour afficher le profil de chaque entreprise enregistré

	public function profilentreprises()
	{


		if (isset($_SESSION['ADMIN'])) {

			$data['Alladmin'] = $this->Admin->findAllAdminBd();
			$data1['Allentreprise'] = $this->Entreprise->findEntreprise($_POST['cible']);
			$data1['Allproprietaire'] = $this->Proprietaire->findProprietaireInfos($data1['Allentreprise']['id_proprietaire']);
			$data1['Allproprietaire']['infos'] = $this->Users->findUsers($data1['Allproprietaire']['id_users']);
			$data1['Allcategorie'] = $this->Categorie->findCategorie($data1['Allentreprise']['id_categorie']);
			$data1['Allsecteur'] = $this->Secteur->findSecteur($data1['Allentreprise']['id_secteur']);
			$data['active'] = 5;
			$this->load->view('ADMIN/index', $data);
			$this->load->view('template_al/navigation', $data);
			$this->load->view('ADMIN/profil_entreprises', $data1);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}


	public function AddCategorie()
	{

		if (isset($_SESSION['ADMIN'])) {
			$data['Alladmin'] = $this->Admin->findAllAdminBd();
			$this->load->view('ADMIN/index', $data);
			$this->load->view('template_al/navigation', $data);
			$this->load->view('ADMIN/formulaireCategorie');
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}
	public function listedescategories()
	{

		if (isset($_SESSION['ADMIN'])) {
			$data['Alladmin'] = $this->Admin->findAllAdminBd();
			$data1['Allcategorie'] = $this->Categorie->findAllCategorieBd();
			$data['active'] = 6;
			for ($i = 0; $i < $data1['Allcategorie']['total']; $i++) {
				$data1['Allentreprise'][$i] = $this->Entreprise->findEntreprisecategories($data1['Allcategorie'][$i]['id']);
			}
			$this->load->view('ADMIN/index', $data);
			$this->load->view('template_al/navigation', $data);
			$this->load->view('ADMIN/listedescategories', $data1);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}

	public function listedessecteurs()
	{

		if (isset($_SESSION['ADMIN'])) {
			$data['Alladmin'] = $this->Admin->findAllAdminBd();
			$data1['Allsecteur'] = $this->Secteur->findAllSecteurBd();
			$data['active'] = 7;
			for ($i = 0; $i < $data1['Allsecteur']['total']; $i++) {
				$data1['Allentreprise'][$i] = $this->Entreprise->findEntreprisesecteur($data1['Allsecteur'][$i]['id']);
			}
			$this->load->view('ADMIN/index', $data);
			$this->load->view('template_al/navigation', $data);
			$this->load->view('ADMIN/listedessecteurs', $data1);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}







	//  fonction pour afficher le profil d'un proprietaire (toutes ces informations)

	public function profil_proprietaire()
	{


		if (isset($_SESSION['ADMIN'])) {

			$data['Alladmin'] = $this->Admin->findAllAdminBd();
			$data1['Allproprietaire'] = $this->Proprietaire->findProprietaireInfos($_POST['cible']);
			$data['active'] = 3;
			$data1['Allproprietaire']['infos'] = $this->Users->findUsers($data1['Allproprietaire']['id_users']);
			$this->load->view('ADMIN/index', $data);
			$this->load->view('template_al/navigation', $data);
			$this->load->view('ADMIN/profil', $data1);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}


	//  fonction pour afficher le formulaire de modification du profil proprietaire

	public function Modifprofilproprietaire()
	{

		if (isset($_SESSION['ADMIN'])) {
			$data['Alladmin'] = $this->Admin->findAllAdminBd();
			$data1['Allproprietaire'] = $this->Proprietaire->findProprietaireInfos($_POST['cible']);
			$data1['Allproprietaire']['infos'] = $this->Users->findUsers($data1['Allproprietaire']['id_users']);
			$data['active'] = 3;
			$this->load->view('ADMIN/index', $data);
			$this->load->view('template_al/navigation', $data);
			$this->load->view('ADMIN/modif_profil', $data1);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}




	//  fonction pour enregistrer les informations d'un proprietaire!

	public function EnregProprietaire()
	{
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				if (isset($_FILES['profil']) and $_FILES['profil']['error'] == 0) {
					// Testons si le fichier n'est pas trop gros
					if ($_FILES['profil']['size'] <= 100000000) {
						// Testons si l'extension est autorisée
						$infosfichier = pathinfo($_FILES['profil']['name']);
						$extension_upload = $infosfichier['extension'];

						$config = $_FILES['profil']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['id'];
						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable . '.' . $extension_upload;
						move_uploaded_file($_FILES['profil']['tmp_name'], 'assets/images/user_profil/' . $config);
						$data['profil'] = $config;
					} else {
						$_SESSION['message_error'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
						$this->session->mark_as_flash('message_error');
						$data['message'] = 'non';
					}
				} else {
					$_SESSION['message_error'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
					$this->session->mark_as_flash('message_error');
					$data['message'] = 'non';
				}

				$Alluser = $this->Users->findAllUsersBd();
				if ($_POST['email'] == $Alluser['email']) {
					redirect(site_url(array('Administration', 'Addproprietaire')));
					$_SESSION['message_error'] = "Adresse email existant veillez la changer!";
					$this->session->mark_as_flash('message_error');
				}
				$data0['email'] = $this->Users->findUsersemail($_POST['email']);
				if ($data0['email']['data'] == 'ok') {
					$_SESSION['message_error'] = "Cette adresse email existe déje veillez la changer!";
					$this->session->mark_as_flash('message_error');
					redirect(site_url(array('Proprietaire', 'AddEmployer')));
				}

				$data['nom'] = htmlspecialchars($_POST['nom']);
				$data['prenom'] = htmlspecialchars($_POST['prenom']);
				$data['date_naissance'] = htmlspecialchars($_POST['date_naissance']);
				$data['sexe'] = htmlspecialchars($_POST['sexe']);
				$data['telephone'] = htmlspecialchars($_POST['telephone']);
				$data['numero_cni'] = htmlspecialchars($_POST['numero_cni']);
				$data['nationalite'] = htmlspecialchars($_POST['nationalite']);
				$data['age'] = htmlspecialchars($_POST['age']);
				$data['regime_matrimo'] = htmlspecialchars($_POST['regime_matrimo']);
				$data['etat_sante'] = htmlspecialchars($_POST['etat_sante']);
				$data['nbre_enfant'] = htmlspecialchars($_POST['nbre_enfant']);
				$data['email'] = htmlspecialchars($_POST['email']);
				$data1['identifiant'] = htmlspecialchars($_POST['identifiant']);
				$data1['password'] = htmlspecialchars(md5($_POST['password']));
				$data1['statut'] = htmlspecialchars($_POST['statut']);

				$data1['profession'] = htmlspecialchars($_POST['profession']);

				$this->Users->hydrate($data);
				$this->Users->addUsers();
				$data1['id_users'] = $this->Users->findlastUser()['id_users'];
				$this->Proprietaire->hydrate($data1);
				$this->Proprietaire->addproprietaire();
				$to  = $_POST['email'];
				$headers = "From: inchentreprise237@gmail.com";
				$body = "Bonjour Mr/Mme, votre compte a été creé avec success! Vous pouvez desormais vous connecter via l'adresse:  www.inchentreprise.com/Proprietaire/formulaireConnexion .  à partir de votre identifiant:" . $_POST['identifiant'] . ", votre adresse email: " . $_POST['email'] . " et de votre mot de passe:" . $_POST['password'] . "!
					Nous vous remercions pour votre confiance!";
				$tete = "Inch Entreprise: Confirmation d'adresse mail";
				mail($to, $tete, $body, $headers);
				$_SESSION['message_save'] = "Propriétaire  enregistré avec success !!";
				$this->session->mark_as_flash('message_save');

				$_SESSION['success'] = 'ok';
				redirect(site_url(array('Administration', 'Allproprietaire')));
			} else {
				$_SESSION['message_error'] = "Une Erreur est subvenu recommencer !!";
				$this->session->mark_as_flash('message_error');
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}
	//  fonction pour mettre a jour et enregistrer les information d'un proprietaire

	public function EnregmodifProprietaire()
	{
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				if (isset($_FILES['profil']) and !empty($_FILES['profil']['name'])) {
					if (isset($_FILES['profil']) and $_FILES['profil']['error'] == 0) {
						// Testons si le fichier n'est pas trop gros
						if ($_FILES['profil']['size'] <= 100000000) {
							// Testons si l'extension est autorisée
							$infosfichier = pathinfo($_FILES['profil']['name']);
							$extension_upload = $infosfichier['extension'];

							$config = $_FILES['profil']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['id'];
							$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable . '.' . $extension_upload;
							move_uploaded_file($_FILES['profil']['tmp_name'], 'assets/images/user_profil/' . $config);
							$data['profil'] = $config;
						} else {
							$_SESSION['message_error'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
							$data['message'] = 'non';
						}
					} else {
						$_SESSION['message_error'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
						$data['message'] = 'non';
					}
				} else {

					$data['profil'] = htmlspecialchars($_POST['profil_pass']);
				}
				$data['nom'] = htmlspecialchars($_POST['nom']);
				$data['prenom'] = htmlspecialchars($_POST['prenom']);
				$data['date_naissance'] = htmlspecialchars($_POST['date_naissance']);
				$data['sexe'] = htmlspecialchars($_POST['sexe']);
				$data['telephone'] = htmlspecialchars($_POST['telephone']);
				$data['numero_cni'] = htmlspecialchars($_POST['numero_cni']);
				$data['nationalite'] = htmlspecialchars($_POST['nationalite']);
				$data['age'] = htmlspecialchars($_POST['age']);
				$data['regime_matrimo'] = htmlspecialchars($_POST['regime_matrimo']);
				$data['etat_sante'] = htmlspecialchars($_POST['etat_sante']);
				$data['nbre_enfant'] = htmlspecialchars($_POST['nbre_enfant']);
				$data['email'] = htmlspecialchars($_POST['email']);
				$data1['identifiant'] = htmlspecialchars($_POST['identifiant']);
				$data1['profession'] = htmlspecialchars($_POST['profession']);
				// print_r($data);
				// print_r($_POST['id_users']);
				// print_r($_POST['id']);

				// $data['date']=date('Y-m-d H:i:s');
				$this->Users->hydrate($data);
				$this->Users->UpdateUserinfo($_POST['id_users']);
				$this->Proprietaire->hydrate($data1);
				$this->Proprietaire->Updateproprietaire($_POST['id']);
				$_SESSION['message_save'] = "Modification de profil  enregistré avec success !!";

				$this->session->mark_as_flash('message_save');

				redirect(site_url(array('Administration', 'Allproprietaire')));
			} else {
				redirect(site_url(array('Administration', 'Allproprietaire')));
				$_SESSION['message_error'] = "Une Erreur est subvenu recommencer !!";
				$this->session->mark_as_flash('message_error');
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}


	//   fonction pour bloquer un proprietaire

	public function Bloquerproprietaire()
	{

		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST) and !empty($_POST)) {
				$data['statut'] = htmlspecialchars($_POST['statut']);
				$this->Proprietaire->hydrate($data);
				$this->Proprietaire->Updatestatutproprietaire($_POST['cible']);
				$_SESSION['message_save'] = "Vous avez bloquer ce proprietaire avec succes";
				$this->session->mark_as_flash('message_save');
				redirect(site_url(array('Administration', 'Allproprietaire')));
			} else {
				$_SESSION['message_error'] = "Désole votre requete n'a pas été enregistré!";
				$this->session->mark_as_flash('message_error');
				redirect(site_url(array('Administration', 'Allproprietaire')));
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}


	public function Restaurerproprietaire()
	{

		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST) and !empty($_POST)) {
				$data['statut'] = htmlspecialchars($_POST['statut']);
				$this->Proprietaire->hydrate($data);
				$this->Proprietaire->Updatestatutproprietaire($_POST['cible']);
				$_SESSION['message_save'] = "Vous avez restauré ce proprietaire avec succes";
				$this->session->mark_as_flash('message_save');
				redirect(site_url(array('Administration', 'Allproprietaire')));
			} else {

				$_SESSION['message_error'] = "Désole votre requete n'a pas été enregistré!";
				$this->session->mark_as_flash('message_error');
				redirect(site_url(array('Administration', 'Allproprietaire')));
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}




	public function EnregEntreprise()
	{
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				if (isset($_FILES['profil']) and $_FILES['profil']['error'] == 0) {
					// Testons si le fichier n'est pas trop gros
					if ($_FILES['profil']['size'] <= 100000000) {
						// Testons si l'extension est autorisée
						$infosfichier = pathinfo($_FILES['profil']['name']);
						$extension_upload = $infosfichier['extension'];

						$config = $_FILES['profil']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['id'];
						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable . '.' . $extension_upload;
						move_uploaded_file($_FILES['profil']['tmp_name'], 'assets/images/user_profil/' . $config);
						$data['profil'] = $config;
					} else {
						$_SESSION['message_error'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
						$this->session->mark_as_flash('message_error');
						$data['message'] = 'non';
					}
				} else {
					$_SESSION['message_error'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
					$this->session->mark_as_flash('message_error');
					$data['message'] = 'non';
				}
				$data['nom'] = htmlspecialchars($_POST['nom']);
				$data['reference'] = htmlspecialchars($_POST['reference']);
				$data['localisation'] = htmlspecialchars($_POST['localisation']);
				$data['date_creation'] = date('Y-m-d H:i:s');
				$data['id_proprietaire'] = htmlspecialchars($_POST['id_proprietaire']);
				$data['id_categorie'] = htmlspecialchars($_POST['id_categorie']);
				$data['numero_contrib'] = htmlspecialchars($_POST['numero_contrib']);
				$data['statut'] = htmlspecialchars($_POST['statut']);
				$data['id_secteur'] = htmlspecialchars($_POST['id_secteur']);
				$this->Entreprise->hydrate($data);
				$this->Entreprise->addEntreprise();
				$data1['users'] = $this->Proprietaire->findId_users($_POST['id_proprietaire']);
				$data1['proprietaire'] = $this->Proprietaire->findpasswordUsers($_POST['id_proprietaire']);
				$data1['email'] = $this->Users->findUsersId($data1['users']['id_users']);
				$email = $data1['email']['email'];
				$to  = $email;
				$headers = "From: inchentreprise237@gmail.com";
				$body = "Bonjour Mr/Mme, votre espace entreprise a été creé avec success! Vous pouvez vous connecter à present via l'adresse  www.inchentreprise.com/Proprietaire/formulaireConnexion . Connecter vous  via votre acces!       
					Nous vous remercions pour votre confiance!";
				$tete = "Inch Entreprise: Confirmation d'adresse mail";
				mail($to, $tete, $body, $headers);
				$_SESSION['message_save'] = "Votre entreprise a été enregistré avec success !!";
				$this->session->mark_as_flash('message_save');
				$_SESSION['success'] = 'ok';
				redirect(site_url(array('Administration', 'touteslesentreprises')));
			} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}

	public function ActivationEntreprise()
	{

		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST) and !empty($_POST)) {
				$data['statut'] = htmlspecialchars($_POST['statut']);
				$this->Entreprise->hydrate($data);
				$this->Entreprise->Updatestatutentreprise($_POST['cible']);
				$_SESSION['message_save'] = "Activation réalisée avec succes";
				$this->session->mark_as_flash('message_save');
				redirect(site_url(array('Administration', 'touteslesentreprises')));
			} else {
				$_SESSION['message_error'] = "Désole votre requete n'a pas été enregistré!";
				$this->session->mark_as_flash('message_error');
				redirect(site_url(array('Administration', 'touteslesentreprises')));
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}




	public function DesactivationEntreprise()
	{

		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST) and !empty($_POST)) {
				$data['statut'] = htmlspecialchars($_POST['statut']);
				$this->Entreprise->hydrate($data);
				$this->Entreprise->Updatestatutentreprise($_POST['cible']);
				$_SESSION['message_save'] = "Desactivation réalisée  avec succes";
				$this->session->mark_as_flash('message_save');
				redirect(site_url(array('Administration', 'touteslesentreprises')));
			} else {
				$_SESSION['message_error'] = "Désole votre requete n'a pas été enregistré!";
				$this->session->mark_as_flash('message_error');
				redirect(site_url(array('Administration', 'touteslesentreprises')));
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}






	// fonction qui charge le formulaire de connexion pour un administrateur
	public function formulaireConnexion()
	{

		if (isset($_SESSION['ADMIN'])) {
			if (isset($_SESSION['ADMIN'])) {
				redirect(site_url(array('Administration', 'index')));
			} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		} else {
			$this->load->view('gestion_admin/formulaire_connexion');
		}
	}

	public function manageConnexion()
	{

		if (isset($_POST['email']) && isset($_POST['password'])) {

			$admin = $this->Admin->findAllAdminBd();
			for ($i = 0; $i < $admin['total']; $i++) {
				if ($admin[$i]['email'] == $_POST['email'] && $admin[$i]['password'] == htmlspecialchars(md5($_POST['password']))) {
					$_SESSION['ADMIN'] = $admin[$i];
				}
			}

			if (isset($_SESSION['ADMIN'])) {
				redirect(site_url(array('Administration', 'index')));
			} else {
				$_SESSION['message_error'] = "Adresse email ou mot de passe incorrect!";
				$this->session->mark_as_flash('message_error');
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		} else {
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}

	// Gestions des Administrateurs


	public function manageAdmin()
	{

		if (isset($_SESSION['ADMIN'])) {

			$data['AllAdmin'] = $this->Admin->findAllAdminBd();
			$this->load->view('WELCOME/index', $data);
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/home_admin');
			$this->load->view('WELCOME/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}
	public function testExitAdmin($email)
	{
		$etat = 0;
		$data['infoAdmin'] = $this->Admin->findAllAdminBd();
		if ($data['infoAdmin']['total'] <= 0) {
		} else {
			for ($i = 0; $i < $data['infoAdmin']['total']; $i++) {
				if ($data['infoAdmin'][$i]['email'] == $email) {
					$etat = 1;
					break;
				} else {
					$etat = 0;
				}
			}
		}
		return $etat;
	}
	public function EnregCategorie()
	{
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				$data['nom'] = htmlspecialchars($_POST['nom']);
				$data['date_modif'] = htmlspecialchars($_POST['date_modif']);
				$data['date_creation'] = date('Y-m-d H:i:s');
				$this->Categorie->hydrate($data);
				$this->Categorie->addCategorie();
				$_SESSION['message_save'] = "Categorie enregistré avec success !!";
				$this->session->mark_as_flash('message_save');
				$_SESSION['success'] = 'ok';
				redirect(site_url(array('Administration', 'listedescategories')));
			} else {
				redirect(site_url(array('Administration', 'formulaireConnexion')));
				$_SESSION['message_error'] = "Une Erreur est subvenu recommencer !!";
				$this->session->mark_as_flash('message_error');
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}
	public function EnregSecteur()
	{
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				$data['nom'] = htmlspecialchars($_POST['nom']);
				$data['date_modif'] = htmlspecialchars($_POST['date_modif']);
				$data['date_creation'] = date('Y-m-d H:i:s');
				$this->Secteur->hydrate($data);
				$this->Secteur->addSecteur();
				$_SESSION['message_save'] = "Secteur enregistré avec success !!";
				$this->session->mark_as_flash('message_save');
				$_SESSION['success'] = 'ok';
				redirect(site_url(array('Administration', 'listedessecteurs')));
			} else {
				redirect(site_url(array('Administration', 'formulaireConnexion')));
				$_SESSION['message_error'] = "Une Erreur est subvenu recommencer !!";
				$this->session->mark_as_flash('message_error');
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}




	//  fonction pour afficher le formulaire pour modifier les informations d'une entreprise

	public function modifProfilEntreprises()
	{


		if (isset($_SESSION['ADMIN'])) {

			$data['Alladmin'] = $this->Admin->findAllAdminBd();
			$data1['Allentreprise'] = $this->Entreprise->findEntreprise($_POST['cible']);
			$data1['Allproprietaire'] = $this->Proprietaire->findProprietaireInfos($data1['Allentreprise']['id_proprietaire']);
			$data1['Allproprietaire']['infos'] = $this->Users->findUsers($data1['Allproprietaire']['id_users']);
			$data1['Allcategorie'] = $this->Categorie->findCategorie($data1['Allentreprise']['id_categorie']);
			$data1['Allsecteur'] = $this->Secteur->findSecteur($data1['Allentreprise']['id_secteur']);
			$data['active'] = 5;
			$data1['Allproprietaire1'] = $this->Proprietaire->findAllproprietaireBd();
			for ($i = 0; $i < $data1['Allproprietaire1']['total']; $i++) {
				$data1['Allproprietaire1'][$i]['infos'] = $this->Users->findUsers($data1['Allproprietaire1'][$i]['id_users']);
			}
			$data1['Allcategorie1'] = $this->Categorie->findAllCategorieBd();
			$data1['Allsecteur1'] = $this->Secteur->findAllSecteurBd();
			$this->load->view('ADMIN/index', $data);
			$this->load->view('template_al/navigation', $data);
			$this->load->view('ADMIN/modif_entreprise', $data1);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}


	//  fonction pour modifier dans la base de donné les informations d'une entreprise.



	public function EnregmodifEntreprise()
	{
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				if (isset($_FILES['profil']) and !empty($_FILES['profil']['name'])) {
					if (isset($_FILES['profil']) and $_FILES['profil']['error'] == 0) {
						// Testons si le fichier n'est pas trop gros
						if ($_FILES['profil']['size'] <= 100000000) {
							// Testons si l'extension est autorisée
							$infosfichier = pathinfo($_FILES['profil']['name']);
							$extension_upload = $infosfichier['extension'];

							$config = $_FILES['profil']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['id'];
							$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable . '.' . $extension_upload;
							move_uploaded_file($_FILES['profil']['tmp_name'], 'assets/images/user_profil/' . $config);
							$data['profil'] = $config;
						} else {
							$_SESSION['message_error'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
							$data['message'] = 'non';
						}
					} else {
						$_SESSION['message_error'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
						$data['message'] = 'non';
					}
				} else {

					$data['profil'] = htmlspecialchars($_POST['profil_pass']);
				}
				$data['nom'] = htmlspecialchars($_POST['nom']);
				$data['reference'] = htmlspecialchars($_POST['reference']);
				$data['localisation'] = htmlspecialchars($_POST['localisation']);
				$data['id_proprietaire'] = htmlspecialchars($_POST['id_proprietaire']);
				$data['id_categorie'] = htmlspecialchars($_POST['id_categorie']);
				$data['id_secteur'] = htmlspecialchars($_POST['id_secteur']);
				$data['numero_contrib'] = htmlspecialchars($_POST['numero_contrib']);

				$this->Entreprise->hydrate($data);
				$this->Entreprise->UpdateEntreprise($_POST['id']);
				$_SESSION['message_save'] = "Modification des informations d'entreprise enregistré avec success!";

				$this->session->mark_as_flash('message_save');

				redirect(site_url(array('Administration', 'touteslesentreprises')));
			} else {
				redirect(site_url(array('Administration', 'touteslesentreprises')));
				$_SESSION['message_error'] = "Une Erreur est subvenu veillez reprendre le processus!";
				$this->session->mark_as_flash('message_error');
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}

	// public function addAdmin(){
	// 	if (isset($_SESSION['ADMIN'])) {
	// 		if (isset($_POST)) {
	// 			$etat=$this->testExitAdmin($_POST['email']);
	// 			if ($etat==0) {
	// 				if (isset($_FILES['profil']) AND $_FILES['profil']['error'] == 0 ){
	//         		// Testons si le fichier n'est pas trop gros
	//                     if ($_FILES['profil']['size'] <= 100000000){
	//                         // Testons si l'extension est autorisée
	//                         $infosfichier =pathinfo($_FILES['profil']['name']);
	//                         $extension_upload = $infosfichier['extension'];

	//                         $config =$_FILES['profil']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['ADMIN']['id'];
	//  						$ma_variable = str_replace('.', '_', $config);
	// 						$ma_variable = str_replace(' ', '_', $config);
	// 						$config = $ma_variable.'.'.$extension_upload;
	// 						move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
	// 						$data['profil']=$config;

	//                     }else{
	//                         $_SESSION['message_error']="La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
	// 						$this->session->mark_as_flash('message_error'); 
	//                         $data['message']='non';
	//                     }
	//                 }else{
	//                     $_SESSION['message_error']="L'image choisie  est endommagée  veuillez le remplacer svp !!";
	// 					$this->session->mark_as_flash('message_error'); 
	//                     $data['message']='non';
	//                 }

	// 				$data['nom']=htmlspecialchars($_POST['nom']);
	// 				$data['email']=htmlspecialchars($_POST['email']);
	// 				$data['password']=htmlspecialchars($_POST['password']);
	// 				$data['telephone']=htmlspecialchars($_POST['telephone']);

	// 				$data['date']=date('Y-m-d H:i:s');
	// 				$this->Admin->hydrate($data);
	// 				$this->Admin->addAdmin();
	// 				$_SESSION['message_save']="Administrateurs enregistré avec success !!";
	// 				$this->session->mark_as_flash('message_save'); 
	// 		 		$_SESSION['success']='ok';
	// 		 		redirect(site_url(array('Administration','manageAdmin')));

	// 			}else{
	// 				session_destroy();
	// 				redirect(site_url(array('Administration','formulaireConnexion')));
	// 			}
	// 		}else{
	// 			session_destroy();
	// 			redirect(site_url(array('Administration','formulaireConnexion')));
	// 		}
	// 	}else{
	// 		session_destroy();
	// 		redirect(site_url(array('Administration','formulaireConnexion')));
	// 	}

	// }
	public function formulairemodifCategorie()
	{
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST) and !empty($_POST)) {
				$data['nom'] = $_POST['nom'];
				$data['date_modif'] = $_POST['date_modif'];
				$this->Categorie->hydrate($data);
				$this->Categorie->UpdateCategorie($_POST['id']);
				redirect(site_url(array('Administration', 'listedescategories')));
			} else {
				redirect(site_url(array('Administration', 'listedescategories')));
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}

	public function formulairemodifSecteur()
	{
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST) and !empty($_POST)) {
				$data['nom'] = htmlspecialchars($_POST['nom']);
				$data['date_modif'] = htmlspecialchars($_POST['date_modif']);
				$this->Secteur->hydrate($data);
				$this->Secteur->UpdateSecteur($_POST['id']);
				redirect(site_url(array('Administration', 'listedessecteurs')));
			} else {
				redirect(site_url(array('Administration', 'listedessecteurs')));
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}
	public function supprimerCategorie()
	{
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST) and !empty($_POST)) {
				if ($_POST['id'] == 0) {
					$_SESSION['message_error'] = "Echec de suppression de categorie!";
					$this->session->mark_as_flash('message_error');
					redirect(site_url(array('Administration', 'listedescategories')));
				} else {
					$this->Categorie->deleteCategorie($_POST['id']);
					$_SESSION['message_save'] = "Categorie supprimée avec succes!";
					$this->session->mark_as_flash('message_save');
					redirect(site_url(array('Administration', 'listedescategories')));
				}
			} else {
				$_SESSION['message_error'] = "Echec de suppression de categorie!";
				$this->session->mark_as_flash('message_error');
				redirect(site_url(array('Administration', 'listedescategories')));
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}

	public function supprimerSecteur()
	{
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST) and !empty($_POST)) {
				if ($_POST['id'] == 0) {
					$_SESSION['message_error'] = "Echec de suppression de categorie!";
					$this->session->mark_as_flash('message_error');
					redirect(site_url(array('Administration', 'listedessecteurs')));
				} else {
					$this->Secteur->deleteSecteur($_POST['id']);
					$_SESSION['message_save'] = "Secteur supprimée avec succes!";
					$this->session->mark_as_flash('message_save');
					redirect(site_url(array('Administration', 'listedessecteurs')));
				}
			} else {
				$_SESSION['message_error'] = "Echec de suppression de secteur!";
				$this->session->mark_as_flash('message_error');
				redirect(site_url(array('Administration', 'listedessecteurs')));
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}

	public function deconnexion()
	{
		if (isset($_SESSION['ADMIN'])) {
			session_destroy();
		}
		redirect(site_url(array('Administration', 'index')));
	}

	public function testmail()
	{
		if (isset($_SESSION['ADMIN'])) {

			$to  = $_POST['email'];
			$headers = "From:inchentreprise237@gmail.com ";
			$body = "Message: ceci est un test \r\n";
			mail($to, "Confirmation d'Inscription", $body, $headers);


			// $dest =$_POST['email'];
			// $sujet = "ENREGISTREMENT DU VOL DE VOTRE VEHICULE ";
			// $corp = "BONJOUR ! LA DECLARATION DU VOL DE VOTRE VEHICULE A BIEN ETE ENREGISTREE. NOS EQUIPES VOUS CONTACTERONT PLUS TARD POUR VOUS DONNER L'ETAT D'AVANCEMENT DE LA RECHERCHE.";
			// $headers = "From: inchEntreprise@gmail.com";

			// redirect(site_url(array('Administration','index')));

		}
		redirect(site_url(array('Administration', 'formulaireconnexion')));
	}
}
