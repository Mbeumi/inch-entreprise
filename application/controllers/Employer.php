<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Employer extends CI_Controller{

    public function index()
    {

        if (isset($_SESSION['EMPLOYER'])) {

            $data1['Allentreprise'] = $this->Entreprise->findEntreprise($_SESSION['EMPLOYER']['id_entreprise']);
            $data['Alluser'] = $this->Users->findUsers($_SESSION['EMPLOYER']['id_users']);
            $data1['Id_departement'] = $this->Employer->findDepartementEmployer($_SESSION['EMPLOYER']['id']); 
			$data1['Id_responsabilite'] = $this->Employer->findResponsabiliteEmployer($_SESSION['EMPLOYER']['id']);
            $data1['departement'] = $this->Departement->findDepartementId($data1['Id_departement']['id_departement']);
			$data1['responsabilite'] = $this->Responsabilite->findResponsabiliteId($data1['Id_responsabilite']['id_responsabilite']);
            $this->load->view('EMPLOYERS/index', $data);
            $this->load->view('EMPLOYERS/navigation', $data1);
            $this->load->view('EMPLOYERS/home');
            $this->load->view('ADMIN/footer');
        } else {
            session_destroy();
            redirect(site_url(array('Employer', 'formulaireConnexion')));
        }
    }



    public function manageConnexion(){

		if (isset($_POST['identifiant']) && isset($_POST['password'])) {

			$employer = $this->Employer->findAllEmployerBd();

			for ($i = 0; $i < $employer['total']; $i++) {
				$employer['infos'] = $this->Users->findUsers($employer[$i]['id_users']);
				if ($employer[$i]['identifiant'] == htmlspecialchars($_POST['identifiant']) && $employer[$i]['password'] == htmlspecialchars(md5($_POST['password'])) ) {
					$_SESSION['EMPLOYER'] = $employer[$i];
				} elseif ($employer['infos']['email'] == htmlspecialchars($_POST['identifiant']) && $employer[$i]['password'] == htmlspecialchars(md5($_POST['password']))) {
					$_SESSION['EMPLOYER'] = $employer[$i];
				}
			}
			
			if (isset($_SESSION['EMPLOYER']) ) {
				if($_SESSION['EMPLOYER']['statut']==1){
                    $Entreprise = $this->Entreprise->findEntreprise($_SESSION['EMPLOYER']['id_entreprise']);
                    if($Entreprise['statut']==1){
                        redirect(site_url(array('Employer','index')));
                    }else{
                        $_SESSION['message_error'] = " Acces a cette entreprise suspendu!";
                        $this->session->mark_as_flash('message_error'); 
                        redirect(site_url(array('Employer','formulaireConnexion')));
                    }
					
				}else{
					$_SESSION['message_error'] = " Acces bloqué veillez contacter l'administrateur!";
					$this->session->mark_as_flash('message_error'); 
					redirect(site_url(array('Employer','formulaireConnexion')));
				}
					
			}else{
				$_SESSION['message_error'] = " Adresse email ou mot de passe incorrect!";
				$this->session->mark_as_flash('message_error'); 
				redirect(site_url(array('Employer','formulaireConnexion')));

			}
		} else {
			redirect(site_url(array('Employer', 'formulaireConnexion')));
		}
	}

	public function formulaireConnexion(){

		if (isset($_SESSION['EMPLOYER'])) {
			if (isset($_SESSION['EMPLOYER'])) {
				
				if($_SESSION['EMPLOYER']['statut']==1){
                    $Entreprise = $this->Entreprise->findEntreprise($_SESSION['EMPLOYER']['id_entreprise']);
                    if($Entreprise['statut']==1){
                        redirect(site_url(array('Employer','index')));
                    }else{
                        $_SESSION['message_error'] = " Acces a cette entreprise suspendu!";
                        $this->session->mark_as_flash('message_error'); 
                        $this->load->view('gestion_employer/formulaire_connexion');
                    }
					
				}else{
					session_destroy();
					$_SESSION['message_error'] = "Acces bloqué veillez contacter l'administrateur!";
					$this->session->mark_as_flash('message_error'); 
					$this->load->view('gestion_employer/formulaire_connexion');	
				}
				
			} else {
				session_destroy();
				redirect(site_url(array('Employer', 'formulaireConnexion')));
			}
		} else {
			$this->load->view('gestion_employer/formulaire_connexion');
		}
	}


	public function forgetpassword(){
		$this->load->view('gestion_employer/index');
		
	}

	public function forgetpasswordentrernew(){
		$data['employer']=$_SESSION['EMAIL'];
		$this->load->view('gestion_employer/entrernewpwd',$data);
		
	}


	public function chainealeatoire(){
		$str = "0123456789abcdefghijklmnopkrstvwyzABCDFGHIJKLMOPQRSTVWYZ";
		   $str = str_shuffle($str);
		   return substr($str, 0, 8);
   }

	public function confirmpasswordemail(){
		if (isset($_POST['email'])) {
			$email=htmlspecialchars($_POST['email']);
			$data['employer']=$this->Users->findUsersemail($email);
			if ($data['employer']['data']=='ok') {
				$code=$this->chainealeatoire();
				$data1['password']=$code;
				$this->Proprietaire->hydrate($data1);
				$this->Proprietaire->Updatepasswordproprietaire($data['employer']['id']);
				$to  = $email;
				$headers = "From: inchentreprise237@gmail.com";
				$body="Bonsoir Mr/Mme, ci-dessous le code de confirmation de votre adresse email pour la reinitialisation de votre mot de passe ".' '.$code;
				$tete="Inch Entreprise: Confirmation d'adresse mail";
				mail($to, $tete, $body, $headers);
				$_SESSION['EMAIL']=$data['employer'];
				$this->load->view('gestion_employer/confirmpwd',$data);
			}else{
				$_SESSION['message_error']="Cette adresse mail ne correspond a aucun compte!";
				$this->session->mark_as_flash('message_error');
				redirect(site_url(array('Employer','forgetpassword')));
			}
			
		}else{
		 session_destroy();
		 redirect(site_url(array('Employer','forgetpassword')));
	 }	   
		
	}

	public function confirmcodemail(){
		$codeemail=$this->Proprietaire->findpassword($_POST['id']);
		if($codeemail['password']==$_POST['code']){
			redirect(site_url(array('Employer','forgetpasswordentrernew')));
		}else{
			$_SESSION['message_error']="Le code que vous avez inscrit n'est pas correct!";
			$this->session->mark_as_flash('message_error');
			redirect(site_url(array('Employer','confirmpwd')));
	
		}
	    
	}

	public function enregupdatepwd(){
		if(isset($_POST['password']) AND !empty($_POST['password'])){
          $password=htmlspecialchars(md5($_POST['password']));
          $data2['password'] = $password;
		  $this->Proprietaire->hydrate($data2);
		  $this->Proprietaire->Updatepasswordproprietaire($_POST['id']);
		  $_SESSION['message_save']="Modification de mot de passe avec succes!";
		  $this->session->mark_as_flash('message_save');
		  redirect(site_url(array('Employer','formulaireConnexion')));
		}else{
			$_SESSION['message_error']="Une erreur est subvenu veillez recommencer!";
			$this->session->mark_as_flash('message_error');
			redirect(site_url(array('Employer','formulaireConnexion')));
		}
		
	}

	public function Adddemandeconge(){
		if (isset($_SESSION['EMPLOYER'])) {
			$data1['Allentreprise'] = $this->Entreprise->findEntreprise($_SESSION['EMPLOYER']['id_entreprise']);
            $data['Alluser'] = $this->Users->findUsers($_SESSION['EMPLOYER']['id_users']);
            $data1['Id_departement'] = $this->Employer->findDepartementEmployer($_SESSION['EMPLOYER']['id']); 
			$data1['Id_responsabilite'] = $this->Employer->findResponsabiliteEmployer($_SESSION['EMPLOYER']['id']);
            $data1['departement'] = $this->Departement->findDepartementId($data1['Id_departement']['id_departement']);
			$data1['responsabilite'] = $this->Responsabilite->findResponsabiliteId($data1['Id_responsabilite']['id_responsabilite']);
            $this->load->view('EMPLOYERS/index', $data);
            $this->load->view('EMPLOYERS/navigation', $data1);
            $this->load->view('EMPLOYERS/pageconstruct');
            $this->load->view('ADMIN/footer');
        } else {
            session_destroy();
            redirect(site_url(array('Employer', 'formulaireConnexion')));
        }
	}

    public function deconnexion() {
        if (isset($_SESSION['EMPLOYER'])) {
            session_destroy();
        }
        redirect(site_url(array('Employer', 'index')));
    }


}
