<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proprietaire extends CI_Controller
{


    public function index(){


        if (isset($_SESSION['PROPRIETAIRE'])) {

            
            $data1['Allcategorie'] = $this->Categorie->findAllCategorieBd();
            $data1['Allsecteur'] = $this->Secteur->findAllSecteurBd();
            $data1['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
            $data['Alluser'] = $this->Users->findUsers($_SESSION['PROPRIETAIRE']['id_users']);
			$data['Allresponsabilite'] = $this->Responsabilite->findResponsabiliteEntreprise($data1['Allentreprise']['id']);
			$data['Alldepartement'] = $this->Departement->findDepartement($data1['Allentreprise']['id']);
			$data['Allemployers'] = $this->Employer->findEmployer($data1['Allentreprise']['id']);
			$data1['active']=1;
            $this->load->view('PROPRIETAIRE/index', $data);
            $this->load->view('PROPRIETAIRE/navigationprop', $data1);
            $this->load->view('PROPRIETAIRE/homeprop');
            $this->load->view('ADMIN/footer');
        } else {
            session_destroy();
            redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
        }
    }

    public function profilentreprises(){

		
		if (isset($_SESSION['PROPRIETAIRE']) ) {
				
			$data['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
            $data['Alluser'] = $this->Users->findUsers($_SESSION['PROPRIETAIRE']['id_users']);
		   	$data1['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
            $data1['Allcategorie'] = $this->Categorie->findCategorie($data1['Allentreprise']['id_categorie']);
            $data1['Allsecteur'] = $this->Secteur->findSecteur($data1['Allentreprise']['id_secteur']);
			$data['active']=2;
		   	$this->load->view('PROPRIETAIRE/index',$data);
		   	$this->load->view('PROPRIETAIRE/navigationprop',$data);
		   	$this->load->view('PROPRIETAIRE/profil_entreprises',$data1);
		   	$this->load->view('ADMIN/footer');   
	   }else{
		   session_destroy();
		redirect(site_url(array('Proprietaire','formulaireConnexion')));
	   		}
   	}


    
	public function Modifprofilentreprise(){
		
            if (isset($_SESSION['PROPRIETAIRE'])) {
               $data['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
               $data['Alluser'] = $this->Users->findUsers($_SESSION['PROPRIETAIRE']['id_users']);
               $data1['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
               $data1['Allcategorie'] = $this->Categorie->findCategorie($data1['Allentreprise']['id_categorie']);
               $data1['Allsecteur'] = $this->Secteur->findSecteur($data1['Allentreprise']['id_secteur']);
			   $data['active']=3;
               $this->load->view('PROPRIETAIRE/index',$data);
               $this->load->view('PROPRIETAIRE/navigationprop',$data);
               $this->load->view('PROPRIETAIRE/modif_entreprise', $data1);
               $this->load->view('ADMIN/footer');
           
                   
           }else{
            	session_destroy();
            	redirect(site_url(array('Proprietaire','formulaireConnexion')));
                }
    }

    public function EnregmodifEntreprise(){
		if (isset($_SESSION['PROPRIETAIRE'])) {
			if (isset($_POST)) {
				if (isset($_FILES['profil']) and !empty($_FILES['profil']['name'])) {
					if (isset($_FILES['profil']) and $_FILES['profil']['error'] == 0) {
						// Testons si le fichier n'est pas trop gros
						if ($_FILES['profil']['size'] <= 100000000) {
							// Testons si l'extension est autorisée
							$infosfichier = pathinfo($_FILES['profil']['name']);
							$extension_upload = $infosfichier['extension'];

							$config = $_FILES['profil']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['id'];
							$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable . '.' . $extension_upload;
							move_uploaded_file($_FILES['profil']['tmp_name'], 'assets/images/user_profil/' . $config);
							$data['profil'] = $config;
						} else {
							$_SESSION['message_error'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
							$data['message'] = 'non';
						}
					} else {
						$_SESSION['message_error'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
						$data['message'] = 'non';
					}
				} else {

					$data['profil'] = htmlspecialchars($_POST['profil_pass']);
				}
				$data['nom'] = htmlspecialchars($_POST['nom']);
				$data['localisation'] = htmlspecialchars($_POST['localisation']);


				$this->Entreprise->hydrate($data);
				$this->Entreprise->UpdateEntrepriseproprietaire($_POST['id']);
				$_SESSION['message_save'] = "Modification des informations d'entreprise enregistré avec success!";

				$this->session->mark_as_flash('message_save');

				redirect(site_url(array('Proprietaire', 'profilentreprises')));
			} else {
				redirect(site_url(array('Proprietaire', 'touteslesentreprises')));
				$_SESSION['message_error'] = "Une Erreur est subvenu veillez reprendre le processus!";
				$this->session->mark_as_flash('message_error');
			}
		} else {
			session_destroy();
			redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
		}
	}

	public function AddEmployer(){
		
		if (isset($_SESSION['PROPRIETAIRE'])) {
           $data1['Allcategorie'] = $this->Categorie->findAllCategorieBd();
           $data1['Allsecteur'] = $this->Secteur->findAllSecteurBd();
           $data1['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
           $data['Alluser'] = $this->Users->findUsers($_SESSION['PROPRIETAIRE']['id_users']);
		   $data1['Alldepartement'] = $this->Departement->findDepartement($data1['Allentreprise']['id']);
		   $data1['Allresponsabilite'] = $this->Responsabilite->findResponsabiliteEntreprise($data1['Allentreprise']['id']); 
			for($i=0; $i<$data1['Alldepartement']['total']; $i++){
				$data1['departement'][$i] = $data1['Alldepartement'][$i];
			}
			for($i=0; $i<$data1['Allresponsabilite']['total']; $i++){
			 	$data1['responsabilite'][$i] = $data1['Allresponsabilite'][$i]	;
			}		
		   $data1['active']=4;
		   $this->load->view('PROPRIETAIRE/index',$data);
		   $this->load->view('PROPRIETAIRE/navigationprop',$data1);
		   $this->load->view('PROPRIETAIRE/formulaire_employers');
		   $this->load->view('ADMIN/footer');
	   
			   
	   }else{
		   session_destroy();
		redirect(site_url(array('Proprietaire','formulaireConnexion')));
			}
	}

	public function Touslesemployers(){
		
		if (isset($_SESSION['PROPRIETAIRE'])) {
           $data1['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
           $data['Alluser'] = $this->Users->findUsers($_SESSION['PROPRIETAIRE']['id_users']);
		   $data1['Allemployers'] = $this->Employer->findEmployer($data1['Allentreprise']['id']);
		   for($i=0; $i<$data1['Allemployers']['total']; $i++){
			  $data1['Employer'][$i] = $this->Users->findUsers($data1['Allemployers'][$i]['id_users']);  
		   	  $data1['Responsabilite'] = $this->Responsabilite->findResponsabiliteId($data1['Allemployers'][$i]['id_responsabilite']);
			
			}
			$data1['Alldepartement'] = $this->Departement->findDepartement($data1['Allentreprise']['id']);
			for($i=0; $i<$data1['Allemployers']['total']; $i++){
				 $data1['Departement'][$i] = $this->Departement->findDepartementId($data1['Allemployers'][$i]['id_departement']);
			}
		  	$data1['active']=5;

		   $this->load->view('PROPRIETAIRE/index',$data);
		   $this->load->view('PROPRIETAIRE/navigationprop',$data1);
		   $this->load->view('PROPRIETAIRE/Touslesemployers',$data1);
		   $this->load->view('ADMIN/footer');
	   
			   
	   }else{
		   session_destroy();
		redirect(site_url(array('Proprietaire','formulaireConnexion')));
			}
	}



	public function profil_employer(){
		
		if (isset($_SESSION['PROPRIETAIRE'])) {

           $data1['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
           $data['Alluser'] = $this->Users->findUsers($_SESSION['PROPRIETAIRE']['id_users']);
		//    $data1['employerss'] = $this->Employer->findResposabiliteIdEmployer($_POST['id_users']);
		   $data1['EmployerPerso']=$this->Employer->findAllInfosEmployer($_POST['id_users']);
		   $data1['Allemployers'] = $this->Employer->findEmployer($_POST['id_entreprise']);
		   $data1['Employer'] = $this->Users->findUsers($_POST['id_users']); 
		   $data1['Alldepartement'] = $this->Departement->findDepartement($_POST['id_entreprise']);
		   for($i=0; $i<$data1['Allemployers']['total']; $i++){
				$data1['Departement'] = $this->Departement->findDepartementId($data1['Allemployers'][$i]['id_departement']);
		   $data1['Allresponsabilite'] = $this->Responsabilite->findResponsabilite($data1['Allemployers'][$i]['id_departement']);
		}

		   $data1['Responsabilite'] = $this->Responsabilite->findResponsabiliteId($data1['EmployerPerso']['id_responsabilite']);


		   $data1['active']=5;

		   
		   $this->load->view('PROPRIETAIRE/index',$data);
		   $this->load->view('PROPRIETAIRE/navigationprop',$data1);
		   $this->load->view('PROPRIETAIRE/profil_employer');
		   $this->load->view('ADMIN/footer');
	   
			   
	   }else{
		   session_destroy();
		redirect(site_url(array('Proprietaire','formulaireConnexion')));
			}
	}



	
	public function Bloqueremployer(){
		
		if (isset($_SESSION['PROPRIETAIRE'])) { 
			if (isset($_POST) AND !empty($_POST)) {
				$data['statut'] =htmlspecialchars($_POST['statut']);
				$this->Employer->hydrate($data);
				$this->Employer->Updatestatutemployer($_POST['cible']);
				$_SESSION['message_save'] = "Employer bloqué avec succes!";
				$this->session->mark_as_flash('message_save'); 
				redirect(site_url(array('Proprietaire','Touslesemployers')));

			}else{
				$_SESSION['message_error'] = "Désole votre requete n'a pas été enregistré!";
				$this->session->mark_as_flash('message_error'); 
				redirect(site_url(array('Proprietaire','Touslesemployers')));
			}
	   }else{
		   session_destroy();
		redirect(site_url(array('Proprietaire','formulaireConnexion')));
			}
	}


	public function Restaureremployer(){
		
		if (isset($_SESSION['PROPRIETAIRE'])) { 
			if (isset($_POST) AND !empty($_POST)) {
				$data['statut'] =htmlspecialchars($_POST['statut']);
				$this->Employer->hydrate($data);
				$this->Employer->Updatestatutemployer($_POST['cible']);
				$_SESSION['message_save'] = "Employer restauré avec succes!";
				$this->session->mark_as_flash('message_save'); 
				redirect(site_url(array('Proprietaire','Touslesemployers')));

			}else{

				$_SESSION['message_error'] = "Désole votre requete n'a pas été enregistré!";
				$this->session->mark_as_flash('message_error'); 
				redirect(site_url(array('Proprietaire','Touslesemployers')));

				}
	   }else{
		   session_destroy();
		redirect(site_url(array('Proprietaire','formulaireConnexion')));
			   }
	}


	public function ModifprofilEmployer(){
		
		if (isset($_SESSION['PROPRIETAIRE'])) {
			$data1['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
			$data['Alluser'] = $this->Users->findUsers($_SESSION['PROPRIETAIRE']['id_users']);
			$data1['Allemployers'] = $this->Employer->findEmployer($_POST['id_entreprise']);
		
			$data1['EmployerPerso'] = $this->Employer->findAllInfosEmployer($_POST['id_users']);
			$data1['Employer'] = $this->Users->findUsers($_POST['id_users']);
			$data1['Alldepartement'] = $this->Departement->findDepartement($_POST['id_entreprise']);
			$data1['Departement'] = $this->Departement->findDepartementId($data1['EmployerPerso']['id_departement']);
			for($i=0; $i<$data1['Allemployers']['total']; $i++){
				 $data1['Allresponsabilite'] = $this->Responsabilite->findResponsabilite($data1['Allemployers'][$i]['id_departement']);
			} 
		    $data1['Responsabilite'] = $this->Responsabilite->findResponsabiliteId($data1['EmployerPerso']['id_responsabilite']);

			$data1['Alldepartement'] = $this->Departement->findDepartement($_POST['id_entreprise']);
			for($i=0; $i<$data1['Alldepartement']['total']; $i++){
				$data1['departement'][$i] = $data1['Alldepartement'][$i];
			}
			for($i=0; $i<$data1['Allresponsabilite']['total']; $i++){
			 	$data1['responsabilite'][$i] = $data1['Allresponsabilite'][$i]	;
			}	
			$data1['active']=5;

			$this->load->view('PROPRIETAIRE/index',$data);
			$this->load->view('PROPRIETAIRE/navigationprop',$data1);
			$this->load->view('PROPRIETAIRE/modif_profil_employer', $data1);
			$this->load->view('ADMIN/footer');
			   
					   
			   }else{
				   session_destroy();
				redirect(site_url(array('Proprietaire','formulaireConnexion')));
					   }
	}



	
	

	public function listedesdepartements(){
		
		if (isset($_SESSION['PROPRIETAIRE'])){
		   $data['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
		   $data['Alluser'] = $this->Users->findUsers($_SESSION['PROPRIETAIRE']['id_users']);
		   $data1['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
		   $data1['Alldepartement'] = $this->Departement->findDepartement($data1['Allentreprise']['id']);
		   $data['active']=8;
		   $this->load->view('PROPRIETAIRE/index',$data);
		   $this->load->view('PROPRIETAIRE/navigationprop',$data);
		   $this->load->view('PROPRIETAIRE/listedesdepartements',$data1);
		   $this->load->view('ADMIN/footer');
	   
			   
	   }else{
		   session_destroy();
		redirect(site_url(array('Proprietaire','formulaireConnexion')));
			}

	}



	public function EnregDepartement(){
		if (isset($_SESSION['PROPRIETAIRE'])) {
			if (isset($_POST)) {
				$data['nom'] = htmlspecialchars($_POST['nom']);
				$data['reference'] = htmlspecialchars($_POST['reference']);
				$data['chef_departement'] = htmlspecialchars($_POST['chef_departement']);
				$data['id_entreprise'] = htmlspecialchars($_POST['id_entreprise']);
				$data['statut'] = htmlspecialchars($_POST['statut']);
				$data['date_creation'] = date('Y-m-d H:i:s');
				$this->Departement->hydrate($data);
				$this->Departement->addDepartement();
				$_SESSION['message_save'] = "Departement enregistré avec success!";
				$this->session->mark_as_flash('message_save');
				$_SESSION['success'] = 'ok';
				redirect(site_url(array('Proprietaire', 'listedesdepartements')));
			} else {
				redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
				$_SESSION['message_error'] = "Une Erreur est subvenu recommencer!";
				}
		} else {
			session_destroy();
			redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
				}
	}

	public function formulairemodifdepartement(){
		if (isset($_SESSION['PROPRIETAIRE'])) {
			if (isset($_POST) and !empty($_POST)) {
				$data['nom'] = htmlspecialchars($_POST['nom']);
				$data['reference'] = htmlspecialchars($_POST['reference']);
				$this->Departement->hydrate($data);
				$this->Departement->UpdateDepartement($_POST['id']);
				$_SESSION['message_save'] = "Modification éffectué avec success!";
				$this->session->mark_as_flash('message_save');
				redirect(site_url(array('Proprietaire', 'listedesdepartements')));
			} else {
				redirect(site_url(array('Proprietaire', 'listedesdepartements')));
				$_SESSION['message_error'] = "Echecs de modifications!";
				$this->session->mark_as_flash('message_error');
			}
		} else {
			session_destroy();
			redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
		}
	}

	// fonction pour supprimer un departement

	public function supprimerDepartement()
	{
		if (isset($_SESSION['PROPRIETAIRE'])) {
			if (isset($_POST) and !empty($_POST)) {
				$this->Departement->deleteDepartement($_POST['id']);
				redirect(site_url(array('Proprietaire', 'listedesdepartements')));
				$_SESSION['message_save'] = "Suppression éffectué avec success!";
				$this->session->mark_as_flash('message_save');
			} else {
				redirect(site_url(array('Proprietaire', 'listedesdepartements')));
				$_SESSION['message_error'] = "Echecs de suppression!";
				$this->session->mark_as_flash('message_error');
			}
		} else {
			session_destroy();
			redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
		}
	}


	public function listedesresponsabilites(){
		
		if (isset($_SESSION['PROPRIETAIRE'])){
			$data['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
			$data['Alluser'] = $this->Users->findUsers($_SESSION['PROPRIETAIRE']['id_users']);
			$data1['Alldepartement'] = $this->Departement->findDepartement($data['Allentreprise']['id']);
			// $data1['departement'] = $this->Departement->findDepartementAllId($data1['Alldepartement']['id']);
			$data['active']=9;
			
			$data1['responsabilite'] = $this->Responsabilite->findResponsabiliteEntreprise($data['Allentreprise']['id']);
            for($i=0; $i<$data1['responsabilite']['total']; $i++){
				$data1['departement'][$i]=$this->Departement->findDepartementId($data1['responsabilite'][$i]['id_departement']);
			}
			$this->load->view('PROPRIETAIRE/index',$data);
			$this->load->view('PROPRIETAIRE/navigationprop',$data);
			$this->load->view('PROPRIETAIRE/listedesresponsabilites',$data1);
			$this->load->view('ADMIN/footer');
		   
				   
		   }else{
			   session_destroy();
				redirect(site_url(array('Proprietaire','formulaireConnexion')));
				}

	}



	// enregister une responsabilite
	public function EnregResponsabilite()
	{
		if (isset($_SESSION['PROPRIETAIRE'])) {
			if (isset($_POST)) {
				$data['nom'] = htmlspecialchars($_POST['nom']);
				$data['reference'] = htmlspecialchars($_POST['reference']);
				$data['statut'] = htmlspecialchars($_POST['statut']);
				$data['id_entreprise'] = htmlspecialchars($_POST['id_entreprise']);
				$data['id_departement'] = htmlspecialchars($_POST['id_departement']);
				$data['description'] = htmlspecialchars($_POST['description']);
				$data['date_creation'] = date('Y-m-d H:i:s');
				$this->Responsabilite->hydrate($data);
				$this->Responsabilite->addResponsabilite();
				$_SESSION['message_save'] = "Responsabilite enregistré avec success !!";
				$this->session->mark_as_flash('message_save');
				$_SESSION['success'] = 'ok';
				redirect(site_url(array('Proprietaire', 'listedesresponsabilites')));
			} else {
				redirect(site_url(array('Proprietaire', 'listedesresponsabilites')));
				$_SESSION['message_error'] = "Une Erreur est subvenu recommencer !!";
				$this->session->mark_as_flash('message_error');
			}
		} else {
			session_destroy();
			redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
		}
	}


	//  fonction pour modifier les informations d'une responsabilite



		public function formulairemodifresponsabilite(){
			if (isset($_SESSION['PROPRIETAIRE'])) {
				if (isset($_POST) AND !empty($_POST)) {
					$data['nom']=htmlspecialchars($_POST['nom']);
					$data['reference']=htmlspecialchars($_POST['reference']);
					$data['id_departement']=htmlspecialchars($_POST['id_departement']);
					$data['description']=htmlspecialchars($_POST['description']);
					$data['statut']= $_POST['statut'];
					$this->Responsabilite->hydrate($data);
					$this->Responsabilite->UpdateResponsabilite($_POST['id']);
					$_SESSION['message_save']="Modification éffectué avec success!";
					$this->session->mark_as_flash('message_save');
					redirect(site_url(array('Proprietaire','listedesresponsabilites')));
				}else{
					redirect(site_url(array('Proprietaire','listedesresponsabilites')));
					$_SESSION['message_error']="Echecs de suppression!";
					$this->session->mark_as_flash('message_error');
				}
			}else{
				session_destroy();
				redirect(site_url(array('Proprietaire','formulaireConnexion')));
				}
		}


	// fonction pour supprimer une responsabilite

		public function supprimerResponsabilite(){
			if (isset($_SESSION['PROPRIETAIRE'])) {
				if (isset($_POST) AND !empty($_POST)) {
					 $this->Responsabilite->deleteResponsabilite($_POST['id']);
					 $_SESSION['message_save']="Suppression éffectué avec success!";
					 $this->session->mark_as_flash('message_save');
					 redirect(site_url(array('Proprietaire','listedesresponsabilites')));
				}else{
					$_SESSION['message_error']="Suppression éffectué avec success!";
					$this->session->mark_as_flash('message_error');
					redirect(site_url(array('Proprietaire','listedesresponsabilites')));
				}

			} else {
				session_destroy();
				redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
					}
		}

	public function EnregEmployers(){

		// $this->form_validation->set_message(array('required'=>'ce champ est obligatoire','min_length'=>'Un minimun de caractere est requis','matches'=>'Le champ de {field} ne correspond pas au champ {param}.','is_unique'=>'Cette addresse mail existe deja'));
		// $this->form_validation->set_rules('nom','nom','trim|required|min_length[4]');
		// $this->form_validation->set_rules('prenom','prenom','trim|required|min_length[4]');
		// $this->form_validation->set_rules('date_naissance','date_naissance','required');
		// $this->form_validation->set_rules('sexe','sexe','required');
		// $this->form_validation->set_rules('telephone','telephone','required|min_length[9]');
		// $this->form_validation->set_rules('numero_contrib','numero_contrib','required|min_length[10]');
		// $this->form_validation->set_rules('nationalite','nationalite','required');
		// $this->form_validation->set_rules('age','age','required');
		// $this->form_validation->set_rules('sexe','sexe','required');
		// $this->form_validation->set_rules('regime_matrimo','regime_matrimo','required');
		// $this->form_validation->set_rules('etat_sante','etat_sante','required');
		// $this->form_validation->set_rules('nbre_enfante','nbre_enfant','required');
		// $this->form_validation->set_rules('email','email','required|valid_email|is_unique[proprietaire.email]'); 
		// $this->form_validation->set_rules('nom','nom','trim|required|min_length[4]');
		// $this->form_validation->set_rules('nom','nom','trim|required|min_length[4]');
		// $this->form_validation->set_rules('nom','nom','trim|required|min_length[4]');
		if (isset($_SESSION['PROPRIETAIRE'])) {
			if (isset($_POST)) {
				if (isset($_FILES['profil']) and $_FILES['profil']['error'] == 0) {
					// Testons si le fichier n'est pas trop gros
					if ($_FILES['profil']['size'] <= 100000000) {
						// Testons si l'extension est autorisée
						$infosfichier = pathinfo($_FILES['profil']['name']);
						$extension_upload = $infosfichier['extension'];
						$config = $_FILES['profil']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['PROPRIETAIRE']['id'];
						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable . '.' . $extension_upload;
						move_uploaded_file($_FILES['profil']['tmp_name'], 'assets/images/user_profil/' . $config);
						$data['profil'] = $config;
					} else {
						$_SESSION['message_error'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
						$this->session->mark_as_flash('message_error');
						$data['message'] = 'non';
					}
				} else {
					$_SESSION['message_error'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
					$this->session->mark_as_flash('message_error');
					$data['message'] = 'non';
				}
                $data0['email'] = $this->Users->findUsersemail($_POST['email']);
				if($data0['email']['data']=='ok'){
					$_SESSION['message_error'] = "Cette adresse email existe déje veillez la changer!";
					$this->session->mark_as_flash('message_error');
					redirect(site_url(array('Proprietaire', 'AddEmployer')));
				}

				$data['nom'] = htmlspecialchars($_POST['nom']);
				$data['prenom'] = htmlspecialchars($_POST['prenom']);
				$data['date_naissance'] = htmlspecialchars($_POST['date_naissance']);
				$data['sexe'] = htmlspecialchars($_POST['sexe']);
				$data['telephone'] = htmlspecialchars($_POST['telephone']);
				$data['numero_cni'] = htmlspecialchars($_POST['numero_cni']);
				$data['nationalite'] = htmlspecialchars($_POST['nationalite']);
				$data['age'] = htmlspecialchars($_POST['age']);
				$data['regime_matrimo'] = htmlspecialchars($_POST['regime_matrimo']);
				$data['etat_sante'] = htmlspecialchars($_POST['etat_sante']);
				$data['nbre_enfant'] = htmlspecialchars($_POST['nbre_enfant']);
				$data['email'] = htmlspecialchars($_POST['email']);
				$data1['identifiant'] = htmlspecialchars($_POST['identifiant']);
				$data1['password'] = htmlspecialchars( md5($_POST['password']));
				$data1['statut'] = htmlspecialchars($_POST['statut']);
				$data1['id_entreprise'] = htmlspecialchars($_POST['id_entreprise']);
				$data1['id_departement'] = htmlspecialchars($_POST['id_departement']);
				$data1['id_responsabilite'] = htmlspecialchars($_POST['id_responsabilite']);
				

				// $data['date']=date('Y-m-d H:i:s');
				$this->Users->hydrate($data);
				$this->Users->addUsers();
				$data1['id_users'] = $this->Users->findlastUser()['id_users'];
				$this->Employer->hydrate($data1);
				$this->Employer->addEmployer();
				$_SESSION['message_save'] = "Employers  enregistré avec success !!";
				$this->session->mark_as_flash('message_save');

				$_SESSION['success'] = 'ok';
				redirect(site_url(array('Proprietaire', 'Touslesemployers')));
			} else {
				redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
				$_SESSION['message_error'] = "Une Erreur est subvenu veillez reprendre !!";
				$this->session->mark_as_flash('message_error');
			}
		} else {
			session_destroy();
			redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
		}
	}


	public function EnregmodifEmployer(){
		if (isset($_SESSION['PROPRIETAIRE'])) {
			if (isset($_POST)) {
				if(isset($_FILES['profil']) AND !empty($_FILES['profil']['name'])){
					if (isset($_FILES['profil']) AND $_FILES['profil']['error'] == 0 ){
							// Testons si le fichier n'est pas trop gros
							if ($_FILES['profil']['size'] <= 100000000){
								// Testons si l'extension est autorisée
								$infosfichier =pathinfo($_FILES['profil']['name']);
								$extension_upload = $infosfichier['extension'];
	
								$config =$_FILES['profil']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['ADMIN']['id'];
								 $ma_variable = str_replace('.', '_', $config);
								$ma_variable = str_replace(' ', '_', $config);
								$config = $ma_variable.'.'.$extension_upload;
								move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
								$data['profil']=$config;
								
							}else{
									$_SESSION['message_error']="La taille du fichier choisie  est très grande veuillez le remplacer svp!";

									$this->session->mark_as_flash('message_error'); 
									$data['message']='non';
							}
					}else{
							$_SESSION['message_error']="L'image choisie  est endommagée  veuillez le remplacer svp!";

							$this->session->mark_as_flash('message_error'); 
							$data['message']='non';
					}
				}else{

					$data['profil']=htmlspecialchars($_POST['profil_pass']);
								
				}
				$data['nom'] = htmlspecialchars($_POST['nom']);
				$data['prenom'] = htmlspecialchars($_POST['prenom']);
				$data['date_naissance'] = htmlspecialchars($_POST['date_naissance']);
				$data['sexe'] = htmlspecialchars($_POST['sexe']);
				$data['telephone'] = htmlspecialchars($_POST['telephone']);
				$data['numero_cni'] = htmlspecialchars($_POST['numero_cni']);
				$data['nationalite'] = htmlspecialchars($_POST['nationalite']);
				$data['age'] = htmlspecialchars($_POST['age']);
				$data['regime_matrimo'] = htmlspecialchars($_POST['regime_matrimo']);
				$data['etat_sante'] = htmlspecialchars($_POST['etat_sante']);
				$data['nbre_enfant'] = htmlspecialchars($_POST['nbre_enfant']);
				$data['email'] = htmlspecialchars($_POST['email']);
				$data1['identifiant'] = htmlspecialchars($_POST['identifiant']);
				$data1['id_entreprise'] = htmlspecialchars($_POST['id_entreprise']);
				$data1['id_departement'] = htmlspecialchars($_POST['id_departement']);
				$data1['id_responsabilite'] = htmlspecialchars($_POST['id_responsabilite']);

				$this->Users->hydrate($data);
				$this->Users->UpdateUserinfo($_POST['id_users']);

				$this->Employer->hydrate($data1);
				$this->Employer->UpdateEmployer($_POST['id_users']);
				$_SESSION['message_save']="Modification de profil  enregistré avec success!";
	
				$this->session->mark_as_flash('message_save'); 
				redirect(site_url(array('Proprietaire','Touslesemployers')));
			}else{
				redirect(site_url(array('Proprietaire','Touslesemployers')));
				$_SESSION['message_error']="Une Erreur est subvenu recommencer!";
				$this->session->mark_as_flash('message_error'); 
			}
		}else{
			session_destroy();
			redirect(site_url(array('Proprietaire','formulaireConnexion')));
		}

	}



	public function manageConnexion(){

		if (isset($_POST['identifiant']) && isset($_POST['password'])) {

			$proprietaire = $this->Proprietaire->findAllproprietaireBd();

			for ($i = 0; $i < $proprietaire['total']; $i++) {
				$proprietaire['infos'][$i] = $this->Users->findUsers($proprietaire[$i]['id_users']);
				if ($proprietaire[$i]['identifiant'] == $_POST['identifiant'] && $proprietaire[$i]['password'] == htmlspecialchars(md5($_POST['password']))) {
					$_SESSION['PROPRIETAIRE'] = $proprietaire[$i];
				} elseif ($proprietaire['infos'][$i]['email'] == $_POST['identifiant'] && $proprietaire[$i]['password'] ==htmlspecialchars(md5($_POST['password']))) {
					$_SESSION['PROPRIETAIRE'] = $proprietaire[$i];
				}
		    }
			if (isset($_SESSION['PROPRIETAIRE']) ) {
				if($_SESSION['PROPRIETAIRE']['statut']==1){
					redirect(site_url(array('Proprietaire','index')));
				}else{
					$_SESSION['message_error'] = " Acces bloqué veillez contacter l'administrateur!";
					$this->session->mark_as_flash('message_error'); 
					redirect(site_url(array('Proprietaire','formulaireConnexion')));
				}
					
			}else{
				$_SESSION['message_error'] = " Adresse email ou mot de passe incorrect!";
				$this->session->mark_as_flash('message_error'); 
				redirect(site_url(array('Proprietaire','formulaireConnexion')));

			}
		} else {
			redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
		}
	}

	public function formulaireConnexion(){

		if (isset($_SESSION['PROPRIETAIRE'])) {
			if (isset($_SESSION['PROPRIETAIRE'])) {
				
				if($_SESSION['PROPRIETAIRE']['statut']==1){
					redirect(site_url(array('Proprietaire', 'index')));
					
				}else{
					session_destroy();
					$_SESSION['message_error'] = "Acces bloqué veillez contacter l'administrateur!";
					$this->session->mark_as_flash('message_error'); 
					$this->load->view('gestion_proprio/formulaire_connexion');	
				}
				
			} else {
				session_destroy();
				redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
			}
		} else {
			$this->load->view('gestion_proprio/formulaire_connexion');
		}
	}


	public function forgetpassword(){
		$this->load->view('gestion_proprio/index');
		
	}

	public function forgetpasswordentrernew(){
		$data['proprio']=$_SESSION['EMAIL'];
		$this->load->view('gestion_proprio/entrernewpwd',$data);
		
	}


	public function chainealeatoire(){
		$str = "0123456789abcdefghijklmnopkrstvwyzABCDFGHIJKLMOPQRSTVWYZ";
		   $str = str_shuffle($str);
		   return substr($str, 0, 8);
   }

	public function confirmpasswordemail(){
		if (isset($_POST['email'])) {
			$email=htmlspecialchars($_POST['email']);
			$data['proprietaire']=$this->Users->findUsersemail($email);
			if ($data['proprietaire']['data']=='ok') {
				$code=$this->chainealeatoire();
				$data1['password']=$code;
				$this->Proprietaire->hydrate($data1);
				$this->Proprietaire->Updatepasswordproprietaire($data['proprietaire']['id']);
				$to  = $email;
				$headers = "From: inchentreprise237@gmail.com";
				$body="Bonsoir Mr/Mme, ci-dessous le code de confirmation de votre adresse email pour la reinitialisation de votre mot de passe ".' '.$code;
				$tete="Inch Entreprise: Confirmation d'adresse mail";
				mail($to, $tete, $body, $headers);
				$_SESSION['EMAIL']=$data['proprietaire'];
				$this->load->view('gestion_proprio/confirmpwd',$data);
			}else{
				$_SESSION['message_error']="Cette adresse mail ne correspond a aucun compte!";
				$this->session->mark_as_flash('message_error');
				redirect(site_url(array('Proprietaire','forgetpassword')));
			}
			
		}else{
		 session_destroy();
		 redirect(site_url(array('Proprietaire','forgetpassword')));
	 }	   
		
	}

	public function confirmcodemail(){
		$codeemail=$this->Proprietaire->findpassword($_POST['id']);
		if($codeemail['password']==$_POST['code']){
			redirect(site_url(array('Proprietaire','forgetpasswordentrernew')));
		}else{
			$_SESSION['message_error']="Le code que vous avez inscrit n'est pas correct!";
			$this->session->mark_as_flash('message_error');
			redirect(site_url(array('Proprietaire','confirmpwd')));
	
		}
	    
	}

	public function enregupdatepwd(){
		if(isset($_POST['password']) AND !empty($_POST['password'])){
          $password=htmlspecialchars(md5($_POST['password']));
          $data2['password'] = $password;
		  $this->Proprietaire->hydrate($data2);
		  $this->Proprietaire->Updatepasswordproprietaire($_POST['id']);
		  $_SESSION['message_save']="Modification de mot de passe avec succes!";
		  $this->session->mark_as_flash('message_save');
		  redirect(site_url(array('Proprietaire','formulaireConnexion')));
		}else{
			$_SESSION['message_error']="Une erreur est subvenu veillez recommencer!";
			$this->session->mark_as_flash('message_error');
			redirect(site_url(array('Proprietaire','formulaireConnexion')));
		}
		
	}

    public function profilproprietaire(){

        if (isset($_SESSION['PROPRIETAIRE'])) {

			$data1['Allcategorie'] = $this->Categorie->findAllCategorieBd();
            $data1['Allsecteur'] = $this->Secteur->findAllSecteurBd();
            $data1['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
			$data1['active']=10;
			$data['Alluser'] = $this->Users->findUsers($_SESSION['PROPRIETAIRE']['id_users']);
            $this->load->view('PROPRIETAIRE/index',$data);
            $this->load->view('PROPRIETAIRE/navigationprop', $data1);
            $this->load->view('PROPRIETAIRE/profilprop', $data);
            $this->load->view('ADMIN/footer');
        } else {
            session_destroy();
            redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
        }
    }

	public function EnregmodifProprietaire(){
		if (isset($_SESSION['PROPRIETAIRE'])) {
			if (isset($_POST)) {
				if(isset($_FILES['profil']) AND !empty($_FILES['profil']['name'])){
					if (isset($_FILES['profil']) AND $_FILES['profil']['error'] == 0 ){
							// Testons si le fichier n'est pas trop gros
							if ($_FILES['profil']['size'] <= 100000000){
								// Testons si l'extension est autorisée
								$infosfichier =pathinfo($_FILES['profil']['name']);
								$extension_upload = $infosfichier['extension'];
	
								$config =$_FILES['profil']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['ADMIN']['id'];
								 $ma_variable = str_replace('.', '_', $config);
								$ma_variable = str_replace(' ', '_', $config);
								$config = $ma_variable.'.'.$extension_upload;
								move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
								$data['profil']=$config;
								
							}else{
									$_SESSION['message_error']="La taille du fichier choisie  est très grande veuillez le remplacer svp!";

									$this->session->mark_as_flash('message_error'); 
									$data['message']='non';
							}
					}else{
							$_SESSION['message_error']="L'image choisie  est endommagée  veuillez le remplacer svp!";

							$this->session->mark_as_flash('message_error'); 
							$data['message']='non';
					}
				}else{

					$data['profil']=htmlspecialchars($_POST['profil_pass']);
								
				}
					$data['nom']=htmlspecialchars($_POST['nom']);
					$data['prenom']=htmlspecialchars($_POST['prenom']);
					$data['telephone']=htmlspecialchars($_POST['telephone']);
					$data['numero_cni']=htmlspecialchars($_POST['numero_cni']);
					$data['email']=htmlspecialchars($_POST['email']);
					
					$this->Users->hydrate($data);
					$this->Users->UpdateUser($_POST['id']);
					$_SESSION['message_save']="Modification de profil  enregistré avec success!";
	
					$this->session->mark_as_flash('message_save'); 

					redirect(site_url(array('Proprietaire','profilproprietaire')));
			}else{
				redirect(site_url(array('Proprietaire','profilproprietaire')));
				$_SESSION['message_error']="Une Erreur est subvenu recommencer!";
				$this->session->mark_as_flash('message_error'); 
			}
		}else{
			session_destroy();
			redirect(site_url(array('Proprietaire','formulaireConnexion')));
		}

	}

	public function Addemployerexterne(){
		if (isset($_SESSION['PROPRIETAIRE'])) {

			$data1['Allcategorie'] = $this->Categorie->findAllCategorieBd();
            $data1['Allsecteur'] = $this->Secteur->findAllSecteurBd();
            $data1['Allentreprise'] = $this->Entreprise->findEntrepriseInfos($_SESSION['PROPRIETAIRE']['id']);
			$data1['active']=10;
			$data['Alluser'] = $this->Users->findUsers($_SESSION['PROPRIETAIRE']['id_users']);
            $this->load->view('PROPRIETAIRE/index',$data);
            $this->load->view('PROPRIETAIRE/navigationprop', $data1);
            $this->load->view('Welcome/pageconstruct');
            $this->load->view('ADMIN/footer');
        } else {
            session_destroy();
            redirect(site_url(array('Proprietaire', 'formulaireConnexion')));
        }
	}



    public function deconnexion() {
        if (isset($_SESSION['PROPRIETAIRE'])) {
            session_destroy();
        }
        redirect(site_url(array('Proprietaire', 'index')));
    }
}

